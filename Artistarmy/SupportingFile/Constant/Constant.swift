//
//  Constent.swift
//  Auction
//
//  Created by mac on 10/12/20.
//

import Foundation
import  UIKit
let appDelegate = UIApplication.shared.delegate as! AppDelegate

let kAppName = "Artist Army"
let kisLogin = "isLogin"

// http://artistarmy.de/artist_army_new/index.php/api/

let commonUrl = "http://artistarmy.de/"
//let commonUrl = "http://artistarmy.de/artist_army_new/index.php/"

let testImgUrl = "https://my-artistarmy-bucket.s3.us-east-2.amazonaws.com"

let baseUrl  =  "\(commonUrl)api/" // live server

let PostImagePath = "\(testImgUrl)/post/"

let ProfileImagePath  = "\(testImgUrl)/profile/"

let CategoryImagePath = "\(testImgUrl)/category/"

let  VideoImagePath =  "\(testImgUrl)/reels/"

let  ConcertImagePath =  "\(testImgUrl)/concerts/"

let  goalImagePath  = "\(testImgUrl)/images/"

let  songsPath  = "\(testImgUrl)/songs/"



let  termsCondition = "https://ctinfotech.com/CTCC/artist_army/home/terms"

let  privacypolicy = "https://ctinfotech.com/CTCC/artist_army/home/privacy"




//struct appColor {
let Pink = UIColor(named: "Pink")
let  lightPurple = UIColor(named: "lightPurple")
let gradWhite = UIColor(named: "white")
let gradgray = UIColor(named: "grey")
let gradCoral = UIColor(named: "Coral")
let green = UIColor(named: "green")

let darkgreen = UIColor(named: "darkgreen")
//}


//struct appColor {
//    let green = UIColor(named: "green")
//}



struct ApiAction {
    static let category_list = "category_list"
    static let signup_mobile = "signup_mobile"
    static let otp_verification = "otp_verification"
    static let signup_pass = "signup_pass"
    static let login = "login"
    static let add_post = "add_post"
    static let post_list = "post_list"
    static let post_like = "post_like"
    static let delete_post = "delete_post"
    static let getProfile = "getProfile"
    static let update_profile = "update_profile"
    static let post_all_comments = "post_all_comments"
    static let post_comment_like = "post_comment_like"
    static let add_post_comment = "add_post_comment"
    static let reels_list = "reels_list_with_type"
    static let add_followers = "add_followers"
    static let get_allfollowers = "get_allfollowers"
    static let artistProfile = "artistProfile"
    static let add_friends = "add_friends"
    static let all_concert = "all_concert"
    static let songs_list = "songs_list"
    static let get_allfansList = "get_allfansList"
    static let artistgoal_list = "artistgoal_list"
    static let newsfeed_list = "newsfeed_list"
    static let artistsongs_list = "artistsongs_list"
    static let reels_like = "reels_like"
    static let reels_favourite = "reels_favourite"
    static let genre_category = "genre_category"
    static let update_image = "update_image"
    static let ksearch = "search"
    static let kgrouplist_byid = "grouplist_byid"
    static let get_pinboard = "get_pinboard"
    static let add_pinboard = "add_pinboard"
    static let fanProfile = "fanProfile"
    static let get_media = "get_media"
    static let concertbooked_byuser = "concertbooked_byuser"
    static let favourite_artistlist = "favourite_artistlist"
    static let cancel_concert_booking = "cancel_concert_booking"
    static let concert_booking = "concert_booking"
    static let get_notification_by_date = "get_notification_by_date"
    static let get_allfriends = "get_allfriends"
    static let redeem_check = "redeem_check"
    static let leave_group = "leave_group"
    static let hall_of_fame = "hall_of_fame"
    static let delete_concerts = "delete_concerts"
    static let add_concerts = "add_concerts"
    static let get_bitpack = "get_bitpack"
    static let reels_all_comments = "reels_all_comments"
    static let add_reel_comment = "add_reel_comment"
    static let reels_comment_like = "reels_comment_like"
    static let add_reels = "add_reels"
    static let user_chat_messages = "user_chat_messages"
    static let chat_users = "chat_users"
    static let report_user = "report_user"
    static let chat_image = "chat_image"
    static let home_search = "home_search"
    static let delete_chat = "delete_chat"
    static let add_genre = "add_genre"
    static let reels_share = "reels_share"
    static let post_share = "post_share"
    static let block_user = "block_user"
    static let report_artist = "report_artist"
    static let artist_page_share = "artist_page_share"
    static let add_credits = "add_credits"
    static let donate_bits = "donate_bits"
    static let get_post_image = "get_post_image"
    static let select_bitpack = "select_bitpack"
    
    static let add_artistgoals = "add_artistgoals"
    
    
    
}



//MARK:- Validation string
enum Validation: String {
    case kEnterName = "Please Enter Username"
    case kEnterEmail = "Please Enter Your Email"
    case kEnterValidEmail = "Please Enter Valid Email Address"
    case kEnterMobileNumber = "Please Enter Mobile Number"
    case kEnterValidNumber = "Please Enter Valid Mobile Number"
    case kEnterCompanyName = "Please Enter Company Name"
    case kEnterEINNumber = "Please Enter EIN Number"
    case kEnterBusinessType = "Please Select Business Type"
    case kEnterWebUrl = "Please Enter Website Url"
    case kSelectCategory = "Please Select Category"
    case kEnterCompanyDis = "Please Enter Company Description"
    case kEnterAddress = "Please Enter Your Address"
    case kEnterPassword = "Please Enter Your Password"
    case kEnterConfirmPassword = "Please Enter Confirm Password"
    case kEnterValidPassword = "Please Enter Valid Password"
    case kPassowrdNotMatched = "New Password & Confirm Password Not Matched"
    case kEnterNewPassword = "Please Enter New Password"
    
    case kProductName = "Please Enter Product Name"
    case kProductDetail = "Please Enter Product Detail"
    case kProductDiscription = "Please Enter Product Description"
    case kEnterTitle = "Please Enter Title"
    case kEnterDetail = "Please Enter Detail"
    case kDiscription = "Please Enter Description"
    case kUserType = "Please Select UserType"
    case kCompanyImg = "Please Select Company image"
    case kCountryName = "Please Enter Country name"
    case kCityName = "Please Enter City name"
    case kStateName = "Please Enter State name"
    case kPostalCode = "Please Enter PostalCode"
    case kprivacy = "Please check privacy & policy"
    case kdate = "Please select date"
    case ktime = "Please select time"
    case kvenue = "Please enter  concert venue"
    case kdescription = "Please enter concert description"
    case kdonateAmount = "Please enter donate amount"
    case kdonateValidAmount = "Please enter valid donate amount"
    case kGoalName = "Please enter goal name"
    case kCerditsNedded = "Please enter amount of credits needed"
    case kImageUpload  = "Please upload image"
    
}

//MARK:- Button titile string
enum ButtonTitle: String {
    case kOk = "OK"
    case kCancel = "CANCEL"
    case kYes = "YES"
    case kNo = "NO"
}


struct Message {
    static let msgSorry            = "Sorry something went wrong."
    static let msgTimeOut          = "Request timed out."
    static let msgCheckConnection  = "Please check your connection and try again."
    static let msgConnectionLost   = "Network connection lost."
    static let Key_Alert           = ""
    static let couldNotConnect     = "Could not able to connect with server. Please try again."
    let networkAlertMessage   = "Please Check Internet Connection"
}


struct params {
    static let kmobile_number  = "mobile_number"
    static let kcountry_code  = "country_code"
    static let ksocial  = "social"
    static let kmobile_otp  = "otp"
    static let kpassword  = "password"
    static let kuser_id  = "user_id"
    static let kdevice_type  = "device_type"
    static let kios_token  = "ios_token"
    static let kdescription  = "description"
    static let kpost_type  = "post_type"
    static let kimage  = "images[]"
    static let kpost_id  = "post_id"
    static let kartist_name = "artist_name"
    static let kdate_of_birth = "date_of_birth"
    static let kgenre_cat = "genre_cat"
    static let kbiography = "biography"
    static let kformer_bands = "former_bands"
    static let kaddress = "address"
    static let kspotify = "spotify"
    static let kyoutube = "youtube"
    static let kinstagram = "instagram"
    static let kfacebook = "facebook"
    static let kprofile_image = "profile_image"
    static let kcomment_id = "comment_id"
    static let ksender_id = "sender_id"
    static let kmessage = "message"
    static let kfan_id = "fan_id"
    
    static let kartist_id = "artist_id"
    static let kfans_id = "fans_id"
    static let kfriends_id = "friends_id"
    static let kreels_id = "reels_id"
    static let ksearch = "search"
    static let kconcert_id = "concert_id"
    
    static let kgoal_id = "goal_id"
    static let kfull_name = "full_name"
    static let kemail = "email"
    static let kphone_number = "phone_number"
    static let kcountry = "country"
    static let kaddress1 = "address1"
    static let kaddress2 = "address2"
    static let kcity = "city"
    static let kstate = "state"
    static let kpostcode = "postcode"
    static let kgroup_id = "group_id"
    
    static let kconcert_title = "concert_title"
    static let kconcert_date = "concert_date"
    static let kconcert_time = "concert_time"
    static let kconcert_venue = "concert_venue"
    static let kconcert_image = "concert_image"
    
    static let  reel_description = "reel_description"
    static let  reel_songs = "reel_songs"
    static let  reel_video = "reel_video"
    static let  block_id = "block_id"
    static let  kgenre_name = "genre_name"
    static let  kfilter = "filter"
    static let  ktype = "type"
    static let  kshare_to = "share_to"
    static let  kref_user_id = "ref_user_id"
    static let  kref_artist_id = "ref_artist_id"
    static let   ksong_id  = "song_id"
    static let   ksong_no  = "song_no"
    static let   krefer_id  = "refer_id"
    static let kstart  = "start"
    static let kpageCount  = "page_count"
    static let kdonate_bit = "donate_bit"
    static let kdonate_by = "donate_by"
    static let kdonate_to = "donate_to"
    static let kdonate_name = "donate_name"
    static let kbit_pack_id = "bit_pack_id"
    static let kbit_amount = "bit_amount"
    static let reel_image = "reel_image"
               
    static let goal_name = "goal_name"
    static let amount = "amount"
    static let exp_date = "exp_date"
    static let exp_time = "exp_time"
    static let description = "description"
    static let goal_image = "goal_image"
    
    
    ///-----
    static let kusername  = "username"
    
    static let kein  = "ein"
    static let kbusiness_type  = "business_type"
    static let kwebsite  = "website"
    static let kcompany_description  = "company_description"
    static let kcategorys  = "categorys"
    static let kcompany_name  = "company_name"
    
    
    //
    //login
    static let kfcm_token  = "fcm_token"
    static let kcompany_image  = "company_image"
    
    static let kcategory_id  = "category_id"
    static let ksearch_keyword  = "search_keyword"
    static let kproduct_name  = "product_name"
    static let kcategory  = "category"
    static let kproduct_detail = "product_detail"
    static let kproduct_description = "product_description"
    static let kid = "id"
    static let ksort_filter = "sort_filter"
    static let kto_user = "to_user"
    static let krate = "rate"
    static let kreview_title = "review_title"
    static let kcompany_id = "company_id"
    static let kreview_description = "review_description"
    static let kproduct_id = "product_id"
    static let kcustomer_id = "customer_id"
    static let kuser_type = "user_type"
    static let kplan_id = "plan_id"
    static let kstripeToken = "stripeToken"
    static let krate_id = "rate_id"
    static let ktitle = "title"
    static let kdetail = "detail"
    static let kticket_id = "ticket_id"
    static let kcomment = "comment"
    static let kfriend_id = "friend_id"
    
}




extension HomeVC{
    func setTabBarItems(){
        let tabBar = self.tabBarController!.tabBar
        let myTabBarItem1 = (tabBar.items?[0])! as UITabBarItem
        myTabBarItem1.image = UIImage(named: "home")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem1.selectedImage = UIImage(named: "home_selected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem1.title = ""
        myTabBarItem1.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        
        let myTabBarItem2 = (tabBar.items?[1])! as UITabBarItem
        myTabBarItem2.image = UIImage(named: "notification")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem2.selectedImage = UIImage(named: "notification_selected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem2.title = ""
        myTabBarItem2.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        
        
        let myTabBarItem3 = (tabBar.items?[2])! as UITabBarItem
        myTabBarItem3.image = UIImage(named: "video")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem3.selectedImage = UIImage(named: "video_selected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem3.title = ""
        myTabBarItem3.imageInsets = UIEdgeInsets(top: 3, left: 0, bottom: -3, right: 0)
        
        let myTabBarItem4 = (tabBar.items?[3])! as UITabBarItem
        myTabBarItem4.image = UIImage(named: "favourite")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem4.selectedImage = UIImage(named: "favourite_selected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem4.title = ""
        myTabBarItem4.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        
        let myTabBarItem5 = (tabBar.items?[4])! as UITabBarItem
        myTabBarItem5.image = UIImage(named: "profile2")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem5.selectedImage = UIImage(named: "profile_selected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem5.title = ""
        myTabBarItem5.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        
    }
}



extension UITabBar {
    
    static func setTransparentTabbar() {
     /*   if #available(iOS 15.0, *) {
           let appearance = UITabBarAppearance()
           appearance.configureWithOpaqueBackground()
           appearance.backgroundColor = customColor
           
           self.tabController.tabBar.standardAppearance = appearance
           self.tabController.tabBar.scrollEdgeAppearance = view.standardAppearance
        }else{
            UITabBar.appearance().backgroundImage = UIImage(named: "tabbarBg")
                    UITabBar.appearance().shadowImage     = UIImage()
                    UITabBar.appearance().clipsToBounds   = true
        }*/
        UITabBar.appearance().backgroundImage = UIImage(named: "tabbarBg")
                UITabBar.appearance().shadowImage     = UIImage()
                UITabBar.appearance().clipsToBounds   = true
        
    }
}


class tabBar: UITabBarController{
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 15.0, *) {
               let appearance = UITabBarAppearance()
               appearance.configureWithOpaqueBackground()
            appearance.backgroundImage = UIImage(named: "tabbarBg")
            appearance.backgroundColor = .clear
            appearance.shadowImage = UIImage()
           // appearance.accessibilityPath?.addClip()
               self.tabBar.standardAppearance = appearance
               self.tabBar.scrollEdgeAppearance = appearance
        }else{
            UITabBar.appearance().backgroundImage = UIImage(named: "tabbarBg")
                    UITabBar.appearance().shadowImage     = UIImage()
                    UITabBar.appearance().clipsToBounds   = true
        }
    }
}
