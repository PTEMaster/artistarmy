/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Donaters : Codable {
	let id : String?
	let donate_bit : String?
	let donate_by : String?
	let donate_to : String?
	let message : String?
	let donate_name : String?
	let created_at : String?
	let full_name : String?
	let email : String?
	let profile_image : String?
	let donator_id : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case donate_bit = "donate_bit"
		case donate_by = "donate_by"
		case donate_to = "donate_to"
		case message = "message"
		case donate_name = "donate_name"
		case created_at = "created_at"
		case full_name = "full_name"
		case email = "email"
		case profile_image = "profile_image"
		case donator_id = "donator_id"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		donate_bit = try values.decodeIfPresent(String.self, forKey: .donate_bit)
		donate_by = try values.decodeIfPresent(String.self, forKey: .donate_by)
		donate_to = try values.decodeIfPresent(String.self, forKey: .donate_to)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		donate_name = try values.decodeIfPresent(String.self, forKey: .donate_name)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
		full_name = try values.decodeIfPresent(String.self, forKey: .full_name)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
		donator_id = try values.decodeIfPresent(String.self, forKey: .donator_id)
	}

}


struct Donate_list : Codable {
    let id : String?
    let donate_bit : String?
    let donate_by : String?
    let donate_to : String?
    let message : String?
    let donate_name : String?
    let created_at : String?
    let full_name : String?
    let email : String?
    let profile_image : String?
    let donator_id : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case donate_bit = "donate_bit"
        case donate_by = "donate_by"
        case donate_to = "donate_to"
        case message = "message"
        case donate_name = "donate_name"
        case created_at = "created_at"
        case full_name = "full_name"
        case email = "email"
        case profile_image = "profile_image"
        case donator_id = "donator_id"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        donate_bit = try values.decodeIfPresent(String.self, forKey: .donate_bit)
        donate_by = try values.decodeIfPresent(String.self, forKey: .donate_by)
        donate_to = try values.decodeIfPresent(String.self, forKey: .donate_to)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        donate_name = try values.decodeIfPresent(String.self, forKey: .donate_name)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        full_name = try values.decodeIfPresent(String.self, forKey: .full_name)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
        donator_id = try values.decodeIfPresent(String.self, forKey: .donator_id)
    }

}
