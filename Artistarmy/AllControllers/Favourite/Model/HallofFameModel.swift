/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct HallofFameModel : Codable {
	let code : String?
	let message : String?
	let top_fan : [Top_fan]?
	let credits : [Credits]?
	let donaters : [Donaters]?
    let donate_list : [Donate_list]?
    
    

	enum CodingKeys: String, CodingKey {

		case code = "code"
		case message = "message"
		case top_fan = "top_fan"
		case credits = "credits"
		case donaters = "donaters"
        case  donate_list = "donate_list"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		code = try values.decodeIfPresent(String.self, forKey: .code)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		top_fan = try values.decodeIfPresent([Top_fan].self, forKey: .top_fan)
		credits = try values.decodeIfPresent([Credits].self, forKey: .credits)
		donaters = try values.decodeIfPresent([Donaters].self, forKey: .donaters)
        donate_list = try values.decodeIfPresent([Donate_list].self, forKey: .donate_list)
        
	}

}
