//
//  HomeVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit

class HallofFrameVC: KBaseViewController {
    
    @IBOutlet weak var tablePinboard: UITableView!
    @IBOutlet weak var viewBack: UIView!
    
    
    var isComeFromProfile = false
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBack.isHidden = true
        if   isComeFromProfile{
            viewBack.isHidden = false
        }
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Hall_of_fame_API()
    }
    
    
  
    @IBAction func actionBack(_ sender: Any) {
        pop()
    }
   
    
      @IBAction func actionFilter(_ sender: UIButton) {
        let alertController = storyboard?.instantiateViewController(withIdentifier: "DaysPopupOver") as! DaysPopupOver
            alertController.modalPresentationStyle = .popover
            let popover = alertController.popoverPresentationController
            popover?.delegate = self
            alertController.delegate = self
            popover?.sourceView = sender
            popover?.permittedArrowDirections =  .up
            alertController.preferredContentSize = CGSize(width: 90, height: 120)
            self.present(alertController, animated: true, completion: nil)
            
        }
    
}



extension HallofFrameVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  5
    }
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HallofFrameCell")  as! HallofFrameCell

            return cell
    
        
    
    
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    
    }
      
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
 
}



extension HallofFrameVC {
    func Hall_of_fame_API(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        ServerManager.shared.POST(url: ApiAction.hall_of_fame , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SignupModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
}

extension HallofFrameVC: UIPopoverPresentationControllerDelegate ,DayPopupOverDelegate{
    func onButtonClick(button str: dayList) {
        if str == .All{
            
        }else if str == .thirty{
            
        }else{
            //sixty
            
        }
    }
    
    
    
  
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
  
    
}
