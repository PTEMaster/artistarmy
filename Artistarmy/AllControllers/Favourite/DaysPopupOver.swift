//
//  DatPopover.swift
//  Artistarmy
//
//  Created by mac on 12/08/21.
//

import UIKit
protocol DayPopupOverDelegate {
    func onButtonClick(button str:dayList )
}
class DaysPopupOver: UIViewController {

    @IBOutlet weak var btnAll: UIButton!
    @IBOutlet weak var btnThirty: UIButton!
    @IBOutlet weak var btnSixty: UIButton!
    var delegate: DayPopupOverDelegate?
    var isSaved = ""
    var myid = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    // MARK: - Button Action

    @IBAction func actionAll(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.onButtonClick(button: .All)
        }
    }
    
    @IBAction func actionThirty(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.onButtonClick(button: .thirty)
        }
    }
    @IBAction func actionsixty(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.onButtonClick(button: .sixty)
        }
    }
  
   
}


enum dayList {
    case All
    case thirty
    case sixty
}

