//
//  HalloffameStaticVC.swift
//  Artistarmy
//
//  Created by mac on 30/07/21.
//

import UIKit
import SDWebImage

class HalloffameStaticVC: KBaseViewController {

    @IBOutlet weak var cvFans: UICollectionView!
    @IBOutlet weak var tblFans: UITableView!
    @IBOutlet weak var cvDonators: UICollectionView!
    @IBOutlet weak var tblDonators: UITableView!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var viewBg: UIView!
    
    var arrtopfan = [Top_fan]()
    var arrcredits = [Credits]()
    var arrdonaters = [Donaters]()
    var arrdonate_list = [Donate_list]()
    
    @IBOutlet weak var contFanList: NSLayoutConstraint!
    @IBOutlet weak var constFour: NSLayoutConstraint!
    @IBOutlet weak var constthree: NSLayoutConstraint!
    @IBOutlet weak var constTwo: NSLayoutConstraint!
    @IBOutlet weak var viewBack: UIView!
    
    var isComeFromProfile = false
    var usedID = ""
    var userName = ""
    var filter = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewBg.isHidden = true
        viewBack.isHidden = true
        if   isComeFromProfile{
            viewBack.isHidden = false
            lblName.text = userName
        }else{
            lblName.text = AppDataHelper.shard.logins.full_name!
            usedID = AppDataHelper.shard.logins.id!
        }
        filter = ""
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.viewBg.isHidden = true
        Hall_of_fame_API()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func actionBack(_ sender: Any) {
        pop()
    }
    
    @IBAction func actionFilter(_ sender: UIButton) {
      let alertController = storyboard?.instantiateViewController(withIdentifier: "DaysPopupOver") as! DaysPopupOver
          alertController.modalPresentationStyle = .popover
          let popover = alertController.popoverPresentationController
          popover?.delegate = self
          alertController.delegate = self
          popover?.sourceView = sender
          popover?.permittedArrowDirections =  .up
          alertController.preferredContentSize = CGSize(width: 90, height: 120)
          self.present(alertController, animated: true, completion: nil)
          
      }

}


extension HalloffameStaticVC {
    func Hall_of_fame_API(){
        var param = [String : Any]()
        param[params.kuser_id] = usedID
        param[params.kfilter] = filter
        if   isComeFromProfile{
            param[params.ktype] = "1"
        }else{
            if AppDataHelper.shard.isFan{
                param[params.ktype] = "0"
            }else{
                param[params.ktype] = "1"
            }
        }
       
        
        ServerManager.shared.POST(url: ApiAction.hall_of_fame , param: param, true,true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(HallofFameModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrtopfan = obj.top_fan ?? []
                self?.arrcredits = obj.credits!
                self?.arrdonaters = obj.donaters ?? []
                self?.arrdonate_list = obj.donate_list ?? []
                if  (self?.arrtopfan.count)! == 0{
                    self?.contFanList.constant = 0
                }
                if  (self?.arrcredits.count)! == 0{
                    self?.tblFans.isHidden = true
                    self?.constTwo.constant = 0
                }
                if  (self?.arrdonaters.count)! == 0{
                    self?.constthree.constant = 0
                }
                if  (self?.arrdonate_list.count)! == 0{
                    self?.constFour.constant = 0
                }
                
                self?.cvFans.reloadData()
                self?.tblFans.reloadData()
                self?.cvDonators.reloadData()
                self?.tblDonators.reloadData()
                self?.viewBg.isHidden = false
              //  Artistarmy.show(message: obj.message!)
            } else {
                print("failure")
                self?.viewBg.isHidden = true
                Artistarmy.show(message: obj.message!)
            }
        }
    }
}


extension HalloffameStaticVC: UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
  
    
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         if cvFans == collectionView{
            return arrtopfan.count
        }else{
            return arrdonaters.count
        }
        
        
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if cvFans == collectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileDetailCell", for: indexPath) as! ProfileDetailCell
            let obj = arrtopfan[indexPath.row]
            
            cell.img.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.img.sd_setImage(with: URL(string: ProfileImagePath +  (obj.profile_image)! ), placeholderImage: UIImage(named: "user"))
            cell.lblFanName.text = obj.full_name
            cell.lblcount.text = obj.total_like
            return cell
        }
     else{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CreditGoalsDetailCell", for: indexPath) as! CreditGoalsDetailCell
          let obj = arrdonaters[indexPath.row]

        cell.img.backgroundColor = green
        cell.lblFanName.text = obj.full_name
        cell.lblcount.text = obj.donate_bit
        
            
            return cell
        }
      
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
       
    }
}



extension HalloffameStaticVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblFans{
            return  arrcredits.count
        }else{
            return  arrdonate_list.count
        }
       
    }
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "TopFanCell")  as! TopFanCell
    if tableView == tblFans{
        let obj = arrcredits[indexPath.row]

      cell.lblCount.text  = "\(indexPath.row + 1)"
      cell.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
      if (obj.profile_image != nil){
      cell.imgProfile.sd_setImage(with: URL(string: ProfileImagePath +  (obj.profile_image)! ), placeholderImage: UIImage(named: "user"))
      }
      cell.lblName.text = obj.full_name
      cell.lblFollowCount.text = obj.amount
    }else{
        let obj = arrdonate_list[indexPath.row]

      cell.lblCount.text  = "\(indexPath.row + 1)"
      cell.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
      if (obj.profile_image != nil){
      cell.imgProfile.sd_setImage(with: URL(string: ProfileImagePath +  (obj.profile_image)! ), placeholderImage: UIImage(named: "user"))
      }
      cell.lblName.text = obj.full_name
      cell.lblFollowCount.text = obj.donate_bit
    }

    cell.lblDiscription.text = "Top Fan"
    cell.lblCredit.text = "Credits"
    
    
        return cell

      }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    
    }
      
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
 
}


extension HalloffameStaticVC: UIPopoverPresentationControllerDelegate ,DayPopupOverDelegate{
    func onButtonClick(button str: dayList) {
        if str == .All{
            filter = ""
            Hall_of_fame_API()
        }else if str == .thirty{
            filter = "30"
            Hall_of_fame_API()
        }else{
            //sixty
            filter = "60"
            Hall_of_fame_API()
        }
    }
    
    
    
  
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
  
    
}
