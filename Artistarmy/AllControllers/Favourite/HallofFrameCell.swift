//
//  HallofFrameCell.swift
//  Artistarmy
//
//  Created by mac on 30/06/21.
//


import UIKit


class HallofFrameCell: UITableViewCell {
  
    @IBOutlet weak var tableFan: UITableView!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var lblCount1: UILabel!
    @IBOutlet weak var lblName1: UILabel!
    
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var lblCount2: UILabel!
    @IBOutlet weak var lblName2: UILabel!
    
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var lblCount3: UILabel!
    @IBOutlet weak var lblName3: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.tableFan.delegate = self
         self.tableFan.dataSource = self
    }

  
}


extension HallofFrameCell: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  5
    }
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TopFanCell")  as! TopFanCell
            return cell
    
        
    
    
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    
    }
      
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
 
}


class TopFanCell: UITableViewCell {
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDiscription: UILabel!
    @IBOutlet weak var lblFollowCount: UILabel!
    @IBOutlet weak var lblCredit: UILabel!
    
  
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

  
}
