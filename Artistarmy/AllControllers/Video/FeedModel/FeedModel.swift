

import Foundation
struct FeedModel : Codable {
	let code : String?
	let message : String?
	let reels_list : [Reels_list]?
	let for_you : [For_you]?

	enum CodingKeys: String, CodingKey {

		case code = "code"
		case message = "message"
		case reels_list = "reels_list"
		case for_you = "for_you"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		code = try values.decodeIfPresent(String.self, forKey: .code)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		reels_list = try values.decodeIfPresent([Reels_list].self, forKey: .reels_list)
		for_you = try values.decodeIfPresent([For_you].self, forKey: .for_you)
	}

}
