

import Foundation
struct Reels_list : Codable {
	let id : String?
	let reel_description : String?
	let reel_video : String?
	let reel_songs : String?
	let user_id : String?
	let total_comment : String?
	var total_like : String?
	var total_star : String?
	let created_at : String?
	let song_name : String?
	let singer_name : String?
	var is_like : String?
	var is_favourite : String?
	let user_profile_image : String?
	let user_full_name : String?
	let sum_reel : String?
    let reel_image : String?
    let song_url : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case reel_description = "reel_description"
		case reel_video = "reel_video"
		case reel_songs = "reel_songs"
		case user_id = "user_id"
		case total_comment = "total_comment"
		case total_like = "total_like"
		case total_star = "total_star"
		case created_at = "created_at"
		case song_name = "song_name"
		case singer_name = "singer_name"
		case is_like = "is_like"
		case is_favourite = "is_favourite"
		case user_profile_image = "user_profile_image"
		case user_full_name = "user_full_name"
        case sum_reel = "sum_reel"
        case reel_image = "reel_image"
        case song_url =  "song_url"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		reel_description = try values.decodeIfPresent(String.self, forKey: .reel_description)
		reel_video = try values.decodeIfPresent(String.self, forKey: .reel_video)
		reel_songs = try values.decodeIfPresent(String.self, forKey: .reel_songs)
		user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
		total_comment = try values.decodeIfPresent(String.self, forKey: .total_comment)
		total_like = try values.decodeIfPresent(String.self, forKey: .total_like)
		total_star = try values.decodeIfPresent(String.self, forKey: .total_star)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
		song_name = try values.decodeIfPresent(String.self, forKey: .song_name)
		singer_name = try values.decodeIfPresent(String.self, forKey: .singer_name)
		is_like = try values.decodeIfPresent(String.self, forKey: .is_like)
		is_favourite = try values.decodeIfPresent(String.self, forKey: .is_favourite)
		user_profile_image = try values.decodeIfPresent(String.self, forKey: .user_profile_image)
		user_full_name = try values.decodeIfPresent(String.self, forKey: .user_full_name)
		sum_reel = try values.decodeIfPresent(String.self, forKey: .sum_reel)
        reel_image = try values.decodeIfPresent(String.self, forKey: .reel_image)
        song_url = try values.decodeIfPresent(String.self, forKey: .song_url)
	}

}
