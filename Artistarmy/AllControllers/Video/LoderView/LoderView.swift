//
//  LoderView.swift
//  Borobear
//
//  Created by mac on 28/09/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

import Foundation

class  loderView :UIViewController  , NVActivityIndicatorViewable{
    private let presentingIndicatorTypes = {
        return NVActivityIndicatorType.allCases.filter { $0 != .blank }
    }()
    
    static let shared = loderView()
    func showLoder() {
     //    startAnimating(CGSize(width: 60, height: 40), message: "", type: presentingIndicatorTypes[0], fadeInAnimation: nil)
        startAnimating(CGSize(width: 60, height: 40), message: nil, messageFont: nil, type: presentingIndicatorTypes[0], color: green, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: UIColor.black.withAlphaComponent(0.3), textColor: nil, fadeInAnimation: nil)
        //     self.startAnimating(CGSize(width: 60, height: 40), message: "", type: presentingIndicatorTypes[0], fadeInAnimation: nil)
        
    }
    func hideLoder() {
        self.stopAnimating(nil)
    }
}

//class LoderView1: UIView  , NVActivityIndicatorViewable{
//    private let presentingIndicatorTypes = {
//        return NVActivityIndicatorType.allCases.filter { $0 != .blank }
//    }()
//
//    func show(viewVC:UIViewController) {
//   //     viewVC.startAnimating(CGSize(width: 60, height: 40), message: "", type: presentingIndicatorTypes[0], fadeInAnimation: nil)
//
//    //     self.startAnimating(CGSize(width: 60, height: 40), message: "", type: presentingIndicatorTypes[0], fadeInAnimation: nil)
//
//    }
//}
