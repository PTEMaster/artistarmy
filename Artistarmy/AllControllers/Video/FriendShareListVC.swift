//
//  FriendList.swift
//  Artistarmy
//
//  Created by mac on 17/08/21.
//


import Foundation
import UIKit
import SDWebImage


 class FriendShareListVC: KBaseViewController {
    
    @IBOutlet weak var tblFriend: UITableView!
    var arrfriends = [Friends]()
    var arrFollower =  [Followers]()
    
    var artistId = ""
    var reelId = ""
    
    
    var enumShareThrough = shareThrough.reels
    override func viewDidLoad() {
        super.viewDidLoad()
        if AppDataHelper.shard.isFan{
            get_allfriendAPI()
        }else{
            get_allfollowers_API()
        }
    }
    
    @IBAction func actiondismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionShare(_ sender: Any) {
        if AppDataHelper.shard.isFan{
            let even = arrfriends.filter { $0.isSelected == true }
            let values = even.map { $0.id }
            let str = (values.map{String($0!)}).joined(separator: ",")
            
            switch enumShareThrough {
            case .post:
                post_share_API(str)
                break
            case .reels:
                reels_share_API(str)
                break
            case .profile:
                artist_page_share_API(str)
                break
                
            
            }
          
        }else{
            let even = arrFollower.filter { $0.isSelected == true }
            let values = even.map { $0.id }
            let str = (values.map{String($0!)}).joined(separator: ",")
         
            switch enumShareThrough {
            case .post:
                post_share_API(str)
                break
            case .reels:
                reels_share_API(str)
                break
            case .profile:
                artist_page_share_API(str)
                break
                
            
            }
        }
        
       
      //
    }
    
    
}

// MARK: - UITableViewDataSource
extension FriendShareListVC: UITableViewDelegate , UITableViewDataSource {
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if AppDataHelper.shard.isFan{
            return arrfriends.count
        }else{
            return arrFollower.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReelShareCell", for: indexPath) as! ReelShareCell
        if AppDataHelper.shard.isFan{
            let obj = arrfriends[indexPath.row]
            cell.lblName.text = obj.full_name
            
            cell.imgUser.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
            cell.imgUser.sd_setImage(with: URL(string:  ProfileImagePath + (obj.profile_image)!), placeholderImage: UIImage(named: "user"))
            if obj.isSelected{
                cell.btnCheck.isSelected = true
            }else{
                cell.btnCheck.isSelected = false
                }
            cell.btnCheck.tag = indexPath.row
            cell.btnCheck.addTarget(self, action: #selector(userDetail(_:)), for: .touchUpInside)
            
        }else{
            let obj = arrFollower[indexPath.row]
            cell.lblName.text = obj.full_name
            cell.imgUser.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
            cell.imgUser.sd_setImage(with: URL(string:  ProfileImagePath + (obj.profile_image)!), placeholderImage: UIImage(named: "user"))
            if obj.isSelected{
                cell.btnCheck.isSelected = true
            }else{
                cell.btnCheck.isSelected = false
                }
            
            cell.btnCheck.tag = indexPath.row
            cell.btnCheck.addTarget(self, action: #selector(userDetail(_:)), for: .touchUpInside)
        }
        
        return cell
    }

    
    @objc func userDetail(_ sender: UIButton) {
        if AppDataHelper.shard.isFan{
       let dict = self.arrfriends[sender.tag]
        let ind =  IndexPath(row: sender.tag, section: 0)
            let cell = tblFriend.cellForRow(at: ind) as! ReelShareCell
         if  dict.isSelected{
            arrfriends[sender.tag].isSelected = false
            cell.btnCheck.isSelected = false
         }else{
            arrfriends[sender.tag].isSelected = true
            cell.btnCheck.isSelected = true
         }
        }else{
            let dict = self.arrFollower[sender.tag]
             let ind =  IndexPath(row: sender.tag, section: 0)
                 let cell = tblFriend.cellForRow(at: ind) as! ReelShareCell
              if  dict.isSelected{
                arrFollower[sender.tag].isSelected = false
                 cell.btnCheck.isSelected = false
              }else{
                arrFollower[sender.tag].isSelected = true
                 cell.btnCheck.isSelected = true
              }
        }
     }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   
   }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}






class ReelShareCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var btnCheck: UIButton!
}







extension FriendShareListVC{
    func get_allfriendAPI(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        
        ServerManager.shared.POST(url: ApiAction.get_allfriends , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(FriendModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrfriends = obj.friends ?? []
                self?.tblFriend.reloadData()
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    
    func get_allfollowers_API(){
        var param = [String : Any]()
       param[params.kartist_id] = AppDataHelper.shard.logins.id
        
        ServerManager.shared.POST(url: ApiAction.get_allfollowers , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(FollowerModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrFollower = obj.followers!
                self?.tblFriend.reloadData()
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    func reels_share_API(_ share_to : String){
        var param = [String : Any]()
        param[params.kartist_id] = artistId
        param[params.kshare_to] = share_to
       param[params.kreels_id] = reelId
       param[params.kuser_id] = AppDataHelper.shard.logins.id
        
        ServerManager.shared.POST(url: ApiAction.reels_share , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self!.dismiss(animated: true, completion: nil)
                Artistarmy.show(message: obj.message!)
                
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    func post_share_API(_ share_to : String){
        var param = [String : Any]()
        
        
        
        param[params.kpost_id] = artistId
        param[params.kshare_to] = share_to
       param[params.kuser_id] = AppDataHelper.shard.logins.id
        
        ServerManager.shared.POST(url: ApiAction.post_share , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self!.dismiss(animated: true, completion: nil)
                Artistarmy.show(message: obj.message!)
                
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    func artist_page_share_API(_ share_to : String){
        var param = [String : Any]()
        param[params.kartist_id] = artistId
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kshare_to] = share_to
        ServerManager.shared.POST(url: ApiAction.artist_page_share , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self!.dismiss(animated: true, completion: nil)
                Artistarmy.show(message: obj.message!)
            } else {
                print("failure")
              
                Artistarmy.show(message: obj.message ?? "")
            }
        }
    }
    
 
    
}


enum shareThrough {
    case reels
    case post
    case profile
}
