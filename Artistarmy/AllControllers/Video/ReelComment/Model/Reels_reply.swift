/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Reels_reply : Codable {
	let comment_id : String?
	let reels_id : String?
	let reciever_id : String?
	let sender_id : String?
	let message : String?
	let created_date : String?
	let created_time : String?
	let refer_id : String?
	let created_at : String?
	var total_like : String?
	var is_comment_like : Int?
	let full_name : String?
	let profile_image : String?
	let email : String?
    let user_type : String?

	enum CodingKeys: String, CodingKey {

		case comment_id = "comment_id"
		case reels_id = "reels_id"
		case reciever_id = "reciever_id"
		case sender_id = "sender_id"
		case message = "message"
		case created_date = "created_date"
		case created_time = "created_time"
		case refer_id = "refer_id"
		case created_at = "created_at"
		case total_like = "total_like"
		case is_comment_like = "is_comment_like"
		case full_name = "full_name"
		case profile_image = "profile_image"
		case email = "email"
        case user_type = "user_type"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		comment_id = try values.decodeIfPresent(String.self, forKey: .comment_id)
		reels_id = try values.decodeIfPresent(String.self, forKey: .reels_id)
		reciever_id = try values.decodeIfPresent(String.self, forKey: .reciever_id)
		sender_id = try values.decodeIfPresent(String.self, forKey: .sender_id)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		created_date = try values.decodeIfPresent(String.self, forKey: .created_date)
		created_time = try values.decodeIfPresent(String.self, forKey: .created_time)
		refer_id = try values.decodeIfPresent(String.self, forKey: .refer_id)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
		total_like = try values.decodeIfPresent(String.self, forKey: .total_like)
		is_comment_like = try values.decodeIfPresent(Int.self, forKey: .is_comment_like)
		full_name = try values.decodeIfPresent(String.self, forKey: .full_name)
		profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
		email = try values.decodeIfPresent(String.self, forKey: .email)
        user_type = try values.decodeIfPresent(String.self, forKey: .user_type)
	}

}
