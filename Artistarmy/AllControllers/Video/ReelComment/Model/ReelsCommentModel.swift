

import Foundation
struct ReelsCommentModel : Codable {
	let code : String?
	let message : String?
	let reels_comments : [Reels_comments]?
	let total_comment : Int?

	enum CodingKeys: String, CodingKey {

		case code = "code"
		case message = "message"
		case reels_comments = "reels_comments"
		case total_comment = "total_comment"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		code = try values.decodeIfPresent(String.self, forKey: .code)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		reels_comments = try values.decodeIfPresent([Reels_comments].self, forKey: .reels_comments)
		total_comment = try values.decodeIfPresent(Int.self, forKey: .total_comment)
	}

}
