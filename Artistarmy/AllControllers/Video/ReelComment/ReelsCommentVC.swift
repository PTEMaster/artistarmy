//
//  ReelsCommentVC.swift
//  Traydi
//
//  Created by mac on 26/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit
import  SDWebImage
class ReelsCommentVC: KBaseViewController {

   // @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var txtVComment: UITextView!
    @IBOutlet weak var CommentConstraintBottom: NSLayoutConstraint!
    @IBOutlet weak var txtVConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var tableViewComments: UITableView!
    @IBOutlet weak var btnSend: UIButton!
    var postComment: [Reels_comments]?
    var post_id = ""
    var isImages:Bool = true
    var myIndex : IndexPath?
    @IBOutlet weak var lblReplyTo: UILabel!
    var isReplyToComment  : Bool = false
    var replyToIndex : Int = 0
    var replyToRow : Int = 0
    var isMoveToBottom = true
    @IBOutlet weak var viewComment: UIView!
    
    @IBOutlet weak var stkReply: UIStackView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        post_all_comments_API()
        txtVComment.delegate = self
        txtVComment.text = ""
        btnSend.isEnabled = false
        tableViewComments.sectionHeaderHeight = UITableView.automaticDimension
      //  self.lblComment.text = "\(postComment?.count ?? 0)" + " Comments"
          
        self.tableViewComments.remembersLastFocusedIndexPath = true
        stkReply.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
  
    }
    
    func scrollToBottom(){
        DispatchQueue.main.async {
            if (self.postComment?.count)! > 0{
                let indexPath = IndexPath(row: NSNotFound, section: (self.postComment?.count)! - 1)
                self.tableViewComments.scrollToRow(at: indexPath, at: .bottom, animated: false)
                }
            
        }
    }
    
   
    
    func validation() -> Bool {
        var status:Bool = true
        if txtVComment.text.isEmpty   {
            status = false
        }
        return status
    }
    @IBAction func actionCloseToReply(_ sender: Any) {
         isReplyToComment = false
          stkReply.isHidden = true
    }
    @IBAction func actionBack(_ sender: Any) {
       pop()
    }
    
    
    @IBAction func sendMessageBtnAction(_ sender: Any) {
        
        if validation() {
            if isReplyToComment{
                self.addcomment(isReplyOnComment: true)
            }else{
                self.addcomment(isReplyOnComment: false)
            }
           
        }
    }
}

extension ReelsCommentVC: UITableViewDataSource, UITableViewDelegate, CommentDelegate {
    func profile(cell: TDCommentTableViewHeaderCell, index: Int) {
        let dict = postComment?[index]
        
        if dict?.user_type == "0"{
            //fan
            let vc = ProfileFanVC.instance(storyBoard: .Profile) as! ProfileFanVC
            vc.fanid = dict?.sender_id ?? ""
            vc.isSeenFanProfile = true
            push(viewController: vc)
        }else{
            //Artist
            let vc = ProfileVC.instance(storyBoard: .Profile) as! ProfileVC
            vc.userID = dict?.sender_id ?? ""
            vc.isComeFromFan = true
            push(viewController: vc)
        }
    }
    
   
  
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "TDCommentTableViewHeaderCell")  as! TDCommentTableViewHeaderCell
            let dict = postComment?[section]
            cell.lblUserName.text = dict?.full_name
            cell.lblComment.text = dict!.message
            cell.delegate = self
            cell.myIndex = section
           
                //like button
             if  dict?.is_comment_like  == 1{
                  cell.imgLike.image = UIImage(named: "like")
              }else{
                   cell.imgLike.image = UIImage(named: "unlike")
              }
             if dict?.total_like == "0"{
                cell.lblLike.text = (dict?.total_like)!  + " like"
              }
             else if dict?.total_like == "1"{
                cell.lblLike.text = (dict?.total_like)!  + " like"
              }else{
                cell.lblLike.text = (dict?.total_like)! + " likes"
              }
            
             cell.lblUserName.text = dict!.full_name
            cell.imgVCommentUserProfile.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
             cell.imgVCommentUserProfile.sd_setImage(with: URL(string:  ProfileImagePath + (dict?.profile_image)!), placeholderImage: UIImage(named: "user"))
            
            return cell
       
    }
    

    

    func numberOfSections(in tableView: UITableView) -> Int {
            return postComment?.count ?? 0
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (postComment?[section].reels_reply?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TDCommentTableViewCell")  as! TDCommentTableViewCell
        cell.delegate = self
        cell.myIndexRow = indexPath
            let dict = postComment?[indexPath.section].reels_reply?[indexPath.row]
                cell.lblComment.text = dict?.message ?? "N/A"
                cell.lblUserName.text = dict?.full_name  ?? "N/A"
           cell.imgVCommentUserProfile.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
        
        if  dict?.is_comment_like  == 1{
             cell.imgLike.image = UIImage(named: "like")
         }else{
              cell.imgLike.image = UIImage(named: "unlike")
         }
       
        if ((dict?.profile_image) != nil) {
            cell.imgVCommentUserProfile.sd_setImage(with: URL(string: ProfileImagePath  + (dict?.profile_image)! ), placeholderImage: UIImage(named: "user"))
        }
     
            return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TDCommentTableViewHeaderCell")  as! TDCommentTableViewHeaderCell
            let dict = postComment?[section]
            cell.lblComment.text = dict!.message
               //like button
            if  dict?.is_comment_like  == 1{
            cell.imgLike.image = UIImage(named: "like")
             }else{
                  cell.imgLike.image = UIImage(named: "unlike")
             }
        
            if dict?.total_like == "0"{
                cell.lblLike.text =   "\((dict?.total_like!)!) like"
             }
            else if dict?.total_like == "1"{
                cell.lblLike.text = (dict?.total_like!)! + " like"
             }else{
                cell.lblLike.text = (dict?.total_like!)! + " likes"
             }
           
            cell.lblUserName.text = dict!.full_name
            cell.imgVCommentUserProfile.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
     
       cell.imgVCommentUserProfile.sd_setImage(with: URL(string: ProfileImagePath + (dict?.profile_image)!), placeholderImage: UIImage(named: "user"))
            return cell.frame.size.height
        
    }
    private func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    

   
   
    

   
    
}





extension ReelsCommentVC:UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
    }
    
   
    func textViewDidChange(_ textView: UITextView) {
        let textHeight = textView.contentSize.height
        if textHeight < 100 {
            txtVConstraintHeight.constant = textHeight
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        if text != "Add a comment..." {
            btnSend.isEnabled = true
        }
               
        return true
    }

}




extension ReelsCommentVC: CommentOnCommentDelegate{
    
    func profile(cell: TDCommentTableViewCell, index: IndexPath) {
        let dict = postComment?[index.section].reels_reply?[index.row]
        
  
        
        if dict?.user_type == "0"{
            //fan
            let vc = ProfileFanVC.instance(storyBoard: .Profile) as! ProfileFanVC
            vc.fanid = dict?.sender_id ?? ""
            vc.isSeenFanProfile = true
            push(viewController: vc)
        }else{
            //Artist
            let vc = ProfileVC.instance(storyBoard: .Profile) as! ProfileVC
            vc.userID = dict?.sender_id ?? ""
            vc.isComeFromFan = true
            push(viewController: vc)
        }
    }
    
    func post_all_comments_API(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kreels_id] = post_id
        ServerManager.shared.POST(url: ApiAction.reels_all_comments , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(ReelsCommentModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
               
                self?.txtVComment.text = ""
                self?.stkReply.isHidden = true
                self?.postComment  = obj.reels_comments
                self?.tableViewComments.reloadData()
                if self!.isMoveToBottom{
                    self?.scrollToBottom()
                }
                self?.isMoveToBottom = false
                
            } else {
                print("failure")
               // presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    
    func  addcomment(isReplyOnComment : Bool){
        //---------
        var param = [String : Any]()
         //param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kreels_id] = post_id
        param[params.kmessage] = txtVComment.text
        param[params.ksender_id] = AppDataHelper.shard.logins.id
        
        if isReplyOnComment{
            if replyToRow == 0{
                let comment_id = postComment?[replyToIndex].comment_id
                param[params.kcomment_id] = comment_id
            }else{
                let comment_id = postComment?[replyToIndex].reels_reply?[replyToRow].comment_id
                param[params.kcomment_id] = comment_id
            }
           
        }
        
        ServerManager.shared.POST(url: ApiAction.add_reel_comment, param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.txtVComment.text = ""
                self?.isMoveToBottom = true
                self?.post_all_comments_API()
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
  
    }
    

    
    func addComment(cell: TDCommentTableViewHeaderCell, index: Int) {
        // FIXME: HIMANSHU PAL reply to particular comment
        stkReply.isHidden = false
        let dict = postComment?[index]
        lblReplyTo.text =  "Reply to: " + (dict?.full_name)!
        isReplyToComment = true
        replyToIndex = index
        replyToRow = 0
        self.txtVComment.becomeFirstResponder()
    }
    
    func likeComment(cell:  TDCommentTableViewHeaderCell, index: Int) {
        let comment_id = postComment?[index].comment_id
//        guard let celli = cell as? TDCommentTableViewHeaderCell else {
//            return
//        }
        
        //---------
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kcomment_id] = comment_id
        
        ServerManager.shared.POST(url: ApiAction.reels_comment_like , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.post_all_comments_API()
            /*    let liked = obj.is_comment_like
                let likes = obj.count
                self?.postComment?[index].is_comment_like =  obj.is_comment_like
                self?.postComment?[index].total_like =  obj.count
                
             if  liked  == 1{
                    cell.imgLike.image = UIImage(named: "like")
                }else{
                     cell.imgLike.image = UIImage(named: "unlike")
                }
                
                
                if likes == "0"{
                    cell.lblLike.text = likes!  + " like"
                 }
                else if likes == "1"{
                    cell.lblLike.text = likes! + " like"
                 }else{
                    cell.lblLike.text = likes! + " likes"
                 }
                
                self?.tableViewComments.reloadData()*/
                
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    //------------------------Row
    func addComment(cell: TDCommentTableViewCell, index: IndexPath) {
        stkReply.isHidden = false
        let dict = postComment?[index.section].reels_reply?[index.row]
        lblReplyTo.text =  "Reply to: " + (dict?.full_name)!
        isReplyToComment = true
        replyToIndex = index.section
        replyToRow = index.row
        self.txtVComment.becomeFirstResponder()
        
    }
    
    func likeComment(cell: TDCommentTableViewCell, index: IndexPath) {
        let comment_id = postComment?[index.section].reels_reply?[index.row].comment_id
//        guard let celli = cell as? TDCommentTableViewHeaderCell else {
//            return
//        }
        //---------
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kcomment_id] = comment_id
        
        ServerManager.shared.POST(url: ApiAction.reels_comment_like , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
         
            
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.post_all_comments_API()
             /*   let liked = obj.is_comment_like
                let likes = obj.count
                self?.postComment?[index.section].reels_reply?[index.row].is_comment_like =  obj.is_comment_like
                self?.postComment?[index.section].reels_reply?[index.row].total_like =  obj.count
                
             if  liked  == 1{
                    cell.imgLike.image = UIImage(named: "like")
                }else{
                     cell.imgLike.image = UIImage(named: "unlike")
                }
                
                
                if likes == "0"{
                    cell.lblLike.text = likes!  + " like"
                 }
                else if likes == "1"{
                    cell.lblLike.text = likes! + " like"
                 }else{
                    cell.lblLike.text = likes! + " likes"
                 }
                
                self?.tableViewComments.reloadData()*/
                
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
}









