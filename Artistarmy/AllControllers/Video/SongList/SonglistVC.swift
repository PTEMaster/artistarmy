import UIKit
import SDWebImage
import AVFoundation

class SonglistVC: KBaseViewController, TDImagePickerDelegate {
    @IBOutlet weak var tblSonglist: UITableView!
    let audioPlayerManager = AudioPlayerManager()
    var onDataAvailable : ((_ data: Songs ) -> ())?
    var imagepickerDocument:TDImagePicker!
    
    var arrSongs = [Songs]()
    var songId = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        imagepickerDocument = TDImagePicker(presentationController: self, delegate: self)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        stopPlaying(true)//deinit
        Get_songs_list_API()
    }
    override func viewWillDisappear(_ animated: Bool) {
            stopPlaying(true)
    }
    override func viewDidDisappear(_ animated: Bool) {
            stopPlaying(true)
    }
    
    @IBAction func actionDismissViewSong(_ sender: Any) {
            stopPlaying(true)
        pop()
       // self.dismiss(animated: true, completion: nil)
    }
    func didSelect(image: UIImage?) {
        let vc = CreateReelVC.instance(storyBoard: .Video) as! CreateReelVC
        vc.songId = songId
        vc.imaged = image
        push(viewController: vc)
       /* var param = [String : Any]()
        var img  = UIImageView()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        if  imgType == .profileImg{
            param[params.ktype] = "1"
            lmgProfile.image = image
            img = lmgProfile
        }else{
            lmgBgProfile.image = image
            param[params.ktype] = "2"
            img = lmgBgProfile
        }
*/
    }
    
}
extension SonglistVC: UITableViewDelegate , UITableViewDataSource {

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return  arrSongs.count
    }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SonglistCell", for: indexPath) as! SonglistCell
        let obj = arrSongs[indexPath.row]
        cell.lblSongtext.text = obj.song_name
        cell.lblArtistName.text = obj.singer_name
        
        cell.btnPlay.tag = indexPath.row
        cell.btnPlay.addTarget(self, action: #selector(playAudio(_:)), for: .touchUpInside)
            if obj.isPlay{
                cell.btnPlay.isSelected = true
            }else{
                cell.btnPlay.isSelected = false
            }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
  // replay()
       
        let obj = arrSongs[indexPath.row]
        songId = obj.id ?? ""
       // self.onDataAvailable?(obj)
            self.stopPlaying()
     //   pop()
        imagepickerDocument.present(from: self.view )
   }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    @objc func playAudio(_ sender: UIButton) {
      //  let ind = IndexPath(row: sender.tag, section: 0)
    //    let cell = tblSonglist.cellForRow(at: ind) as! SonglistCell
        let dict = self.arrSongs[sender.tag]
        for (index, _) in arrSongs.enumerated() {
            arrSongs[index].isPlay = false
        }
        arrSongs[sender.tag].isPlay = true
        stopPlaying()
        prepareAudio(from: dict.file_path!)
        tblSonglist.reloadData()
     }
    
    
}


extension SonglistVC {
    func Get_songs_list_API(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        ServerManager.shared.POST(url: ApiAction.songs_list , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SongListModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrSongs = obj.songs ?? []
                self?.tblSonglist.reloadData()
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
}



extension SonglistVC {
    // MARK:- Pre-audio
    func prepareAudio(from urlString: String) {
        if audioPlayerManager.isPlaying{
            stopPlaying()
        }
        let urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        print("urlString=\(urlString)=")
        if let url = URL(string: urlString!) {
            loderView.shared.showLoder()
            print("url-\(url)-")
            
            audioPlayerManager.prepareAudio(from: url) {
                print("prepareAudio--")
               /* if let totalDuration = self.audioPlayerManager.audioPlayer?.currentItem?.duration{
                    let durationSeconds = CMTimeGetSeconds(totalDuration)
                    let x = Double(durationSeconds).rounded(toPlaces: 2)
                    print("Total song duration--\(x)")
                }*/
                loderView.shared.hideLoder()
                self.audioPlayerManager.playAudio { (progress) in
                    print("playAudio--")
                    DispatchQueue.main.async {
                  print("progress.........................1......................\(progress)")
               }
                }
            }
        }
    }
    // MARK:- Pre-audio
    func stopPlaying(_ destroyPlayer: Bool = false) {
        audioPlayerManager.stopPlaying(destroyPlayer)
    }
    
   
       
     func checkAnswer()  {
    
         if audioPlayerManager.isPlaying {
//            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
//            })
            audioPlayerManager.stopPlaying()
        }
//show answer by local
       }
    
    func replay() {
                self.audioPlayerManager.replay { (progress) in
                    DispatchQueue.main.async {
                  print("progress.......................3........................\(progress)")
               }
                }
     //  }
    }
}





class SonglistCell: UITableViewCell {
    @IBOutlet weak var lblSongtext: UILabel!
    @IBOutlet weak var lblArtistName: UILabel!
    @IBOutlet weak var btnPlay: UIButton!
    
    
    
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
