//

// himanshu pal
//  Created by mac on 04/07/21.
//  Copyright © 2021 mac. All rights reserved.
//

import UIKit
import MediaPlayer
import AVFoundation
import AVKit
import SDWebImage
import MarqueeLabel




class AudioReelsVC: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnTrending: UIButton!
    @IBOutlet weak var btnforYou: UIButton!
    @IBOutlet weak var sliderView: UIView!
    @IBOutlet weak var lblStar: UILabel!
    @IBOutlet weak var lblHeart: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblreply: UILabel!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDiscription: UILabel!
    @IBOutlet weak var lblSongName: MarqueeLabel!
    @IBOutlet weak var imgVerified: UIImageView!
    @IBOutlet weak var UserImg: UIImageView!
    @IBOutlet weak var btnCreateReel: UIButton!
 //   lazy var items: [URL] = []
    
    var currentCellIndex:IndexPath?
    lazy var arrPosts = [Reels_list]()
    var isTrending = false
    @IBOutlet weak var imgStar: UIImageView!
    @IBOutlet weak var imgHeart: UIImageView!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    
    var page:Int = 0
    var isDownloading = false
    var pointContentOffset = CGPoint.zero
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.register(UINib(nibName: "AudioFeedCell", bundle: nil), forCellReuseIdentifier: "AudioFeedCell")
        self.tblView.sectionHeaderHeight = 0
        self.tblView.tableHeaderView?.frame = CGRect.zero
        tblView.reloadData()
       configureTableView()
     //   self.panelController.removeFromParent(transition: .fade, completion: nil)
        self.view.backgroundColor = UIColor.red
        self.tblView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell2")
        self.tabBarController?.tabBar.isHidden = false
 
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isTrending = false
        if AppDataHelper.shard.isFan{
            btnCreateReel.isHidden = true
        }else{
            btnCreateReel.isHidden = false
            }
       
        self.slideView(button: btnforYou)
        let recordingSession:AVAudioSession = AVAudioSession.sharedInstance()
        try! recordingSession.setCategory(AVAudioSession.Category.playAndRecord, options:.defaultToSpeaker)
        view1.isHidden = true
        view2.isHidden = true
        self.arrPosts.removeAll()
        self.tabBarController?.tabBar.isHidden = false
        page  = 0
        reels_list_Api()
    }
    override  func viewDidAppear(_ animated: Bool) {
           super.viewDidAppear(animated)
      
        self.tabBarController?.tabBar.isHidden = false
        self.slideView(button: btnforYou)
           MarqueeLabel.controllerViewDidAppear(self)
       }
    
    private func configureTableView() {

        self.tblView.rowHeight = UIScreen.main.bounds.height
        self.tblView.estimatedRowHeight = UIScreen.main.bounds.height
        self.tblView.separatorStyle = .none
        self.tblView.isPagingEnabled = true
        self.tblView.bounces = false
        self.tblView.estimatedSectionHeaderHeight = CGFloat.leastNormalMagnitude
        self.tblView.sectionHeaderHeight = CGFloat.leastNormalMagnitude
        self.tblView.estimatedSectionFooterHeight = CGFloat.leastNormalMagnitude
        self.tblView.sectionFooterHeight = CGFloat.leastNormalMagnitude
        self.tblView.contentInsetAdjustmentBehavior = .never
        self.tblView.delegate = self
        self.tblView.dataSource = self
     }


    
    @IBAction func actionTrending(_ sender: Any) {
        self.slideView(button: btnTrending)
        isTrending = true
        self.arrPosts.removeAll()
        page = 0
        reels_list_Api()
    }
    @IBAction func actionForYou(_ sender: Any) {
        isTrending = false
        self.slideView(button: btnforYou)
        self.arrPosts.removeAll()
        page = 0
        reels_list_Api()
      
        
       
    }
    
    @IBAction func actionCreatePost(_ sender: Any) {
          let vc = SonglistVC.instance(storyBoard: .Video) as! SonglistVC
          push(viewController: vc)
      }

    
    @IBAction func actionMoveToArtistProfile(_ sender: Any) {
        if let ind = tblView.indexPathsForVisibleRows {
                for index in ind {
                    let dict = self.arrPosts[index.row]
                    if AppDataHelper.shard.logins.id ==  dict.user_id!{
                        self.tabBarController?.selectedIndex = 4
                    }else{
                        let vc = ProfileVC.instance(storyBoard: .Profile) as! ProfileVC
                        vc.userID = dict.user_id!
                        vc.isComeFromFan = true
                        push(viewController: vc)
                    }
                }
        }
      }
    
    
    @IBAction func actionStar(_ sender: Any) {
        self.Reels_favourite_API()
    }
    
    @IBAction func actionHeart(_ sender: Any) {
        self.Reels_Like_API()
    }
    @IBAction func actionComment(_ sender: Any) {
        let dict = arrPosts[currentCellIndex!.row]
        let vc = ReelsCommentVC.instance(storyBoard: .Home) as! ReelsCommentVC
        vc.post_id = dict.id!
        push(viewController: vc)
        
        
    }
    @IBAction func actionShare(_ sender: Any) {
       let dict = arrPosts[currentCellIndex!.row]
            let vc = FriendShareListVC.instance(storyBoard: .Video) as! FriendShareListVC
            vc.reelId = dict.id!
            vc.artistId = dict.user_id!
            vc.enumShareThrough = .reels
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
        
    }
    
    func slideView(button: UIButton) {
        if button == self.btnTrending {
            
            btnforYou.setTitleColor(UIColor.white, for: .normal)
            btnTrending.setTitleColor(UIColor.black, for: .normal)
        } else {
            btnforYou.setTitleColor(UIColor.black, for: .normal)
            btnTrending.setTitleColor(UIColor.white, for: .normal)
        }
        UIView.animate(withDuration: 0.2) {
            self.sliderView.frame = CGRect(x: button.frame.origin.x, y: button.frame.origin.y, width: self.sliderView.frame.size.width, height: self.sliderView.frame.size.height)
        }
        self.view.layoutIfNeeded()
    }
}


extension AudioReelsVC  : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.arrPosts.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AudioFeedCell", for: indexPath) as! AudioFeedCell
            if self.arrPosts.count > 0{
           let dict = self.arrPosts[indexPath.row]
            cell.set(dict)
            if indexPath.row == (arrPosts.count - 1) && (tblView.contentOffset.y > pointContentOffset.y) {
                if !isDownloading {
                    isDownloading = true
                    if arrPosts.count % appDelegate.pageCount == 0 {
                        page += appDelegate.pageCount
                        self.reels_list_Api()
                    }
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.height
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? AudioFeedCell {
            print("-=-=-didEndDisplaying--)")
            cell.stopPlaying()
        }
    }
 func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        print("scrollViewDidEndDragging----\(decelerate)")
     /*   if !decelerate {
         //   check()
            
        }*/
    }
    
   
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating")
        check()
      
    }
    
    
    //MARK:- tableView

      func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            if  arrPosts.count > indexPath.row {
            let dict  = arrPosts[indexPath.row]
        let ind = IndexPath(row: indexPath.row, section: 0)
        currentCellIndex = ind
            self.lblStar.text = dict.total_star
            self.lblHeart.text =  dict.total_like
            self.lblComment.text =  dict.total_comment
            self.lblreply.text =  "0"//dict.string("")
            self.lblName.text =  dict.user_full_name
            self.lblDiscription.text =  dict.reel_description
        if dict.is_favourite == "0"{
            self.imgStar.tintColor = gradWhite
        }else{
            self.imgStar.tintColor = green
        }
        if dict.is_like == "0"{
            self.imgHeart.tintColor = gradWhite
        }else{
            self.imgHeart.tintColor = green
        }
        
            
            self.lblSongName.type = .continuous
            self.lblSongName.speed = .duration(10)
            self.lblSongName.fadeLength = 10.0
            self.lblSongName.trailingBuffer = 30.0
            self.lblSongName.text =  dict.song_name
            self.lblSongName.isUserInteractionEnabled = false
            self.lblSongName.font = UIFont.regularFont(size: 14)
            
            self.UserImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.UserImg.sd_setImage(with: URL(string: ProfileImagePath +  dict.user_profile_image!  ), placeholderImage: UIImage(named: "user"))
        }
       
     
        }
   
    override func viewWillDisappear(_ animated: Bool) {
      
        if currentCellIndex != nil{
            print("-----viewWillDisappear====")
            let cell = tblView.cellForRow(at: currentCellIndex!) as?  AudioFeedCell
                    cell?.stopPlaying(true)
        }
        
       
    }
    override func viewDidDisappear(_ animated: Bool) {
       
        if currentCellIndex != nil{
            print("-viewDidDisappear")
            let cell = tblView.cellForRow(at: currentCellIndex!) as?  AudioFeedCell
                    cell!.stopPlaying(true)
        }
    }
    

    
    func check() {
        let visibleCells = tblView.visibleCells.compactMap { $0 as? AudioFeedCell }
        
        
        guard visibleCells.count > 0 else { return }
        
        let visibleFrame = CGRect(x: 0, y: tblView.contentOffset.y, width: tblView.bounds.width, height: tblView.bounds.height)

        let visibleCell = visibleCells
            .filter { visibleFrame.intersection($0.frame).height >= $0.frame.height / 2 }
            .first
        
        visibleCell?.play()
    }
}
extension AudioReelsVC {
    func reels_list_Api(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kreels_id] =  appDelegate.reel_id
        param[params.kstart] =  page
        param[params.kpageCount] =   appDelegate.pageCount
        if isTrending{
            param[params.ktype] =  "1"
        }else{
            param[params.ktype] =  "2"
        }
        ServerManager.shared.POST(url: ApiAction.reels_list , param: param, false,false,header: nil) {   (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(FeedModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                appDelegate.reel_id = ""
                self.arrPosts.append(contentsOf: obj.reels_list!)

                self.tblView.reloadData()
                self.view1.isHidden = false
                self.view2.isHidden = false
                self.check()
                if obj.reels_list!.count == 0{
                    self.isDownloading = true
                }else{
                    self.isDownloading = false
                    }
            } else {
                
                print("failure")
                self.isDownloading = true
               // presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    
  func Reels_favourite_API(){
    var param = [String : Any]()
       

        let dict = arrPosts[currentCellIndex!.row]
        let id = dict.id
        param[params.kreels_id] = id
  
        param[params.kuser_id] = AppDataHelper.shard.logins.id
       
        
        ServerManager.shared.POST(url: ApiAction.reels_favourite , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(isfavouriteModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
               // let cell = self?.tblView.cellForRow(at: (self?.currentCellIndex!)!) as?  FeedCell
                
                self?.lblStar.text = String(obj.count!)
                
                    var dict = self?.arrPosts[ (self?.currentCellIndex!.row)!]
                    dict?.total_star = String(obj.count!)
                    if dict?.is_favourite == "0"{
                        self?.imgStar.tintColor = green
                        dict?.is_favourite = "1"
                    }else{
                        self?.imgStar.tintColor = gradWhite
                        dict?.is_favourite = "0"
                    }
                    self?.arrPosts.remove(at: (self?.currentCellIndex!.row)!)
                    self?.arrPosts.insert(dict!, at: (self?.currentCellIndex!.row)!)
                
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    
    
    func Reels_Like_API(){
        var param = [String : Any]()
     
            let dict = arrPosts[currentCellIndex!.row]
            let id = dict.id
            param[params.kreels_id] = id
       
         
          param[params.kuser_id] = AppDataHelper.shard.logins.id
          
          ServerManager.shared.POST(url: ApiAction.reels_like , param: param, true,header: nil) { [weak self]  (data, error) in
              guard let data = data else {
                  print("data not available")
                  return
              }
              guard  let obj = try? JSONDecoder().decode(isfavouriteModel.self, from: data) else {
                  return
              }
              if obj.code == ResponseApis.KSuccess {
                 // let cell = self?.tblView.cellForRow(at: (self?.currentCellIndex!)!) as?  FeedCell
                  self?.lblHeart.text = String(obj.count!)
                    var dict = self?.arrPosts[(self?.currentCellIndex!.row)!]
                    dict?.total_like = String(obj.count!)
                    if dict?.is_like == "0"{
                      self?.imgHeart.tintColor = green
                        dict?.is_like = "1"
                  }else{
                      self?.imgHeart.tintColor = gradWhite
                    dict?.is_like = "0"
                  }
                  self?.arrPosts.remove(at: (self?.currentCellIndex!.row)!)
                    self?.arrPosts.insert(dict!, at: (self?.currentCellIndex!.row)!)
                
               
              } else {
                  print("failure")
                  presentAlert("", msgStr: obj.message, controller: self)
              }
          }
      }
    
    
    
}





