//
//  FeedCell.swift
//  GSPlayer_Example
//
//  Created by Gesen on 2020/5/17.
//  Copyright © 2020 Gesen. All rights reserved.
//

import GSPlayer
import AVFoundation
import SDWebImage

class AudioFeedCell: UITableViewCell {

    @IBOutlet weak var imgSong: UIImageView!
    @IBOutlet weak var playerView: UIView!
    let audioPlayerManager = AudioPlayerManager()
    private var url: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
       
    }
    
    func set(_ dict: Reels_list) {
   
        url = dict.song_url
        imgSong.sd_imageIndicator = SDWebImageActivityIndicator.gray
     //   print(VideoImagePath +  dict.reel_image!)
        print("---------------------------------------------------------")
        imgSong.sd_setImage(with: URL(string: VideoImagePath +  dict.reel_image! ), placeholderImage: UIImage(named: ""))
    }
    
    func play() {
//        print("audioPlayerManager.audioPlayer\(audioPlayerManager.audioPlayer)" )
       // playerView.play(for: url)
        prepareAudio(from: url)
    }
    
   
}


extension AudioFeedCell {
    // MARK:- Pre-audio
    func prepareAudio(from urlString: String) {
        if audioPlayerManager.isPlaying{
            stopPlaying()
        }
        let urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        if let url = URL(string: urlString!) {
            loderView.shared.showLoder()
            
            audioPlayerManager.prepareAudio(from: url) {
                loderView.shared.hideLoder()

                self.audioPlayerManager.playAudio { (progress) in
                    DispatchQueue.main.async {
                        let y = Double(progress).rounded(toPlaces: 2)
                //  print("progress.........................1......................\(y)")
                        if y == 1{
                            self.replay()
                        }
                        
               }
                }
                isTabed = true
                
              
            }
        }
    }
    // MARK:- Pre-audio
    func stopPlaying(_ destroyPlayer: Bool = false) {
        if destroyPlayer{
          //  show(message: "Player stop")
            
        }
        audioPlayerManager.stopPlaying(destroyPlayer)
    }
    
    func replay() {
                self.audioPlayerManager.replay { (progress) in
                    DispatchQueue.main.async {
    //  print("progress.......................3........................\(progress)")
                        
               }
                }
     //  }
    }
    

}
