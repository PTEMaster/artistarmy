

import UIKit
import AVFoundation
var isTabed = false
class VideoReelVC: SwiftyCamViewController, SwiftyCamViewControllerDelegate {
    
    @IBOutlet weak var captureButton    : SwiftyRecordButton!
    @IBOutlet weak var flipCameraButton : UIButton!
    @IBOutlet weak var flashButton      : UIButton!
    @IBOutlet weak var viewAudio      : UIView!
    @IBOutlet weak var progressBar: CircularProgressBar!
    //
    let audioPlayerManager = AudioPlayerManager()
    var audioUrl = ""
    
	override func viewDidLoad() {
		super.viewDidLoad()
       shouldPrompToAppSettings = true
		cameraDelegate = self
		maximumVideoDuration = 30.0
        shouldUseDeviceOrientation = true
        allowAutoRotate = true
        audioEnabled = true
        flashMode = .auto
        flashButton.setImage(#imageLiteral(resourceName: "flashauto"), for: UIControl.State())
        captureButton.buttonEnabled = false
        audioUrl = ""
        videoQuality = .medium
        
        self.tabBarController?.tabBar.isHidden = true
       
	}
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        isTabed = false
        captureButton.growButton()
    }

	override var prefersStatusBarHidden: Bool {
		return true
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
       captureButton.delegate = self
	}
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
        stopPlaying(true)
        print(isSessionRunning)
        progressBar.wholeCircleAnimationDuration = 0
        progressBar.setProgress(to: 0, withAnimation: true)
    //    self.cameraDelegate?.swiftyCamSessionDidStopRunning(self)
    }
    override func viewDidDisappear(_ animated: Bool) {
       stopPlaying(true)
        
    }

    
    @IBAction func actionBack(_ sender: Any) {
        pop()
    }
    
    @IBAction func actionViewSong(_ sender: Any) {
        let vc = SonglistVC.instance(storyBoard: .Video) as! SonglistVC
        vc.onDataAvailable = {[weak self]
                        (data) in
                        if let weakSelf = self {
                            weakSelf.getAudioData(data: data )
                        }
                    }
        push(viewController: vc)
       // self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:-  get audio data
    func getAudioData(data: Songs ) {
        loderView.shared.showLoder()
        audioEnabled = false
        audioUrl = data.file_path ?? ""
        let urlString = audioUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        if let url = URL(string: urlString!) {
        audioPlayerManager.prepareAudio(from: url) {
           if let totalDuration = self.audioPlayerManager.audioPlayer?.currentItem?.duration{
                let durationSeconds = CMTimeGetSeconds(totalDuration)
                let x = Double(durationSeconds).rounded(toPlaces: 2)
                print("Total song duration--\(x)")
            loderView.shared.hideLoder()
            self.maximumVideoDuration =  x //+ 2
            }
        }
        }
        
        }
    
   //
    
    func swiftyCamSessionDidStartRunning(_ swiftyCam: SwiftyCamViewController) {
        print("Session did start running")
        captureButton.buttonEnabled = true
    }
    
    func swiftyCamSessionDidStopRunning(_ swiftyCam: SwiftyCamViewController) {
        print("Session did stop running")
        captureButton.buttonEnabled = false
    }
    

	func swiftyCam(_ swiftyCam: SwiftyCamViewController, didTake photo: UIImage) {
        print("didTake photo")
		//let newVC = PhotoViewController(image: photo)
		//self.present(newVC, animated: true, completion: nil)
	}

	func swiftyCam(_ swiftyCam: SwiftyCamViewController, didBeginRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
		print("Did Begin Recording ---\n\(audioUrl)-")
        print("Audio duration --\( maximumVideoDuration)")
        if    audioUrl == ""{
            audioEnabled = true
            isTabed = true
            self.captureButton.growButton()
            self.hideButtons()
        }else{
            audioEnabled = false
            self.captureButton.growButton()
            self.hideButtons()
            self.audioPlayerManager.playAudio { (progress) in
                DispatchQueue.main.async {
            //  print("progress.........................2......................\(progress)")
           }
            }
        }
        progressBar.safePercent = 100
        progressBar.lineColor = .red
        progressBar.lineFinishColor = .red
        progressBar.lineBackgroundColor = .lightGray
        progressBar.wholeCircleAnimationDuration = maximumVideoDuration
        progressBar.setProgress(to: 100, withAnimation: true)
       
        
	}

	func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
		print("Did finish Recording-=-=-=hp")
	}

	func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishProcessVideoAt url: URL) {
		print("videoUrl------ \(url)")
        print(audioUrl)
        stopPlaying(true)
        isTabed = false
        captureButton.growButton()
        showButtons()
        let urlString = audioUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let audiourl = URL(string: urlString!)
        let vc = CreateReelVC.instance(storyBoard: .Video) as! CreateReelVC
        vc.audioUrl = audiourl
        vc.videoUrl = url
        push(viewController: vc)
        
	}

	func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFocusAtPoint point: CGPoint) {
        print("Did focus at point: \(point)")
        focusAnimationAt(point)
	}
    
    func swiftyCamDidFailToConfigure(_ swiftyCam: SwiftyCamViewController) {
        let message = NSLocalizedString("Unable to capture media", comment: "Alert message when something goes wrong during capture session configuration")
        let alertController = UIAlertController(title: "AVCam", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
    }

	func swiftyCam(_ swiftyCam: SwiftyCamViewController, didChangeZoomLevel zoom: CGFloat) {
        print("Zoom level did change. Level: \(zoom)")
		print(zoom)
	}

	func swiftyCam(_ swiftyCam: SwiftyCamViewController, didSwitchCameras camera: SwiftyCamViewController.CameraSelection) {
        print("Camera did change to \(camera.rawValue)")
		print(camera)
	}
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFailToRecordVideo error: Error) {
        print(error)
    }

    @IBAction func cameraSwitchTapped(_ sender: Any) {
        switchCamera()
    }
    
    @IBAction func toggleFlashTapped(_ sender: Any) {
        //flashEnabled = !flashEnabled
        toggleFlashAnimation()
    }
}


// UI Animations
extension VideoReelVC {
    
    fileprivate func hideButtons() {
        UIView.animate(withDuration: 0.25) {
            self.flashButton.alpha = 0.0
            self.flipCameraButton.alpha = 0.0
            self.viewAudio.alpha = 0.0
        }
    }
    
    fileprivate func showButtons() {
        UIView.animate(withDuration: 0.25) {
            self.flashButton.alpha = 1.0
            self.flipCameraButton.alpha = 1.0
            self.viewAudio.alpha = 1.0
        }
    }
    
    fileprivate func focusAnimationAt(_ point: CGPoint) {
        let focusView = UIImageView(image: #imageLiteral(resourceName: "focus"))
        focusView.center = point
        focusView.alpha = 0.0
        view.addSubview(focusView)
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
            focusView.alpha = 1.0
            focusView.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
        }) { (success) in
            UIView.animate(withDuration: 0.15, delay: 0.5, options: .curveEaseInOut, animations: {
                focusView.alpha = 0.0
                focusView.transform = CGAffineTransform(translationX: 0.6, y: 0.6)
            }) { (success) in
                focusView.removeFromSuperview()
            }
        }
    }
    
    fileprivate func toggleFlashAnimation() {
        //flashEnabled = !flashEnabled
        if flashMode == .auto{
            flashMode = .on
            flashButton.setImage(#imageLiteral(resourceName: "flash"), for: UIControl.State())
        }else if flashMode == .on{
            flashMode = .off
            flashButton.setImage(#imageLiteral(resourceName: "flashOutline"), for: UIControl.State())
        }else if flashMode == .off{
            flashMode = .auto
            flashButton.setImage(#imageLiteral(resourceName: "flashauto"), for: UIControl.State())
        }
    }
}





extension VideoReelVC {
    // MARK:- Pre-audio
    func prepareAudio(from urlString: String) {
        if audioPlayerManager.isPlaying{
            stopPlaying()
        }
        let urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        if let url = URL(string: urlString!) {
            loderView.shared.showLoder()
            
            audioPlayerManager.prepareAudio(from: url) {
                loderView.shared.hideLoder()

                self.audioPlayerManager.playAudio { (progress) in
                    DispatchQueue.main.async {
                  print("progress...................2............................\(progress)")
               }
                }
                isTabed = true
                
              
            }
        }
    }
    // MARK:- Pre-audio
    func stopPlaying(_ destroyPlayer: Bool = false) {
        audioPlayerManager.stopPlaying(destroyPlayer)
    }
    

}
