//
//  CreatePostVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit
import SDWebImage
import UIKit
import AVFoundation
import AVKit
import AssetsLibrary

class CreateReelVC: KBaseViewController ,UITextViewDelegate{

    @IBOutlet weak var tvWhatsYourMind: KMPlaceholderTextView!
    var audioUrl : URL?
    var videoUrl : URL?
    var finalUrl : URL?
    
    
    var songId : String?
    var imaged : UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       /*
    //  print("audioUrl-\(audioUrl)-")
   //    print("videoUrl-\(videoUrl)-")
        loderView.shared.showLoder()
        if audioUrl != nil{
            mergeVideoWithAudio(videoUrl: videoUrl!, audioUrl: audioUrl! ) { (videoUrl) in
                print(videoUrl)
                DispatchQueue.main.async {
                    self.finalUrl = videoUrl
                    loderView.shared.hideLoder()
                   
                let newVC = VideoViewController(videoURL: videoUrl)
                self.present(newVC, animated: true, completion: nil)
                }
                
            } failure: { (error) in
                loderView.shared.hideLoder()
                Artistarmy.show(message: "Video not uploaded")
                print(error!)
            }
            
        }else{
            loderView.shared.hideLoder()
            finalUrl = videoUrl!
           let newVC = VideoViewController(videoURL: videoUrl!)
            self.present(newVC, animated: true, completion: nil)
        }
    
*/
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    @IBAction func actionBack(_ sender: Any) {
        pop()
    }
    
    @IBAction func actionShare(_ sender: Any) {
       // self.Add_reelst_API()
        self.uploadImgSongID()
       
    }
    func mergeVideoWithAudio(videoUrl: URL,
                                    audioUrl: URL,
                                    success: @escaping ((URL) -> Void),
                                    failure: @escaping ((Error?) -> Void)) {

           let mixComposition: AVMutableComposition = AVMutableComposition()
           var mutableCompositionVideoTrack: [AVMutableCompositionTrack] = []
           var mutableCompositionAudioTrack: [AVMutableCompositionTrack] = []
           let totalVideoCompositionInstruction: AVMutableVideoCompositionInstruction = AVMutableVideoCompositionInstruction()

           let aVideoAsset: AVAsset = AVAsset(url: videoUrl)
           let aAudioAsset: AVAsset = AVAsset(url: audioUrl)

           if let videoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid), let audioTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid) {
               mutableCompositionVideoTrack.append( videoTrack )
               mutableCompositionAudioTrack.append( audioTrack )

               if let aVideoAssetTrack: AVAssetTrack = aVideoAsset.tracks(withMediaType: .video).first, let aAudioAssetTrack: AVAssetTrack = aAudioAsset.tracks(withMediaType: .audio).first {
                   do {
                       try mutableCompositionVideoTrack.first?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aVideoAssetTrack, at: CMTime.zero)

                       let videoDuration = aVideoAsset.duration
                       if CMTimeCompare(videoDuration, aAudioAsset.duration) == -1 {
                           try mutableCompositionAudioTrack.first?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aAudioAssetTrack, at: CMTime.zero)
                       } else if CMTimeCompare(videoDuration, aAudioAsset.duration) == 1 {
                           var currentTime = CMTime.zero
                           while true {
                               var audioDuration = aAudioAsset.duration
                               let totalDuration = CMTimeAdd(currentTime, audioDuration)
                               if CMTimeCompare(totalDuration, videoDuration) == 1 {
                                   audioDuration = CMTimeSubtract(totalDuration, videoDuration)
                               }
                               try mutableCompositionAudioTrack.first?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aAudioAssetTrack, at: currentTime)

                               currentTime = CMTimeAdd(currentTime, audioDuration)
                               if CMTimeCompare(currentTime, videoDuration) == 1 || CMTimeCompare(currentTime, videoDuration) == 0 {
                                   break
                               }
                           }
                       }
                       videoTrack.preferredTransform = aVideoAssetTrack.preferredTransform

                   } catch {
                       print(error)
                   }

                   totalVideoCompositionInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration)
               }
           }

           let mutableVideoComposition: AVMutableVideoComposition = AVMutableVideoComposition()
           mutableVideoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
           mutableVideoComposition.renderSize = CGSize(width: 480, height: 640)

           if let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
               let outputURL = URL(fileURLWithPath: documentsPath).appendingPathComponent("\("fileName").mp4")

               do {
                   if FileManager.default.fileExists(atPath: outputURL.path) {

                       try FileManager.default.removeItem(at: outputURL)
                   }
               } catch { }

               if let exportSession = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality) {
                   exportSession.outputURL = outputURL
                   exportSession.outputFileType = AVFileType.mp4
                   exportSession.shouldOptimizeForNetworkUse = true

                   // try to export the file and handle the status cases
                   exportSession.exportAsynchronously(completionHandler: {
                       switch exportSession.status {
                       case .failed:
                           if let error = exportSession.error {
                               failure(error)
                           }

                       case .cancelled:
                           if let error = exportSession.error {
                               failure(error)
                           }

                       default:
                           print("finished")
                           success(outputURL)
                       }
                   })
               } else {
                   failure(nil)
               }
           }
       }
    
}




extension CreateReelVC {
    func Add_reelst_API(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        
        param[params.reel_description ] = tvWhatsYourMind.text
        var arrVideo = [[String:Any]]()
        arrVideo.append([params.reel_video:finalUrl!])
        ServerManager.shared.VideoUpload(url: ApiAction.add_reels, params: param, imageParams: arrVideo, isVideoUpload: true) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.popToRoot()
                Artistarmy.show(message: obj.message!)
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    
    func uploadImgSongID (){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.reel_description ] = tvWhatsYourMind.text
        param[params.reel_songs ] = songId
        let img  = UIImageView()
        img.image = imaged
        
        ServerManager.shared.POSTWithImage(url: ApiAction.add_reels, param: param, imgParam: "reel_image", imageView: img) { (data, error) in
            guard let data = data else {
                  print("data not available")
                  return
              }
             guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                  return
              }
              if obj.code == ResponseApis.KSuccess {
                self.popToRoot()
                Artistarmy.show(message: obj.message!)
              } else {
                  print("failure")
                  presentAlert("", msgStr: obj.message, controller: self)
              }
          }
    }
}

