//
//  FeedCell.swift
//  GSPlayer_Example
//
//  Created by Gesen on 2020/5/17.
//  Copyright © 2020 Gesen. All rights reserved.
//

import GSPlayer

class FeedCell: UITableViewCell {

    @IBOutlet weak var playerView: VideoPlayerView!
    
    private var url: URL!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    //   print( "playerView.volume\(playerView.volume)")
        playerView.volume = 1.0
        playerView.stateDidChanged = { state in
            switch state {
            case .none:
                print("none")
            case .error(let error):
                print("error - \(error.localizedDescription)")
                Loader.hideLoader()
                loderView.shared.hideLoder()
            case .loading:
                print("loading")
           //     Loader.showLoader()
                loderView.shared.showLoder()
            case .paused(let playing, let buffering):
                print("paused-=-=-==-===-==")
             //   print("paused - progress \(Int(playing * 100))% buffering \(Int(buffering * 100))%")
            case .playing:
                print("playing")
                loderView.shared.hideLoder()
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        playerView.isHidden = true
    }
    
    func set(url: URL) {
        self.url = url
    }
    
    func play() {
        playerView.volume = 1
        playerView.play(for: url)
        playerView.isHidden = false
    }
    
    func pause() {
        playerView.pause(reason: .hidden)
    }
}
