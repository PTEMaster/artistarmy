//
//  HomeVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit
import SDWebImage

class NotificationVC: KBaseViewController {
    @IBOutlet weak var tblNotification: UITableView!
    var page:Int = 0
    var isDownloading = false
    var pointContentOffset = CGPoint.zero
    
    
    var arrNotification = [NotificationList]()
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        Get_notification_API()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
}
extension NotificationVC: UITableViewDelegate , UITableViewDataSource {
    func convertDateFormat(inputDate: String) -> String {

         let olDateFormatter = DateFormatter()
         olDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

         let oldDate = olDateFormatter.date(from: inputDate)

         let convertDateFormatter = DateFormatter()
         convertDateFormatter.dateFormat = "h:mm a"

         return convertDateFormatter.string(from: oldDate!)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        arrNotification.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 
        if arrNotification[section].data?.count ?? 0 > 0 {
            return  arrNotification[section].data?.count ?? 0
       }
        return  0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotifictionCell", for: indexPath) as! NotifictionCell
        let obj = arrNotification[indexPath.section].data
        let data = obj![indexPath.row]
        cell.lbltext.text = data.message
        let date =   convertDateFormat(inputDate: data.created_time!)
        cell.lblDate.text = date
        cell.imgUser.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgUser.sd_setImage(with: URL(string: ProfileImagePath  ), placeholderImage: UIImage(named: "user"))
        
        if indexPath.row == (arrNotification.count - 1) && (tblNotification.contentOffset.y > pointContentOffset.y) {
            if !isDownloading {
                isDownloading = true
                if arrNotification.count % appDelegate.pageCount == 0 {
                    page += appDelegate.pageCount
                    self.Get_notification_API()
                }
            }
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = arrNotification[indexPath.section].data
        let data = obj![indexPath.row]
        if data.type == "1"{
            appDelegate.post_id = data.refer_id!
            self.tabBarController?.selectedIndex = 0
         
          /*  let vc = CommentViewController.instance(storyBoard: .Home) as! CommentViewController
            vc.arrPostimages = []
            vc.post_id = data.refer_id!
            push(viewController: vc)*/
            
            
        }else if data.type == "2"{
            appDelegate.reel_id = data.refer_id!
            self.tabBarController!.selectedIndex = 2
        }
        else if data.type == "3"{
            appDelegate.ref_user_id = data.refer_id!
            appDelegate.ref_artist_id = data.ref_artist_id!
            self.tabBarController?.selectedIndex = 0
            
            
        }
        else if data.type == "4"{
            if data.user_type == "0"{
                //fan
                let vc = ProfileFanVC.instance(storyBoard: .Profile) as! ProfileFanVC
                vc.fanid = data.refer_id!
                vc.isSeenFanProfile = true
                push(viewController: vc)
            }else{
                //Artist
                let vc = ProfileVC.instance(storyBoard: .Profile) as! ProfileVC
                vc.userID = data.refer_id!
                vc.isComeFromFan = true
                push(viewController: vc)
            }
            
        }
        
        
   
   }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        let label = UILabel(frame: CGRect(x: 10, y: 15, width: tableView.frame.size.width, height: 18))
    //    label.backgroundColor = .red
        label.font = UIFont.semiBoldFont(size: 15)
        //
        
        
        label.text = convertDateString(dateString: arrNotification[section].date , fromFormat: "yyyy-MM-dd", toFormat: "MM-dd-yyyy") 
        view.backgroundColor = .white
        view.addSubview(label)
        return view
    }
   
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
}


extension NotificationVC {
    func Get_notification_API(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kstart] =  page
        param[params.kpageCount] =   appDelegate.pageCount
        
        ServerManager.shared.POST(url: ApiAction.get_notification_by_date , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(NotificationModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
           //     self?.arrNotification = obj.notification ?? []
                self?.arrNotification.append(contentsOf: obj.notification!)
                
                self?.tblNotification.reloadData()
                if obj.notification!.count == 0{
                    self?.isDownloading = true
                }else{
                    self?.isDownloading = false
                }
            } else {
                print("failure")
                self?.isDownloading = true
            }
        }
    }
}



class NotifictionCell: UITableViewCell {
    @IBOutlet weak var lbltext: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    
    
    
}
