/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct NotificationList : Codable {
    let date : String?
    let data : [Dataa]?

    enum CodingKeys: String, CodingKey {

        case date = "date"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        data = try values.decodeIfPresent([Dataa].self, forKey: .data)
    }

}

struct Dataa : Codable {
    let notification_id : String?
    let message : String?
    let user_id : String?
    let created_time : String?
    let created_at : String?
    let type : String?
    let refer_id : String?
    
    let ref_artist_id : String?
    let type_name : String?
    let user_type : String?
    
    

    
    enum CodingKeys: String, CodingKey {

        case notification_id = "notification_id"
        case message = "message"
        case user_id = "user_id"
        case created_time = "created_time"
        case created_at = "created_at"
        case type = "type"
        case refer_id = "refer_id"
        
        case ref_artist_id  = "ref_artist_id"
        case type_name = "type_name"
        case user_type = "user_type"
        
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        notification_id = try values.decodeIfPresent(String.self, forKey: .notification_id)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        created_time = try values.decodeIfPresent(String.self, forKey: .created_time)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        refer_id = try values.decodeIfPresent(String.self, forKey: .refer_id)
        ref_artist_id  = try values.decodeIfPresent(String.self, forKey: .ref_artist_id)
        type_name  = try values.decodeIfPresent(String.self, forKey: .type_name)
        user_type  = try values.decodeIfPresent(String.self, forKey: .user_type)
    }

}
