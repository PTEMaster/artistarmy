//
//  CreatePostVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit
import SDWebImage
import YPImagePicker
import AVFoundation
import AVKit
import Photos
// TDImagePickerDelegate
class CreatePostVC: KBaseViewController  {
    
    @IBOutlet weak var cvImg: UICollectionView!
    var arrImg = [UIImage]()
    var imagepickerDocument:TDImagePicker!
    let demo = UIImage(named: "add")
    
    @IBOutlet weak var tvDescription: KMPlaceholderTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     //   imagepickerDocument = TDImagePicker(presentationController: self, delegate: self)
        arrImg.append(demo!)
        cvImg.delegate = self
        cvImg.dataSource = self
        let margin: CGFloat = 10
        guard let collectionView = cvImg, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.minimumInteritemSpacing = margin
        flowLayout.minimumLineSpacing = 10
        flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
    }
    
    
    @IBAction func actionPostVideo(_ sender: Any) {
        // imagepickerDocument.present(from: sender as! UIView)
        self.showPicker()
    }
    @IBAction func actionPost(_ sender: Any) {
        cvImg.reloadData()
        
       AddPost_API()
    }
   
   
    @IBAction func actionBack(_ sender: Any) {
        pop()
    }
    
//    func didSelect(image: UIImage?) {
//        print(arrImg.count)
//        //  images.remove(at: images.count-1)
//        arrImg.remove(at: arrImg.count-1)
//        arrImg.append(image!)
//        arrImg.append(demo!)
//        cvImg.reloadData()
//
//    }
}



extension CreatePostVC: UICollectionViewDataSource , UICollectionViewDelegateFlowLayout, createPostImagePickerDeleteDelegate{
    func cellRemoveImages(_ indexPath: IndexPath) {
        self.arrImg.remove(at: indexPath.row)
        if let dict:UIImage = self.arrImg.last as? UIImage {
            if demo != dict {
                arrImg.append(demo!)
            }
        }
        cvImg.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImg.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionsCell", for: indexPath) as! CollectionsCell
        cell.delegate = self
        cell.indexPath = indexPath
        let dict = arrImg[indexPath.row]
        cell.img.image = dict
        cell.btnDeleteImg.layer.cornerRadius = 12
        if let dict:UIImage = self.arrImg[indexPath.item] as? UIImage {
            if demo == dict {
                cell.btnDeleteImg.isHidden = true
            }else {
                cell.btnDeleteImg.isHidden = false
            }
        }
        cell.img.layer.cornerRadius = 10
        cell.img.clipsToBounds = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let dict:UIImage = self.arrImg[indexPath.item] as? UIImage {
            if demo == dict {
                actionPostVideo(collectionView)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let noOfCellsInRow = 3   //number of column you want
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
        
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size, height: size )
    }
}


extension CreatePostVC {
    func showPicker() {
        
        var config = YPImagePickerConfiguration()
        
        config.library.mediaType = .photo
        
        config.shouldSaveNewPicturesToAlbum = false
        config.video.compression = AVAssetExportPresetMediumQuality
        config.startOnScreen = .library
        config.screens = [.library, .photo] //, .video
        config.video.libraryTimeLimit = 60.0//500.0
        config.showsCrop =  .none //.rectangle(ratio: (16/9))
        config.wordings.libraryTitle = "Gallery"
          config.colors.tintColor = .red // Right bar buttons (actions)
       config.bottomMenuItemSelectedTextColour = .red
        // config.bottomMenuItemUnSelectedTextColour = .green
        
        config.hidesStatusBar = false
        config.hidesBottomBar = false
        config.maxCameraZoomFactor = 2.0
        config.library.maxNumberOfItems = 1
        config.gallery.hidesRemoveButton = false
        config.library.defaultMultipleSelection = false
        
        //--
        /*    config.isScrollToChangeModesEnabled = true
         config.onlySquareImagesFromCamera = true
         config.usesFrontCamera = false
         config.showsPhotoFilters = true
         config.showsVideoTrimmer = true
         config.shouldSaveNewPicturesToAlbum = true
         config.albumName = "DefaultYPImagePickerAlbumName"
         config.startOnScreen = YPPickerScreen.photo
         config.screens = [.library, .photo]
         config.showsCrop = .none
         config.targetImageSize = YPImageSize.original
         config.overlayView = UIView()
         config.hidesStatusBar = true
         config.hidesBottomBar = false
         config.hidesCancelButton = false
         config.preferredStatusBarStyle = UIStatusBarStyle.default
         // config.bottomMenuItemSelectedColour = UIColor(r: 38, g: 38, b: 38)
         // config.bottomMenuItemUnSelectedColour = UIColor(r: 153, g: 153, b: 153)
         config.maxCameraZoomFactor = 1.0
         config.library.preSelectItemOnMultipleSelection = true
         config.library.defaultMultipleSelection = true
         //config.library.options = nil
         config.library.onlySquare = false
         config.library.isSquareByDefault = true
         config.library.minWidthForItem = nil
         config.library.mediaType = YPlibraryMediaType.photo
         config.library.defaultMultipleSelection = false
         config.library.maxNumberOfItems = 1
         config.library.minNumberOfItems = 1
         config.library.numberOfItemsInRow = 4
         config.library.spacingBetweenItems = 1.0
         config.library.skipSelectionsGallery = false
         config.library.preselectedItems = nil
         //
         config.video.compression = AVAssetExportPresetHighestQuality
         config.video.fileType = .mov
         config.video.recordingTimeLimit = 60.0
         config.video.libraryTimeLimit = 60.0
         config.video.minimumTimeLimit = 3.0
         config.video.trimmerMaxDuration = 60.0
         config.video.trimmerMinDuration = 3.0
         
         //--*/
        
        let picker = YPImagePicker(configuration: config)
        picker.navigationBar.tintColor = UIColor.white
        picker.navigationBar.barTintColor = UIColor(named: "colorBaseApp")
        picker.navigationBar.isTranslucent = false

        picker.didFinishPicking { [unowned picker] items, cancelled in
            
            if cancelled {
                print("Picker was canceled")
                picker.dismiss(animated: true, completion: nil)
                return
            }
          _ = items.map { print("🧀 \($0)") }
            for dd in items{
                switch dd {
                case .photo(let photo):
                   // self.arrImg.append(photo.image)
                    self.arrImg.insert(photo.image, at: 0)
                   
                    self.dismiss(animated: true, completion: nil)
                    
                case .video(let video):
                    
                    let assetURL = video.url
                    let playerVC = AVPlayerViewController()
                    let player = AVPlayer(playerItem: AVPlayerItem(url:assetURL))
                    playerVC.player = player
                    
                    picker.dismiss(animated: true, completion: { [weak self] in
                        self?.present(playerVC, animated: true, completion: nil)
                        print("😀 \(String(describing: self?.resolutionForLocalVideo(url: assetURL)!))")
                    })
                }
                self.cvImg.reloadData()
            }
            
            if   self.arrImg.count == 4{
                self.arrImg.removeLast()
            }
        }
        present(picker, animated: true, completion: nil)
    }
    
    func resolutionForLocalVideo(url: URL) -> CGSize? {
        guard let track = AVURLAsset(url: url).tracks(withMediaType: AVMediaType.video).first else { return nil }
        let size = track.naturalSize.applying(track.preferredTransform)
        return CGSize(width: abs(size.width), height: abs(size.height))
    }
}




extension CreatePostVC {
    func AddPost_API(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kpost_type] = "images"
        param[params.kdescription] = tvDescription.text
        param[params.kimage] = ""
        var aa =  arrImg
        if   aa.count != 3{
            aa.removeLast()
        }
       
        ServerManager.shared.MultipleImageUpload(url: ApiAction.add_post , params: param, imageParams: aa, isVideoUpload: false) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SignupModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                DispatchQueue.main.async(execute: {
                    let _ = presentAlertWithOptions("", message: obj.message!, controller: self, buttons: ["Ok"]) { (alert, actionTag) in
                        if actionTag == 0 {
                            self?.pop()
                        }
                    }
                })
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
}









protocol createPostImagePickerDeleteDelegate:NSObject {
    func cellRemoveImages(_ indexPath:IndexPath)
}

class CollectionsCell: UICollectionViewCell {
    @IBOutlet weak var btnDeleteImg: UIButton!
    @IBOutlet weak var img: UIImageView!
    var indexPath: IndexPath?
    weak var delegate:createPostImagePickerDeleteDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func btnDeleteImg(_ sender: Any) {
        delegate?.cellRemoveImages(indexPath!)
    }
}















