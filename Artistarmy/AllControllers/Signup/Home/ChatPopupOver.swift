//
//  MenuPopupVC.swift
//  IRatePro
//
//  Created by mac on 17/02/21.
//

import UIKit
protocol ChatPopupOverDelegate {
    func onButtonClick(button str:chatList )
}
class ChatPopupOver: UIViewController {

    @IBOutlet weak var btnblockUser: UIButton!
    @IBOutlet weak var btnDeleteChat: UIButton!
    var delegate: ChatPopupOverDelegate?
    var myIndex : IndexPath?
    var isSaved = ""
    var myid = ""
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    // MARK: - Button Action

    @IBAction func actionblockUser(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.onButtonClick(button: .blockUser)
        }
    }
    
    @IBAction func actionDeleteChat(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.onButtonClick(button: .deleteChat)
        }
    }
    
  
   
}


enum chatList {
    case blockUser
    case deleteChat
}
