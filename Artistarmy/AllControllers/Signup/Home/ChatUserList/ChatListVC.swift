//
//  HomeVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit
import SDWebImage
class ChatListVC: KBaseViewController {
    @IBOutlet weak var tblNotification: UITableView!
    var arrFollower =  [Chat_users]()
    
    var page:Int = 0
    var isDownloading = false
    var pointContentOffset = CGPoint.zero
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SocketIOManager.sharedInstance.connectSocket()
        SocketIOManager.sharedInstance.establishConnection()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        page = 0
        get_allfriendAPI()
    }
    @IBAction func actionBack(_ sender: Any) {
        pop()
    }
}
extension ChatListVC: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrFollower.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatListCell", for: indexPath) as! ChatListCell
        let obj = arrFollower[indexPath.row]
        cell.lblname.text = obj.full_name
        cell.lbldiscripion.text = obj.messages
        cell.lblTime.text =  getDateFromString(strTimestamp: obj.datetime!)
        if obj.count == 0{
            cell.lblCount.isHidden = true
        }else{
            cell.lblCount.isHidden = false
            cell.lblCount.text = "\(obj.count ?? 0)"
        }
        if let img = obj.profile_image{
        cell.imgUser.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgUser.sd_setImage(with: URL(string: ProfileImagePath +  img ), placeholderImage: UIImage(named: "user"))
        }
        
        if indexPath.row == (arrFollower.count - 1) && (tblNotification.contentOffset.y > pointContentOffset.y) {
            if !isDownloading {
                isDownloading = true
                if arrFollower.count % appDelegate.pageCount == 0 {
                    page += appDelegate.pageCount
                    self.get_allfriendAPI()
                }
            }
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = TDChattingViewController.instance(storyBoard: .Home) as! TDChattingViewController
           vc.dictFollower = arrFollower[indexPath.row]
                push(viewController: vc)
      
   
   }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func getDateFromString(strTimestamp: String) -> String {
        
        let dateFormatter = DateFormatter()
        if strTimestamp.count == 0 {
            return ""
        }else{
            let fromDate = Date.init(timeIntervalSince1970: (Double(strTimestamp)!))
            dateFormatter.dateFormat = "hh:mma"
            return dateFormatter.string(from: fromDate)
        }
    }
    
}



class ChatListCell: UITableViewCell {
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lbldiscripion: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var imgMsgType: UIImageView!
    
    
    
}

extension ChatListVC {
    func get_allfriendAPI(){
        var param = [String : Any]()
       param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kstart] =  page
        param[params.kpageCount] =   appDelegate.pageCount
        
        ServerManager.shared.POST(url: ApiAction.chat_users , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(ChatUserModel.self, from: data) else {
                return
            }
            
            if obj.code == ResponseApis.KSuccess {
               //  self?.arrFollower = obj.chat_users!
                self?.arrFollower.append(contentsOf: obj.chat_users!)
                self?.tblNotification.reloadData()
                if obj.chat_users!.count == 0{
                    self?.isDownloading = true
                }else{
                    self?.isDownloading = false
                }
            } else {
                print("failure")
                self?.isDownloading = true
            }
        }
    }
}

