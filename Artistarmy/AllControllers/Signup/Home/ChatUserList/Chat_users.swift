/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Chat_users : Codable {
	let artist_id : String?
	let fans_id : String?
	let id : String?
	let full_name : String?
	let email : String?
	let profile_image : String?
	let messages : String?
	let datetime : String?
	let count : Int?

	enum CodingKeys: String, CodingKey {

		case artist_id = "artist_id"
		case fans_id = "fans_id"
		case id = "id"
		case full_name = "full_name"
		case email = "email"
		case profile_image = "profile_image"
		case messages = "messages"
		case datetime = "datetime"
		case count = "count"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		artist_id = try values.decodeIfPresent(String.self, forKey: .artist_id)
		fans_id = try values.decodeIfPresent(String.self, forKey: .fans_id)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		full_name = try values.decodeIfPresent(String.self, forKey: .full_name)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
		messages = try values.decodeIfPresent(String.self, forKey: .messages)
		datetime = try values.decodeIfPresent(String.self, forKey: .datetime)
		count = try values.decodeIfPresent(Int.self, forKey: .count)
	}

}