

import Foundation
struct ChatUserModel : Codable {
	let code : String?
	let message : String?
	let chat_users : [Chat_users]?

	enum CodingKeys: String, CodingKey {

		case code = "code"
		case message = "message"
		case chat_users = "chat_users"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		code = try values.decodeIfPresent(String.self, forKey: .code)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		chat_users = try values.decodeIfPresent([Chat_users].self, forKey: .chat_users)
	}

}
