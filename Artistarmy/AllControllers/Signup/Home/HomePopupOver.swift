//
//  MenuPopupVC.swift
//  IRatePro
//
//  Created by mac on 17/02/21.
//

import UIKit
protocol HomePopupOverDelegate {
    func onButtonClick(button str:HomeList , index : IndexPath)
}
class HomePopupOver: UIViewController {

    @IBOutlet weak var btnLeave: UIButton!
    @IBOutlet weak var btnMute: UIButton!
    @IBOutlet weak var btnReport: UIButton!
    var delegate: HomePopupOverDelegate?
    var myIndex : IndexPath?
    var isSaved = ""
    var myid = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        if isSaved == "1"{btnMute.setTitle("Unsaved", for: .normal)}else{btnMute.setTitle("Save Post", for: .normal)}
        // Do any additional setup after loading the view.
        btnLeave.isHidden = true
        btnReport.isHidden = false
        if myid == AppDataHelper.shard.logins.id{
            btnLeave.isHidden = false
            btnReport.isHidden = true
            }
    }

    // MARK: - Button Action

    @IBAction func actionLeave(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.onButtonClick(button: .deletePost, index: self.myIndex!)
        }
    }
    
    @IBAction func actionMute(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.onButtonClick(button: .savePost, index: self.myIndex!)
        }
    }
    
    @IBAction func actionReport(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.onButtonClick(button: .report, index: self.myIndex!)
        }
    }
   
}


enum HomeList {
    case savePost
    case deletePost
    case report
}
