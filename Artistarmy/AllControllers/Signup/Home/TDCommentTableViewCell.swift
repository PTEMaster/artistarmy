//
//  TDCpmmentTableViewCell.swift
//  Traydi
//
//  Created by mac on 21/01/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import Foundation
import UIKit

protocol CommentDelegate: NSObject {
    func addComment(cell:TDCommentTableViewHeaderCell , index : Int)
    func likeComment(cell:TDCommentTableViewHeaderCell, index : Int)
    func profile(cell:TDCommentTableViewHeaderCell, index : Int)
}

class TDCommentTableViewHeaderCell: UITableViewCell {
    
    weak var delegate:CommentDelegate?
      var myIndex : Int?
    @IBOutlet weak var viewCommentBg: UIView!
    @IBOutlet weak var imgVCommentUserProfile: UIImageView!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var imgLike: UIImageView!
    @IBOutlet weak var lblReply: UILabel!
    @IBOutlet weak var btnProfile: UIButton!
    
    override func awakeFromNib() {
           super.awakeFromNib()
           // Initialization code
        viewCommentBg.clipsToBounds = true
        viewCommentBg.layer.cornerRadius = 10
        viewCommentBg.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner , .layerMinXMaxYCorner]
      
       }

       override func setSelected(_ selected: Bool, animated: Bool) {
           super.setSelected(selected, animated: animated)

           // Configure the view for the selected state
       }
    
    @IBAction func actionCommentLike(_ sender: Any) {
        delegate?.likeComment(cell: self, index: myIndex!)
    }
    
    @IBAction func actionCommentReply(_ sender: Any) {
        delegate?.addComment(cell: self, index: myIndex!)
       
        
    }
    
    @IBAction func actionProfile(_ sender: Any) {
       
        delegate?.profile(cell: self, index: myIndex!)
        
    }
}
//---------------------cell Row ------
protocol CommentOnCommentDelegate: NSObject {
    func addComment(cell:TDCommentTableViewCell , index : IndexPath)
    func likeComment(cell:TDCommentTableViewCell, index : IndexPath)
    func profile(cell:TDCommentTableViewCell, index : IndexPath)
}

class TDCommentTableViewCell: UITableViewCell {
    @IBOutlet weak var viewCommentBg: UIView!
    
    @IBOutlet weak var imgVCommentUserProfile: UIImageView!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var lblReply: UILabel!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var imgLike: UIImageView!
    
    weak var delegate:CommentOnCommentDelegate?
    var myIndexRow : IndexPath?
    override func awakeFromNib() {
           super.awakeFromNib()
           // Initialization code
        viewCommentBg.clipsToBounds = true
        viewCommentBg.layer.cornerRadius = 10
        viewCommentBg.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner , .layerMinXMaxYCorner]
      
       }

       override func setSelected(_ selected: Bool, animated: Bool) {
           super.setSelected(selected, animated: animated)

           // Configure the view for the selected state
       }
    
    @IBAction func actionCommentLike(_ sender: Any) {
        delegate?.likeComment(cell: self, index: myIndexRow!)
    }
    
    @IBAction func actionCommentReply(_ sender: Any) {
        delegate?.addComment(cell: self, index: myIndexRow!)
    }
    
    @IBAction func actionProfile(_ sender: Any) {
       
        delegate?.profile(cell: self, index: myIndexRow!)
        
    }
}

