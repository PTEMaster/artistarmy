//
//  FeedMultipleViewCell.swift
//  Traydi
//
//  Created by mac on 06/02/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//


import UIKit

@objc protocol FeedViewCellDelegate{
  /* func feedViewCell(cell:UITableViewCell)
   
   
   func feedViewControllerMenu(Cell: UITableViewCell , sender:UIButton)
   func feedViewPlayVideo(cell: UITableViewCell)
   func feedViewShareHeader(myIndex: Int)
   func feedViewLikeHeader(myIndex: Int)*/
   func AllComments(cell: UITableViewCell)
   func popoverView(Cell: FeedViewMultipleCell , sender:UIButton)
   func feedViewCellLikePost(cell: UITableViewCell)
   func feedViewCellUserProfile(cell: UITableViewCell)
    @objc optional  func feedViewShare(cell: UITableViewCell)
    
}

class FeedViewMultipleCell: UITableViewCell {
 
    @IBOutlet weak var constraintImgFeedHeight: NSLayoutConstraint!
    @IBOutlet weak var imgViewFeed: UIImageView!
    @IBOutlet weak var imgViewFeed1: UIImageView!
    @IBOutlet weak var imgViewFeed2: UIImageView!
    @IBOutlet weak var btnMoreOption: UIButton!
    
    @IBOutlet weak var stkRepost: UIStackView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblFeedText: UILabel!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var lblcomment: UILabel!
    @IBOutlet weak var viewMoreImages: UIView!
    @IBOutlet weak var stackTwoImage: UIStackView!
    @IBOutlet weak var constraintImageHeight: NSLayoutConstraint!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var imgLike: UIImageView!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var viewAllImg: UIView!
    @IBOutlet weak var imgeRePostTo: UIImageView!
    @IBOutlet weak var lblRepostToName: UILabel!
    @IBOutlet weak var lblRepostToTime: UILabel!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var myImg: UIImageView!
     weak var delegate:FeedViewCellDelegate?
    @IBOutlet weak var btnUserProfile: UIButton!

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    @IBAction func actionPopoverview(_ sender: UIButton) {
        print("MoreOptionBtnAction")
        delegate?.popoverView(Cell: self, sender: sender)
    }
    
    
    @IBAction func moreOptionBtnAction(_ sender: UIButton) {
        print("MoreOptionBtnAction")
    //    delegate?.feedViewControllerMenu(Cell: self, sender: sender)
    }
    @IBAction func actionUserProfile(_ sender: Any) {
         print("actionUserProfile")
        
        delegate?.feedViewCellUserProfile(cell: self)
    }
    
    @IBAction func writeCommentAction(_ sender: Any) {
         print("EditBtnAction")
    }
    @IBAction func likeBtnAction(_ sender: Any) {
         print("LikeBtnAction")
        if delegate != nil{
           delegate?.feedViewCellLikePost(cell: self)
        }
    }
    @IBAction func commentsBtnAction(_ sender: Any) {
        print("CommentBtnAction")
        if delegate != nil{
            delegate?.AllComments(cell: self)
        }
    }
    @IBAction func shareBtnAction(_ sender: Any) {
        if delegate != nil{
            print("feedViewShare")
            delegate?.feedViewShare!(cell: self)
        }
    }
    @IBAction func actionPlayVideo(_ sender: Any) {
        if delegate != nil{
      //   delegate?.feedViewPlayVideo(cell: self)
        }
    }
    
    
   /*hp static func heightForCellOf(type:FeedViewCellType, text:String?, width:CGFloat)-> Int{
        if type == .Text{
            let hText = text?.heightWithConstrainedWidth(width: width - 40, font: UIFont.systemFont(ofSize: 17))
            return 125 + Int(hText!)
        }
        return 80
    }*/
}
