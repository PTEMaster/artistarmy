//
//  CommentViewController.swift
//  Traydi
//
//  Created by mac on 26/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit
import  SDWebImage
class CommentViewController: KBaseViewController {

   // @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var txtVComment: UITextView!
    @IBOutlet weak var CommentConstraintBottom: NSLayoutConstraint!
    @IBOutlet weak var txtVConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var tableViewComments: UITableView!
    @IBOutlet weak var btnSend: UIButton!
    var postComment = [Post_comments]()
    var post_id = ""
   // var arrComments = [Comment]()
    var isImages:Bool = true
    var myIndex : IndexPath?
    @IBOutlet weak var lblReplyTo: UILabel!
    var isReplyToComment  : Bool = false
    var replyToIndex : Int = 0
    var replyToRow : Int = 0
    @IBOutlet weak var viewComment: UIView!
    
    @IBOutlet weak var stkReply: UIStackView!
    
    var arrPostimages : [Post_images]?
    
    @IBOutlet weak var cvSlider: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
    var timer: Timer?
    var currentIndex = 0
    let automaticScrollDuration: TimeInterval = 3.0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        post_all_comments_API()
        txtVComment.delegate = self
        txtVComment.text = ""
        btnSend.isEnabled = false
        tableViewComments.sectionHeaderHeight = UITableView.automaticDimension
      //  self.lblComment.text = "\(postComment?.count ?? 0)" + " Comments"
          
        self.tableViewComments.remembersLastFocusedIndexPath = true
        stkReply.isHidden = true
        self.cvSlider.reloadData()
        self.pageController.numberOfPages = self.arrPostimages?.count ?? 0
     //  self.startTimer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
  
    }
    
    func scrollToBottom(){
        DispatchQueue.main.async {
            if (self.postComment.count) > 0{
                let indexPath = IndexPath(row: NSNotFound, section: (self.postComment.count) - 1)
                self.tableViewComments.scrollToRow(at: indexPath, at: .bottom, animated: false)
                }
            
        }
    }
    
   
    
    func validation() -> Bool {
        var status:Bool = true
        if txtVComment.text.isEmpty   {
            status = false
        }
        return status
    }
    @IBAction func actionCloseToReply(_ sender: Any) {
         isReplyToComment = false
          stkReply.isHidden = true
    }
    @IBAction func actionBack(_ sender: Any) {
       pop()
    }
    
    
    @IBAction func sendMessageBtnAction(_ sender: Any) {
        
        if validation() {
            if isReplyToComment{
                self.addcomment(isReplyOnComment: true)
            }else{
                self.addcomment(isReplyOnComment: false)
            }
           
        }
    }
}

extension CommentViewController: UITableViewDataSource, UITableViewDelegate, CommentDelegate {
    
    func profile(cell: TDCommentTableViewHeaderCell, index: Int) {
        let dict = postComment[index]
        
        if dict.user_type == "0"{
            //fan
            let vc = ProfileFanVC.instance(storyBoard: .Profile) as! ProfileFanVC
            vc.fanid = dict.sender_id ?? ""
            vc.isSeenFanProfile = true
            push(viewController: vc)
        }else{
            //Artist
            let vc = ProfileVC.instance(storyBoard: .Profile) as! ProfileVC
            vc.userID = dict.sender_id ?? ""
            vc.isComeFromFan = true
            push(viewController: vc)
        }
    }
  
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "TDCommentTableViewHeaderCell")  as! TDCommentTableViewHeaderCell
            let dict = postComment[section]
            cell.lblUserName.text = dict.full_name
            cell.lblComment.text = dict.message
            cell.delegate = self
            cell.myIndex = section
           
                //like button
             if  dict.is_comment_like  == 1{
                  cell.imgLike.image = UIImage(named: "like")
              }else{
                   cell.imgLike.image = UIImage(named: "unlike")
              }
             if dict.total_like == "0"{
                cell.lblLike.text = (dict.total_like)!  + " like"
              }
             else if dict.total_like == "1"{
                cell.lblLike.text = (dict.total_like)!  + " like"
              }else{
                cell.lblLike.text = (dict.total_like)! + " likes"
              }
            
             cell.lblUserName.text = dict.full_name
            cell.imgVCommentUserProfile.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
             cell.imgVCommentUserProfile.sd_setImage(with: URL(string:  ProfileImagePath + (dict.profile_image)!), placeholderImage: UIImage(named: "user"))
            
            return cell
       
    }
    

    

    func numberOfSections(in tableView: UITableView) -> Int {
        return postComment.count
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (postComment[section].post_reply?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TDCommentTableViewCell")  as! TDCommentTableViewCell
        cell.delegate = self
        cell.myIndexRow = indexPath
            let dict = postComment[indexPath.section].post_reply?[indexPath.row]
                cell.lblComment.text = dict?.message ?? "N/A"
                cell.lblUserName.text = dict?.full_name  ?? "N/A"
           cell.imgVCommentUserProfile.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
        
        if  dict?.is_comment_like  == 1{
             cell.imgLike.image = UIImage(named: "like")
         }else{
              cell.imgLike.image = UIImage(named: "unlike")
         }
       
        cell.imgVCommentUserProfile.sd_setImage(with: URL(string: ProfileImagePath  + (dict?.profile_image)! ), placeholderImage: UIImage(named: "user"))
            return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TDCommentTableViewHeaderCell")  as! TDCommentTableViewHeaderCell
            let dict = postComment[section]
        cell.lblComment.text = dict.message
               //like button
        if  dict.is_comment_like  == 1{
            cell.imgLike.image = UIImage(named: "like")
             }else{
                  cell.imgLike.image = UIImage(named: "unlike")
             }
        
        if dict.total_like == "0"{
            cell.lblLike.text =   "\((dict.total_like!)) like"
             }
        else if dict.total_like == "1"{
            cell.lblLike.text = (dict.total_like!) + " like"
             }else{
                cell.lblLike.text = (dict.total_like!) + " likes"
             }
           
        cell.lblUserName.text = dict.full_name
            cell.imgVCommentUserProfile.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
     
        cell.imgVCommentUserProfile.sd_setImage(with: URL(string: ProfileImagePath + (dict.profile_image)!), placeholderImage: UIImage(named: "user"))
            return cell.frame.size.height
        
    }
    private func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    

   
   
    

   
    
}





extension CommentViewController:UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
    }
    
   
    func textViewDidChange(_ textView: UITextView) {
        let textHeight = textView.contentSize.height
        if textHeight < 100 {
            txtVConstraintHeight.constant = textHeight
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        if text != "Add a comment..." {
            btnSend.isEnabled = true
        }
               
        return true
    }

}



extension UITableView {
      func lastIndexpath() -> IndexPath {
          let section = max(numberOfSections - 1, 0)
          let row = max(numberOfRows(inSection: section) - 1, 0)

          return IndexPath(row: row, section: section)
      }
  }





extension CommentViewController: CommentOnCommentDelegate{
    
    func profile(cell: TDCommentTableViewCell, index: IndexPath) {
        let dict = postComment[index.section].post_reply?[index.row]
        
       
        
        if dict?.user_type == "0"{
            //fan
            let vc = ProfileFanVC.instance(storyBoard: .Profile) as! ProfileFanVC
            vc.fanid = dict?.sender_id ?? ""
            vc.isSeenFanProfile = true
            push(viewController: vc)
        }else{
            //Artist
            let vc = ProfileVC.instance(storyBoard: .Profile) as! ProfileVC
            vc.userID = dict?.sender_id ?? ""
            vc.isComeFromFan = true
            push(viewController: vc)
        }
    }
    func post_all_comments_API(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kpost_id] = post_id
        ServerManager.shared.POST(url: ApiAction.post_all_comments , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(CommentModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.txtVComment.text = ""
                self?.stkReply.isHidden = true
                self?.postComment  = obj.post_comments!
                self?.tableViewComments.reloadData()
                if  self?.arrPostimages?.count == 0{
                    self?.get_post_image_API()
                    }


            } else {
                if  self?.postComment.count == 0{
                    self?.tableViewComments.displayBackgroundImageWithText(text: "No comment available", imgName: "cloud", textColor: .black)
                              }else{
                                self?.tableViewComments.displayBackgroundImageWithText(text: "", imgName: "", textColor: .black)
                              }
                print("failure")
               // presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    func get_post_image_API(){
        var param = [String : Any]()
        param[params.kpost_id] = post_id
        ServerManager.shared.POST(url: ApiAction.get_post_image , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(ImgPostModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrPostimages = obj.postImage
                self?.cvSlider.reloadData()
            } else {
                print("failure")
               // presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    
    
    func  addcomment(isReplyOnComment : Bool){
        //---------
        var param = [String : Any]()
         //param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kpost_id] = post_id
        param[params.kmessage] = txtVComment.text
        param[params.ksender_id] = AppDataHelper.shard.logins.id
        
        if isReplyOnComment{
            if replyToRow == 0{
                let comment_id = postComment[replyToIndex].comment_id
                param[params.kcomment_id] = comment_id
            }else{
                let comment_id = postComment[replyToIndex].post_reply?[replyToRow].comment_id
                param[params.kcomment_id] = comment_id
            }
           
        }
        
        ServerManager.shared.POST(url: ApiAction.add_post_comment, param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(AddPostCommentModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.txtVComment.text = ""
                self?.post_all_comments_API()
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
  
    }
    

    
    func addComment(cell: TDCommentTableViewHeaderCell, index: Int) {
        // FIXME: HIMANSHU PAL reply to particular comment
        stkReply.isHidden = false
        let dict = postComment[index]
        lblReplyTo.text =  "Reply to: " + (dict.full_name)!
        isReplyToComment = true
        replyToIndex = index
        replyToRow = 0
        self.txtVComment.becomeFirstResponder()
    }
    
    func likeComment(cell:  TDCommentTableViewHeaderCell, index: Int) {
        let comment_id = postComment[index].comment_id
//        guard let celli = cell as? TDCommentTableViewHeaderCell else {
//            return
//        }
        
        //---------
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kcomment_id] = comment_id
        
        ServerManager.shared.POST(url: ApiAction.post_comment_like , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(PostCommentLikeModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                let liked = obj.is_comment_like
                let likes = obj.count
                self?.postComment[index].is_comment_like =  obj.is_comment_like
                self?.postComment[index].total_like =  obj.count
                
             if  liked  == 1{
                    cell.imgLike.image = UIImage(named: "like")
                }else{
                     cell.imgLike.image = UIImage(named: "unlike")
                }
                
                
                if likes == "0"{
                    cell.lblLike.text = likes!  + " like"
                 }
                else if likes == "1"{
                    cell.lblLike.text = likes! + " like"
                 }else{
                    cell.lblLike.text = likes! + " likes"
                 }
                
                self?.tableViewComments.reloadData()
                
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    //------------------------Row
    func addComment(cell: TDCommentTableViewCell, index: IndexPath) {
        stkReply.isHidden = false
        let dict = postComment[index.section].post_reply?[index.row]
        lblReplyTo.text =  "Reply to: " + (dict?.full_name)!
        isReplyToComment = true
        replyToIndex = index.section
        replyToRow = index.row
        self.txtVComment.becomeFirstResponder()
        
    }
    
    func likeComment(cell: TDCommentTableViewCell, index: IndexPath) {
        let comment_id = postComment[index.section].post_reply?[index.row].comment_id
//        guard let celli = cell as? TDCommentTableViewHeaderCell else {
//            return
//        }
        //---------
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kcomment_id] = comment_id
        
        ServerManager.shared.POST(url: ApiAction.post_comment_like , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            if data.count > 0 {
                            do {
                                let jsonDecoder = JSONDecoder()
                                _ = try jsonDecoder.decode(PostCommentLikeModel.self, from: data)
                                
                            } catch DecodingError.typeMismatch( _, let context) {
                                print("DecodingError.typeMismatch: \(context.debugDescription)")
                                for i in 0..<context.codingPath.count { print("  [\(i)] = \(context.codingPath[i])") }
                                
                            } catch DecodingError.valueNotFound( _, let context) {
                                print("DecodingError.valueNotFound: \(context.debugDescription)")
                                for i in 0..<context.codingPath.count { print("  [\(i)] = \(context.codingPath[i])") }
                                
                            } catch DecodingError.keyNotFound( _, let context) {
                                print("DecodingError.keyNotFound: \(context.debugDescription)")
                                for i in 0..<context.codingPath.count { print("  [\(i)] = \(context.codingPath[i])") }
                            } catch DecodingError.dataCorrupted(let context) {
                                print("DecodingError.dataCorrupted: \(context.debugDescription)")
                                for i in 0..<context.codingPath.count { print("  [\(i)] = \(context.codingPath[i])") }
                                
                            } catch let err {
                                print(err.localizedDescription)
                            }
                        }
            
            guard  let obj = try? JSONDecoder().decode(PostCommentLikeModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                let liked = obj.is_comment_like
                let likes = obj.count
                self?.postComment[index.section].post_reply?[index.row].is_comment_like =  obj.is_comment_like
                self?.postComment[index.section].post_reply?[index.row].total_like =  obj.count
                
             if  liked  == 1{
                    cell.imgLike.image = UIImage(named: "like")
                }else{
                     cell.imgLike.image = UIImage(named: "unlike")
                }
                
                
                if likes == "0"{
                    cell.lblLike.text = likes!  + " like"
                 }
                else if likes == "1"{
                    cell.lblLike.text = likes! + " like"
                 }else{
                    cell.lblLike.text = likes! + " likes"
                 }
                
                self?.tableViewComments.reloadData()
                
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
}



//MARK:- UICollectionView delegate and DataSource method(s)
extension CommentViewController: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrPostimages!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KSliderCollectionViewCell", for: indexPath) as! KSliderCollectionViewCell
        let dict =  self.arrPostimages?[indexPath.row]
        let img = dict?.image
        let imgURL = URL(string: PostImagePath + img! )
        print( PostImagePath + img!)
        cell.imgSlider.sd_setImage(with: imgURL, completed: nil)
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.frame.size.height - 30
        let width = collectionView.frame.size.width
        return CGSize(width: width, height: height)
    }
    
    //MARK:- scroll view Delegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let xyz = scrollView.contentOffset.x / scrollView.frame.size.width
        let pageIndex: Int = Int(xyz)
        currentIndex = pageIndex
        self.updatePage(pageIndex: pageIndex)
    }
    
    //MARK:- Methods
    func updatePage(pageIndex: Int) {
        self.pageController.currentPage = pageIndex
    }
    
    func startTimer() {
        if let timer = self.timer {
            timer.invalidate()
        }
        timer = Timer.scheduledTimer(timeInterval: automaticScrollDuration, target: self, selector: #selector(updateCollectionScroll), userInfo: nil, repeats: true)
    }
    
    func stopTimer() {
        if let timer = self.timer {
            timer.invalidate()
        }
        self.timer = nil
    }
    
    @objc func updateCollectionScroll() {
        if (self.arrPostimages?.count ?? 0) > 0 {
            currentIndex += 1
            if currentIndex >= (self.arrPostimages?.count ?? 0) {
                currentIndex = 0
            }
            cvSlider.scrollToItem(at: IndexPath.init(item: currentIndex, section: 0), at: .centeredHorizontally, animated: true)
            updatePage(pageIndex: currentIndex)
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        stopTimer()
    }
}




//---------

struct CommentModel : Codable {
    let code : String?
    let message : String?
    let post_comments : [Post_comments]?
    let total_comment : Int?

    enum CodingKeys: String, CodingKey {

        case code = "code"
        case message = "message"
        case post_comments = "post_comments"
        case total_comment = "total_comment"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(String.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        post_comments = try values.decodeIfPresent([Post_comments].self, forKey: .post_comments)
        total_comment = try values.decodeIfPresent(Int.self, forKey: .total_comment)
    }

}


struct PostCommentLikeModel: Codable {
    let code, message, count: String?
    let is_comment_like: Int?

    enum CodingKeys: String, CodingKey {
        case code, message, count
        case is_comment_like
    }
}

struct ReelsCommentlike: Codable {
    let code, message: String?
    let count: Int?
}

// MARK: - AddPostCommentModel
struct AddPostCommentModel: Codable {
    let code, message: String?
    let comment: [Comment]?
    let count: Int?
}

// MARK: - Comment
struct Comment: Codable {
    let commentID, postID, recieverID, senderID: String?
    let message, createdDate, createdTime, referID: String?
    let totalLike: String?

    enum CodingKeys: String, CodingKey {
        case commentID
        case postID
        case recieverID
        case senderID
        case message
        case createdDate
        case createdTime
        case referID
        case totalLike
    }
}



extension String {
    var decodeEmoji: String{
        let data = self.data(using: String.Encoding.utf8);
        let decodedStr = NSString(data: data!, encoding: String.Encoding.nonLossyASCII.rawValue)
        if let str = decodedStr{
            return str as String
        }
        return self
    }
    var encodeEmoji: String{
        if let encodeStr = NSString(cString: self.cString(using: .nonLossyASCII)!, encoding: String.Encoding.utf8.rawValue){
            return encodeStr as String
        }
        return self
    }
}

class KSliderCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgSlider: UIImageView!
    @IBOutlet weak var pageControlSlider: UIPageControl!
    
    @IBOutlet weak var lblTitle: UILabel!
    
}



struct ImgPostModel: Codable {
    let code, message: String
    let postImage: [Post_images]

    enum CodingKeys: String, CodingKey {
        case code, message
        case postImage = "post_images"
    }
}


