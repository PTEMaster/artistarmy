//
//  TDChattingRecevierTextTableViewCell.swift
//  Traydi
//
//  Created by mac on 16/01/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit

class TDChattingRecevierTextTableViewCell: UITableViewCell {

    @IBOutlet weak var imgReciver: UIImageView!
    @IBOutlet weak var lblMsgReciver: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
