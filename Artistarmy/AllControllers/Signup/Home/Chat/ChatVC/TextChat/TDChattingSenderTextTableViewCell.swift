//
//  TDChattingSenderTextTableViewCell.swift
//  Traydi
//
//  Created by mac on 16/01/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit

class TDChattingSenderTextTableViewCell: UITableViewCell {

    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var imgSender: UIImageView!
    @IBOutlet weak var lbldate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
