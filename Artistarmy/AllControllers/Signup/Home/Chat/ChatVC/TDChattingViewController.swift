//
//  TDChattingViewController.swift
//  Traydi
//
//  Created by mac on 09/01/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit
import UserNotifications
import SDWebImage
import IQKeyboardManagerSwift

class TDChattingViewController: UIViewController ,UIScrollViewDelegate {
    @IBOutlet weak var tVChatting: UITableView!
    @IBOutlet weak var tvMsg: UITextView!//{
     /*   didSet {
            tvMsg.font = UIFont.regularFont(size: 15)
          //  tvMsg.placeholder = "Type a message"
            tvMsg.minHeight = initailTextviewHeight
            tvMsg.maxHeight = UIScreen.main.bounds.size.height * 0.2
            tvMsg.placeholderColor = UIColor.init(red: 102/255, green: 102/255, blue: 102/255, alpha: 1.0)
            tvMsg.layer.cornerRadius = 8
            tvMsg.textContainer.lineFragmentPadding = 10
            tvMsg.textContainerInset.top = 15
            tvMsg.delegate = self
        }*/
  //  }
    
    @IBOutlet weak var tvConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var viewmsgBottom: NSLayoutConstraint!
    var arrDocument = [[String:Any]]()
    var myId = ""
    var imagepickerDocument:TDImagePicker!
//
    var initailTextviewHeight : CGFloat = 45
    
    var handlerDidSentNewMessage : (() -> Void)? = nil
    
    var isSendingMessage : Bool = false
    @IBOutlet weak var consWidthBtnSend: NSLayoutConstraint!
    @IBOutlet weak var btnSend : UIButton!
    
    
    @IBOutlet weak var lblName: UILabel!
    var arrCommentList = [Chat_messages]()
    var  isMoveButtom = false
    
    var dictFollower :  Chat_users?
    
    
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // IQKeyboardManager.shared.disabledToolbarClasses = [TDChattingViewController.self]
        //  SocketIOManager.sharedInstance.connectSocket()
        //  SocketIOManager.sharedInstance.establishConnection()
        tvMsg.textColor = .gray
        tvMsg.delegate = self
        self.registerNib(tableview: tVChatting)
        self.tvMsg.text = ""
        self.tvConstraintHeight.constant = 42
        self.consWidthBtnSend.constant = 0
        lblName.text = dictFollower?.full_name
        imagepickerDocument = TDImagePicker(presentationController: self, delegate: self)
      //  self.setupNotificationkeyboard()
       self.call_getComments_API()
        receiveChatData()
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        self.setupKeyboardNotification()
    }
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       // IQKeyboardManager.shared.enable = true
        //   SocketIOManager.sharedInstance.closeConnection()
    }
    
    
    func registerNib(tableview:UITableView) {
        tableview.register(UINib(nibName: "TDChattingSenderTextTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "TDChattingSenderTextTableViewCell")
        tableview.register(UINib(nibName: "TDChattingRecevierTextTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "TDChattingRecevierTextTableViewCell")
        tableview.register(UINib(nibName: "ChattingSenderImgCell", bundle: Bundle.main), forCellReuseIdentifier: "ChattingSenderImgCell")
        tableview.register(UINib(nibName: "ChattingRecevierImgCell", bundle: Bundle.main), forCellReuseIdentifier: "ChattingRecevierImgCell")
        
        
        
        
    }
    
    
    func receiveChatData(){
        let id  =  ""
        print("-\(id)-")
        print("ReceiverById\(id)")
        let aa  = AppDataHelper.shard.logins.id
        // on recive
        SocketIOManager.sharedInstance.defaultSocket.on("ReceiverMessageById\(aa!)") { (receiveData, ask) in
            print("receiveData-\(receiveData)-")
            
            if   let dict =  receiveData[0] as? [String:Any]{
                var cc = Chat_messages()
                cc.sender_id = dict["sender_id"] as? String ?? ""
                cc.sender_name = dict["sender_name"] as? String ?? ""
                cc.sender_image = dict["sender_image"] as? String ?? ""
                cc.receiver_id =  dict["receiver_id"] as? String ?? ""
                cc.receiver_name =  dict["receiver_name"] as? String ?? ""
                cc.receiver_image =  dict["receiver_image"] as? String ?? ""
                cc.messages =  dict["messages"] as? String ?? ""
                cc.datetime =  dict["datetime"] as? String ?? ""
                cc.image =  dict["image"] as? String ?? ""
                cc.refer_id =  dict["refer_id"] as? String ?? ""
                cc.type =  dict["type"] as? String ?? ""
                
                self.arrCommentList.append(cc)
                
                let indexPath = IndexPath.init(row: self.arrCommentList.count - 1, section: 0)
                self.tVChatting.insertRows(at: [indexPath], with: .bottom)
                self.tVChatting.scrollToRow(at: indexPath, at: .bottom, animated: true)
                self.view.layoutIfNeeded()
            }
        }}
    
    @IBAction func actionSendMessage(_ sender: Any) {
         if tvMsg.text == "" {
            return
       }
        var params = [String:Any]()
        let timestamp = Date().currentTimeSec()
        params["sender_id"] = AppDataHelper.shard.logins.id
        params["sender_name"] = AppDataHelper.shard.logins.full_name
        params["sender_image"] = AppDataHelper.shard.logins.profile_image!
        params["receiver_id"] = dictFollower?.id
        params["receiver_name"] = dictFollower?.full_name
        params["receiver_image"] =  dictFollower?.profile_image
        params["messages"] = tvMsg.text
        params["datetime"] = "\(timestamp)"
        params["image"] = ""
        params["type"] =  "0"
        //send
        SocketIOManager.sharedInstance.defaultSocket.emit("SenderMessageById" , with: [params])
        
        var cc = Chat_messages()
        cc.sender_id = AppDataHelper.shard.logins.id
        cc.sender_name = AppDataHelper.shard.logins.full_name
        cc.sender_image = AppDataHelper.shard.logins.profile_image!
        cc.receiver_id = dictFollower?.id
        cc.receiver_name = dictFollower?.full_name
        cc.receiver_image =   dictFollower?.profile_image
        cc.messages = tvMsg.text
        cc.datetime = "\(timestamp)"
        cc.image = ""
        cc.type = "0"
        arrCommentList.append(cc)
        let indexPath = IndexPath.init(row: arrCommentList.count - 1, section: 0)
        tVChatting.insertRows(at: [indexPath], with: .bottom)
        tVChatting.scrollToRow(at: indexPath, at: .bottom, animated: true)
        tvMsg.text = ""
        textViewDidChange(tvMsg)
        tvConstraintHeight.constant = 42
         self.view.layoutIfNeeded()
    }
    
  
  
    

    
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionUploadProfileImg(_ sender: Any) {
     imagepickerDocument.present(from: sender as! UIView)
        
    }
    @IBAction func actionchatpopup(_ sender: UIButton) {
    let alertController = storyboard?.instantiateViewController(withIdentifier: "ChatPopupOver") as! ChatPopupOver
        alertController.modalPresentationStyle = .popover
        let popover = alertController.popoverPresentationController
        popover?.delegate = self
        alertController.delegate = self
        popover?.sourceView = sender
        popover?.permittedArrowDirections =  .up
        alertController.preferredContentSize = CGSize(width: 90, height: 80)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
}

extension TDChattingViewController : UITableViewDelegate , UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  arrCommentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj = arrCommentList[indexPath.row]
        
        if AppDataHelper.shard.logins.id == obj.sender_id {
            if obj.image == ""{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TDChattingSenderTextTableViewCell", for: indexPath) as! TDChattingSenderTextTableViewCell
            cell.lblMsg.text = obj.messages?.decodeEmoji
            cell.lbldate.text = getDateFromString(strTimestamp: obj.datetime!)
            cell.imgSender.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
            cell.imgSender.sd_setImage(with: URL(string:  ProfileImagePath + (obj.sender_image)!), placeholderImage: UIImage(named: "user"))
            return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChattingSenderImgCell", for: indexPath) as! ChattingSenderImgCell
                cell.lblMsg.text = obj.messages?.decodeEmoji
                cell.lbldate.text = getDateFromString(strTimestamp: obj.datetime!)
                cell.imgSender.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
                cell.imgUploaded.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
                cell.imgSender.sd_setImage(with: URL(string:  ProfileImagePath + (obj.sender_image)!), placeholderImage: UIImage(named: "user"))
                cell.imgUploaded.sd_setImage(with: URL(string:   (obj.image)!), placeholderImage: UIImage(named: "user"))
                
               
                return cell
            }
        }else{
            if obj.image == ""{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TDChattingRecevierTextTableViewCell", for: indexPath) as! TDChattingRecevierTextTableViewCell
            cell.lblMsgReciver.text = obj.messages?.decodeEmoji
            cell.lbldate.text = getDateFromString(strTimestamp: obj.datetime!)
            cell.imgReciver.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
            cell.imgReciver.sd_setImage(with: URL(string:  ProfileImagePath + (obj.sender_image)!), placeholderImage: UIImage(named: "user"))
            return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChattingRecevierImgCell", for: indexPath) as! ChattingRecevierImgCell
                cell.lblMsgReciver.text = obj.messages?.decodeEmoji
                cell.lbldate.text = getDateFromString(strTimestamp: obj.datetime!)
                cell.imgReciver.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
                cell.imgUploaded.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
                cell.imgReciver.sd_setImage(with: URL(string:  ProfileImagePath + (obj.sender_image)!), placeholderImage: UIImage(named: "user"))
                cell.imgUploaded.sd_setImage(with: URL(string:  (obj.image)!), placeholderImage: UIImage(named: "user"))
               
                return cell
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = arrCommentList[indexPath.row]
        print("didSelectRowAt\(obj.type!)-=-=-=\(obj.refer_id!)")
        if obj.type != "0" || obj.type != ""{
            if obj.type == "1"{
                appDelegate.post_id = obj.refer_id!
                popToRoot()
            }else if obj.type == "2"{
                appDelegate.reel_id = obj.refer_id!
                popToRoot()
            }else{
                //3
                let vc = ProfileVC.instance(storyBoard: .Profile) as! ProfileVC
                appDelegate.ref_user_id = obj.sender_id!
                appDelegate.ref_artist_id = obj.refer_id!
                vc.userID = appDelegate.ref_artist_id
                vc.isComeFromFan = true
                push(viewController: vc)
            }
            
        }
    }
    func backTwo() {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
        }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 44)
        let lableView = UILabel()
        let lblx = (headerView.frame.size.width / 2)
        lableView.frame = CGRect(x: lblx - 50 , y: 5, width: 100, height: headerView.frame.height - 10)
        lableView.backgroundColor = .white
        //  lableView.text = "Today"
        lableView.textAlignment = .center
        lableView.font = UIFont.boldSystemFont(ofSize: 12)
        lableView.layer.cornerRadius = 17
        lableView.clipsToBounds = true
        headerView.addSubview(lableView)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    

    
}

extension TDChattingViewController:UITextViewDelegate  {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
    }
    
   
 
    
 /*  func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

        if textView == tvMsg
        {
            textView.text = textView.text.replacingOccurrences(of: " ", with: "")
        }
        return true
    }
     func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
           if(text == "\n") {
               textView.resignFirstResponder()
               return false
           }
           return true
           
       }
     */
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard range.location == 0 else {
            return true
        }
        let newString = (textView.text as NSString).replacingCharacters(in: range, with: text) as NSString
        return newString.rangeOfCharacter(from: CharacterSet.whitespacesAndNewlines).location != 0
    }
    
    func scrollToBottom(){
        DispatchQueue.main.async {
            if self.arrCommentList.count > 0{
                let indexPath = IndexPath(row: self.arrCommentList.count-1, section: 0)
                self.tVChatting.scrollToRow(at: indexPath, at: .none, animated: false)
            }
        }
    }
    
    
    
    
    func call_getComments_API( ){
        var param = [String : Any]()
        param[params.kfriend_id] = dictFollower?.id
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        
        ServerManager.shared.POST(url: ApiAction.user_chat_messages  , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(ChatModel.self, from: data) else {
                return
            }
            
            if obj.code == ResponseApis.KSuccess {
                self.arrCommentList.removeAll()
                self.arrCommentList = obj.chat_messages ?? []
                self.tVChatting.reloadData()
                    self.scrollToBottom()
              
            } else {
                print("failure")
            }
        }
    }
    
    
    func delete_chat_API( ){
        var param = [String : Any]()
        param[params.kfriend_id] = dictFollower?.id
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        
        ServerManager.shared.POST(url: ApiAction.delete_chat  , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(ChatModel.self, from: data) else {
                return
            }
            
            if obj.code == ResponseApis.KSuccess {
                self.arrCommentList.removeAll()
                self.tVChatting.reloadData()
              
            } else {
                print("failure")
            }
        }
    }
    
    
   
    
    

    
    
    
    
    
    
    
    
    
    func getDateFromString(strTimestamp: String) -> String {
        
        let dateFormatter = DateFormatter()
        if strTimestamp.count == 0 {
            return ""
        }else{
            let fromDate = Date.init(timeIntervalSince1970: (Double(strTimestamp)!))
            dateFormatter.dateFormat = "MM-dd-yy, hh:mma"
            return dateFormatter.string(from: fromDate)
        }
    }
    
    
}






extension TDChattingViewController  {
    
   
    
    func textViewDidChange(_ textView: UITextView) {
        
        let isEmpty : Bool = textView.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
        let widthToSet : CGFloat = isEmpty ? 0 : 45
        let textHeight = textView.contentSize.height
        if textHeight < 100 {
            tvConstraintHeight.constant = textHeight
        }

        if self.consWidthBtnSend.constant != widthToSet {
            
            self.consWidthBtnSend.constant = widthToSet
            
            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.btnSend.superview?.layoutIfNeeded()
            }
        }
    }
   
   
}


extension TDChattingViewController {
    
    private func setupKeyboardNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShowForResizing),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHideForResizing),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    @objc private func keyboardWillShowForResizing(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {

            self.viewmsgBottom.constant = keyboardSize.height

            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                
            }) { [weak self] (_) in
                guard let this = self else { return }
                self?.scrollToBottom()
                self?.tVChatting.superview?.layoutIfNeeded()
                
            }

        } else {
            debugPrint("We're showing the keyboard and either the keyboard size or window is nil: panic widely.")
        }
    }

    @objc private func keyboardWillHideForResizing(notification: Notification) {
        self.viewmsgBottom.constant = 0

        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.tVChatting.superview?.layoutIfNeeded()
        }
    }
}


extension TDChattingViewController:  TDImagePickerDelegate {
    func didSelect(image: UIImage?) {
        let param = [String : Any]()
        let imgProfile = UIImageView(image: image!)
        ServerManager.shared.POSTWithImage(url: ApiAction.chat_image, param: param, imgParam: "image", imageView: imgProfile) { (data, error) in
            guard let data = data else {
                  print("data not available")
                  return
              }
             guard  let obj = try? JSONDecoder().decode(imgUploadModel.self, from: data) else {
                  return
              }
              if obj.code == ResponseApis.KSuccess {
              
                Artistarmy.show(message: obj.message ?? "")
                var params = [String:Any]()
                let timestamp = Date().currentTimeSec()
                params["sender_id"] = AppDataHelper.shard.logins.id
                params["sender_name"] = AppDataHelper.shard.logins.full_name
                params["sender_image"] = AppDataHelper.shard.logins.profile_image
                params["receiver_id"] = self.dictFollower?.id
                params["receiver_name"] = self.dictFollower?.full_name
                params["receiver_image"] =  self.dictFollower?.profile_image
                params["messages"] = self.tvMsg.text
                params["datetime"] = "\(timestamp)"
                params["image"] =  obj.image_url
                params["type"] =  "0"
                
                SocketIOManager.sharedInstance.defaultSocket.emit("SenderMessageById" , with: [params])
                
                var cc = Chat_messages()
                cc.sender_id = AppDataHelper.shard.logins.id
                cc.sender_name = AppDataHelper.shard.logins.full_name
                cc.sender_image = AppDataHelper.shard.logins.profile_image
                cc.receiver_id = self.dictFollower?.id
                cc.receiver_name = self.dictFollower?.full_name
                cc.receiver_image = self.dictFollower?.profile_image
                cc.messages = self.tvMsg.text
                cc.datetime = "\(timestamp)"
                cc.image = obj.image_url
                self.arrCommentList.append(cc)
                let indexPath = IndexPath.init(row: self.arrCommentList.count - 1, section: 0)
                self.tVChatting.insertRows(at: [indexPath], with: .bottom)
                self.tVChatting.scrollToRow(at: indexPath, at: .bottom, animated: true)
                self.tvMsg.text = ""
                self.textViewDidChange(self.tvMsg)
                self.tvConstraintHeight.constant = 42
                 self.view.layoutIfNeeded()
              } else {
                  print("failure")
                Artistarmy.show(message: obj.message ?? "")
              }
          }

    }
    
   
}



extension TDChattingViewController: UIPopoverPresentationControllerDelegate ,ChatPopupOverDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
  
    func onButtonClick(button str: chatList) {
        if str == .blockUser{
            
        }else{
            //delete chat
            delete_chat_API()
            }
    }
}



extension Date {
    func currentTimeSec() -> Int64 {
        return Int64(self.timeIntervalSince1970 )
    }
}


struct imgUploadModel : Codable {
    let code : String?
    let message : String?
   let image_url : String?
    enum CodingKeys: String, CodingKey {

        case code = "code"
        case message = "message"
        case image_url = "image_url"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(String.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        image_url  = try values.decodeIfPresent(String.self, forKey: .image_url)
    }

}

