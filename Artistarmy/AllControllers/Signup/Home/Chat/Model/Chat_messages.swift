

import Foundation
struct Chat_messages : Codable {
	var messages_id : String?
	var sender_id : String?
	var sender_name : String?
	var sender_image : String?
    
	var receiver_id : String?
	var receiver_name : String?
	var receiver_image : String?
	var messages : String?
    
	var datetime : String?
	var group_id : String?
	var status : String?
    var image:String?
    var type:String?
    var refer_id :String?
    

	enum CodingKeys: String, CodingKey {

		case messages_id = "messages_id"
		case sender_id = "sender_id"
		case sender_name = "sender_name"
		case sender_image = "sender_image"
		case receiver_id = "receiver_id"
		case receiver_name = "receiver_name"
		case receiver_image = "receiver_image"
		case messages = "messages"
		case datetime = "datetime"
		case group_id = "group_id"
		case status = "status"
        case image = "image"
        case type = "type"
        case refer_id = "refer_id"
	}



}
