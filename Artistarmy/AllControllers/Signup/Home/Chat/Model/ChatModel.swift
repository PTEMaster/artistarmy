

import Foundation
struct ChatModel : Codable {
	let code : String?
	let message : String?
	let chat_messages : [Chat_messages]?

	enum CodingKeys: String, CodingKey {

		case code = "code"
		case message = "message"
		case chat_messages = "chat_messages"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		code = try values.decodeIfPresent(String.self, forKey: .code)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		chat_messages = try values.decodeIfPresent([Chat_messages].self, forKey: .chat_messages)
	}

}
