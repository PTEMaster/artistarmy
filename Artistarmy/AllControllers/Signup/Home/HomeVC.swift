//
//  HomeVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit
import SDWebImage

import AVFoundation
import GSPlayer
import MediaToolbox
import MediaPlayer


class HomeVC: KBaseViewController, MPMediaPickerControllerDelegate {
    @IBOutlet weak var tableViewFeed: UITableView!
    @IBOutlet weak var imgLogout: UIImageView!
    @IBOutlet weak var btnAdd: UIButton!
    var items: [URL] = []
    var arrPostList = [Post_list]()

    var page:Int = 0
    var isDownloading = false
    var pointContentOffset = CGPoint.zero
    //    deinit {
    //         print("Remove NotificationCenter Deinit")
    //        NotificationCenter.default.removeObserver(self)
    //     }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTabBarItems()
        UITabBar.setTransparentTabbar()
        self.tabBarController?.tabBar.isHidden = false
        
        
        if AppDataHelper.shard.isFan{
            btnAdd.isHidden = true
            imgLogout.image = UIImage(named: "logout")
        }else{
            btnAdd.isHidden = false
            imgLogout.image = UIImage(named: "setting")
        }
        
        self.tableViewFeed.register(UINib(nibName: "FeedViewMultipleCell", bundle: nil), forCellReuseIdentifier: "FeedViewMultipleCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: .Deeplinking, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: .notificationData, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification2(notification:)), name: .notificationData, object: nil)
        
  
        
        
  

    }
   
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        //
        let fullEventArr = notification.object as? [String]
        
        let  artist_id = fullEventArr![0]
        let  user_id = fullEventArr![1]
        
        let listItems = artist_id.components(separatedBy: "=")
        appDelegate.ref_artist_id = listItems[1]
        let listItems2 = user_id.components(separatedBy: "=")
        appDelegate.ref_user_id = listItems2[1]
        
        let vc = ProfileVC.instance(storyBoard: .Profile) as! ProfileVC
        vc.userID = appDelegate.ref_artist_id
        vc.isComeFromFan = true
        push(viewController: vc)
        
        
      
    }
    
    
    @objc func openArtistProfile(notification: Notification) {
        self.tabBarController?.selectedIndex = 2
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        items.removeAll()
      //  print("appDelegate.ref_user_id\(appDelegate.ref_user_id)")
       /// print("appDelegate.reel_id\(appDelegate.reel_id)")
      //  print("appDelegate.post_id\(appDelegate.post_id)")
        if   appDelegate.ref_user_id != ""{
            let vc = ProfileVC.instance(storyBoard: .Profile) as! ProfileVC
            vc.userID = appDelegate.ref_artist_id
            vc.isComeFromFan = true
            push(viewController: vc)
        }else if  appDelegate.reel_id != ""{
            self.tabBarController!.selectedIndex = 2
        }else{
            self.arrPostList.removeAll()
            post_list_API()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
     //   NotificationCenter.default.removeObserver(self)
        //  SocketIOManager.sharedInstance.closeConnection()
    }
    //-----
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        
        do {
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsDirectory, includingPropertiesForKeys: nil, options: [])
            
            // Print the urls of the files contained in the documents directory
        } catch {
            print("Could not search for urls of files in documents directory: \(error)")
        }
       
    }
    
    
    
    @objc func methodOfReceivedNotification2(notification: Notification) {
        let notificationData = notification.object as? [String:Any]
        //  print("notificationData-=-=-=-=-=-=\(notificationData!)")
        if   let str = notificationData?["gcm.notification.text"] as? String{
            if let data = str.data(using: String.Encoding.utf8) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
               //     print(json["notification"]!)
                    let notification = json["notification"] as? [String:Any]
                    let refer_id = notification?["refer_id"]as? String ?? ""
                    let ref_artist_id = notification?["ref_artist_id"]as? String ?? ""
                    let type = notification?["type"]as? String ?? ""
                    let user_type = notification?["user_type"]as? String ?? ""
                    
                    if type == "1"{
                        appDelegate.post_id = refer_id
                        self.viewWillAppear(true)
                      /*  let vc = CommentViewController.instance(storyBoard: .Home) as! CommentViewController
                        vc.arrPostimages = []
                        vc.post_id = data.refer_id!
                        push(viewController: vc)*/
                    }else if type == "2"{
                        appDelegate.reel_id = refer_id
                        self.tabBarController!.selectedIndex = 2
                    }
                    else if type == "3"{
                        appDelegate.ref_user_id = refer_id
                        appDelegate.ref_artist_id = ref_artist_id
                        self.viewWillAppear(true)
                    }
                    else if type == "4"{
                        if user_type == "0"{
                            //fan
                            let vc = ProfileFanVC.instance(storyBoard: .Profile) as! ProfileFanVC
                            vc.fanid = refer_id
                            vc.isSeenFanProfile = true
                            push(viewController: vc)
                        }else{
                            //Artist
                            let vc = ProfileVC.instance(storyBoard: .Profile) as! ProfileVC
                            vc.userID = refer_id
                            vc.isComeFromFan = true
                            push(viewController: vc)
                        }
                        
                    }
                } catch {
                    print("Something went wrong")
                }
            }
        }
    }
    
    
    
    @IBAction func actionPost(_ sender: Any) {
        let vc = CreatePostVC.instance(storyBoard: .Home) as! CreatePostVC
        push(viewController: vc)
        
        
    }
    
    @IBAction func actionSearch(_ sender: Any) {
        let vc = SearchForArtistVC.instance(storyBoard: .Profile) as! SearchForArtistVC
        push(viewController: vc)
    }
    @IBAction func actionComment(_ sender: Any) {
        let vc = ChatListVC.instance(storyBoard: .Home) as! ChatListVC
        push(viewController: vc)
    }
    @IBAction func actionSetting(_ sender: Any) {
        if AppDataHelper.shard.isFan{
            Util.showAlertWithCallback(kAppName, message: "Sure to Logout", isWithCancel: true){
                UserDefaults.standard.removeAllUserdefault()
                SwitchNav.loginRootNavigation()
             }
            
        }else{
            let vc = SettingVC.instance(storyBoard: .Profile) as! SettingVC
            push(viewController: vc)
        }
        
    }
}

extension HomeVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPostList.count
        // return DemoSource.shared.demoData.count  // video play in cell
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedViewMultipleCell")  as! FeedViewMultipleCell
        cell.delegate = self
        let dict = arrPostList[indexPath.row]
        cell.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgProfile.sd_setImage(with: URL(string: ProfileImagePath +  dict.user_profile_image! ), placeholderImage: UIImage(named: "user"))
        print("cell image",ProfileImagePath +  dict.user_profile_image!)
        cell.lblTitle.text = dict.user_full_name ?? ""
        cell.lblFeedText.text = dict.description
        cell.lblTime.text =  convertDateString(dateString: dict.created_at , fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "MM-dd-yyyy")
        
        cell.myImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.myImg.sd_setImage(with: URL(string: ProfileImagePath +  AppDataHelper.shard.logins.profile_image! ), placeholderImage: UIImage(named: "user"))
        //like button
        if  dict.is_like  == "1"{
            cell.imgLike.image = UIImage(named: "like")
        }else{
            cell.imgLike.image = UIImage(named: "unlike")
        }
        if dict.total_like == "0"{
            cell.lblLike.text = dict.total_like! + " like"
        }
        else if dict.total_like == "1"{
            cell.lblLike.text = dict.total_like! + " like"
        }else{
            cell.lblLike.text = dict.total_like! + " likes"
        }
        //comment
        if dict.total_comment == "0"{
            cell.lblcomment.text =   "\(dict.total_comment ?? "0") comment"
        }else if dict.total_comment == "1"{
            cell.lblcomment.text = "\(dict.total_comment ?? "1") comment"
        }
        else{
            cell.lblcomment.text = "\(dict.total_comment ?? "") comments"
        }
        
        
        if dict.post_type == "images"{
            if  (dict.post_images != nil) {
                cell.constraintImageHeight.constant = 201
                if dict.post_images?.count == 1 {
                    cell.imgViewFeed?.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
                    cell.imgViewFeed?.sd_setImage(with: URL(string: PostImagePath + dict.post_images![0].image!), placeholderImage: UIImage(named: "user"))
                    cell.btnPlay.isHidden = true
                    
                    cell.imgViewFeed.isHidden = false
                    cell.viewAllImg.isHidden = false
                    cell.stackTwoImage.isHidden = true
                }
                else if dict.post_images?.count == 2 {
                    cell.imgViewFeed?.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
                    cell.imgViewFeed?.sd_setImage(with: URL(string: PostImagePath + dict.post_images![0].image!), placeholderImage: UIImage(named: "user"))
                    cell.imgViewFeed1?.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
                    cell.imgViewFeed1?.sd_setImage(with: URL(string: PostImagePath + dict.post_images![1].image!), placeholderImage: UIImage(named: "user"))
                    cell.imgViewFeed1.isHidden = false
                    cell.imgViewFeed.isHidden = false
                    cell.viewAllImg.isHidden = false
                    cell.stackTwoImage.isHidden = false
                    cell.viewMoreImages.isHidden = true
                }
                
                else if dict.post_images!.count >= 3 {
                    cell.imgViewFeed.isHidden = false
                    cell.imgViewFeed?.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
                    cell.imgViewFeed?.sd_setImage(with: URL(string: PostImagePath + dict.post_images![0].image!), placeholderImage: UIImage(named: "user"))
                    cell.imgViewFeed1?.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
                    cell.imgViewFeed1?.sd_setImage(with: URL(string: PostImagePath + dict.post_images![1].image!), placeholderImage: UIImage(named: "user"))
                    cell.imgViewFeed2?.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
                    cell.imgViewFeed2?.sd_setImage(with: URL(string: PostImagePath + dict.post_images![2].image!), placeholderImage: UIImage(named: "user"))
                    
                    
                    cell.imgViewFeed.isHidden = false
                    cell.viewAllImg.isHidden = false
                    cell.imgViewFeed1.isHidden = false
                    cell.stackTwoImage.isHidden = false
                    cell.viewMoreImages.isHidden = false
                }else{
                    cell.imgViewFeed.image  = UIImage(named: "user")
                }
            }
        }
        
        
        if indexPath.row == (arrPostList.count - 1) && (tableViewFeed.contentOffset.y > pointContentOffset.y) {
            if !isDownloading {
                isDownloading = true
                if arrPostList.count % appDelegate.pageCount == 0 {
                    page += appDelegate.pageCount
                    self.post_list_API()
                }
            }
        }
        
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = arrPostList[indexPath.row]
        if dict.post_type == "images"{
            let obj = arrPostList[indexPath.row]
            let postId = arrPostList[indexPath.row].id
            let vc = CommentViewController.instance(storyBoard: .Home) as! CommentViewController
            vc.arrPostimages = obj.post_images
            vc.post_id = postId ?? ""
            push(viewController: vc)
            
        }
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let objPost = arrPostList[indexPath.row]
        if objPost.post_type  == "video" {
            
        } else {
            guard let cell = cell as? FeedViewMultipleCell else { return }
            if objPost.total_like == "0"{
                cell.lblLike.text = objPost.total_like! + " like"
            }
            else if objPost.total_like == "1"{
                cell.lblLike.text = objPost.total_like! + " like"
            }else{
                cell.lblLike.text = objPost.total_like! + " likes"
            }
        }
    }
    
}


extension HomeVC: UIPopoverPresentationControllerDelegate,FeedViewCellDelegate , HomePopupOverDelegate{
    
    
    func onButtonClick(button str: HomeList, index: IndexPath) {
        let  postid = arrPostList[index.row].id!
        if str == .deletePost{
            DeletePost_API(postid, index: index)
        }else if  str == .savePost{
            add_pinboard_API(postid, index: index)
        }else{
            //report
            
            report_user_API(postid, index: index)
            
        }
    }
    
    func AllComments(cell: UITableViewCell) {
        guard  let indexPath = tableViewFeed.indexPath(for: cell) else {
            return
        }
        let postId = arrPostList[indexPath.row].id
        let vc = CommentViewController.instance(storyBoard: .Home) as! CommentViewController
        vc.post_id = postId ?? ""
        vc.arrPostimages = arrPostList[indexPath.row].post_images
        push(viewController: vc)
    }
    
    func feedViewCellUserProfile(cell: UITableViewCell) {
        guard  let indexPath = tableViewFeed.indexPath(for: cell) else {
            return
        }
        let userID = arrPostList[indexPath.row].user_id
        
        
        if AppDataHelper.shard.logins.id == userID{
            self.tabBarController?.selectedIndex = 4
        }else{
            let vc = ProfileVC.instance(storyBoard: .Profile) as! ProfileVC
            vc.userID = userID!
            vc.isComeFromFan = true
            push(viewController: vc)
        }
        //
        
        
    }
    
    func feedViewShare(cell: UITableViewCell) {
        guard  let indexPath = tableViewFeed.indexPath(for: cell) else {
            return
        }
        guard let celli = cell as? FeedViewMultipleCell else {
            return
        }
        let dict = arrPostList[indexPath.row]
        let vc = FriendShareListVC.instance(storyBoard: .Video) as! FriendShareListVC
        vc.artistId = dict.id!
        vc.enumShareThrough = .post
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
        
        
    }
    
    func feedViewCellLikePost(cell: UITableViewCell) {
        guard  let indexPath = tableViewFeed.indexPath(for: cell) else {
            return
        }
        guard let celli = cell as? FeedViewMultipleCell else {
            return
        }
        let dict = arrPostList[indexPath.row]
        var param = [String:Any]()
        param[params.kpost_id] = dict.id
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        ServerManager.shared.POST(url: ApiAction.post_like , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(LikeModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrPostList[indexPath.row].total_like = "\(obj.count ?? 0)"
                if  dict.is_like  == "1"{
                    //likepost
                    self?.arrPostList[indexPath.row].is_like = "0"
                    celli.imgLike.image = UIImage(named: "unlike")
                }else{
                    self?.arrPostList[indexPath.row].is_like = "1"
                    celli.imgLike.image = UIImage(named: "like")
                }
                self?.tableView(self!.tableViewFeed, willDisplay: cell, forRowAt: indexPath)
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
        
    }
    func DeletePost_API(_ postid : String , index : IndexPath){
        var param = [String : Any]()
        param[params.kpost_id] = postid
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        ServerManager.shared.POST(url: ApiAction.delete_post , param: param, false,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(LikeModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrPostList.remove(at:index.row)
                self?.tableViewFeed.deleteRows(at: [index], with: .fade)
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    
    func report_user_API(_ postid : String , index : IndexPath){
        let  user_id = self.arrPostList[index.row].user_id
        var param = [String : Any]()
        param[params.kpost_id] = postid
        param[params.block_id] = user_id
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        
        ServerManager.shared.POST(url: ApiAction.report_user , param: param, false,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(LikeModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                Artistarmy.show(message: obj.message!)
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    
    func add_pinboard_API(_ postid : String , index : IndexPath){
        var param = [String : Any]()
        param[params.kpost_id] = postid
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        ServerManager.shared.POST(url: ApiAction.add_pinboard , param: param, false,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                let  is_saved = self?.arrPostList[index.row].is_saved
                if is_saved == "0"{
                    self?.arrPostList[index.row].is_saved = "1"
                }else{
                    self?.arrPostList[index.row].is_saved = "0"
                }
                Artistarmy.show(message: obj.message ?? "")
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    func popoverView(Cell: FeedViewMultipleCell, sender: UIButton) {
        guard let indexPath = tableViewFeed.indexPath(for: Cell) else {
            return
        }
        let alertController = HomePopupOver.instance(storyBoard: .Home) as! HomePopupOver
        let isSaved = arrPostList[indexPath.row].is_saved
        alertController.modalPresentationStyle = .popover
        let popover = alertController.popoverPresentationController
        popover?.delegate = self
        alertController.delegate = self
        alertController.myid = arrPostList[indexPath.row].user_id!
        alertController.isSaved = isSaved ?? "0"
        alertController.myIndex = indexPath
        popover?.sourceView = sender
        popover?.permittedArrowDirections =  .up
        if arrPostList[indexPath.row].user_id! == AppDataHelper.shard.logins.id{
            alertController.preferredContentSize = CGSize(width: 90, height: 80)
        }else{
            alertController.preferredContentSize = CGSize(width: 90, height: 80)
        }
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}





extension HomeVC {
    func post_list_API(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kpost_id] = appDelegate.post_id
        param[params.kstart] =  page
        param[params.kpageCount] =   appDelegate.pageCount
        
        ServerManager.shared.POST(url: ApiAction.post_list , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(HomeListingModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                appDelegate.reel_id = ""
                appDelegate.post_id = ""
                appDelegate.ref_user_id = ""
                appDelegate.ref_artist_id = ""
                print("Success")
                self?.arrPostList.append(contentsOf: obj.post_list!)
                self?.tableViewFeed.reloadData()
                if obj.post_list!.count == 0{
                    self?.isDownloading = true
                }else{
                    self?.isDownloading = false
                }
             //   self?.actionPost()
            } else {
                print("failure")
                self?.isDownloading = true
                //presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    
    
    
    func actionPost(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kreels_id] =  appDelegate.reel_id
        param[params.kstart] =  page
        param[params.kpageCount] =   appDelegate.pageCount
        param[params.ktype] =  "2"
        
        
        ServerManager.shared.POST(url: ApiAction.reels_list , param: param, false,header: nil) {   (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(FeedModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                
                for dict in obj.reels_list!{
                    
                    let videoLink =  dict.reel_video
                    let aa = VideoImagePath  + videoLink!
                    self.items.append(URL(string: aa)!)
                }
                
                self.checkPreload()
            } else {
                print("failure")
              //  presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    func checkPreload() {
        let urls = items
            .suffix(from: min(0, items.count))
            .prefix(2)
        VideoPreloadManager.shared.set(waiting: Array(urls))
    }
}




struct LikeModel: Codable {
    let code, message: String?
    let count: Int?
}


func convertDateString(dateString : String!, fromFormat sourceFormat : String!, toFormat desFormat : String!) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = sourceFormat
    let date = dateFormatter.date(from: dateString)
    dateFormatter.dateFormat = desFormat
    return dateFormatter.string(from: date!)
}



