

import Foundation
struct Post_list : Codable {
	let id : String?
	let user_id : String?
	let group_id : String?
	let post_type : String?
	let description : String?
	var total_like : String?
	let total_comment : String?
	let created_at : String?
    var is_like : String?
    var is_saved : String?
	let user_profile_image : String?
	let user_full_name : String?
	let post_images : [Post_images]?
	let post_comments : [Post_comments]?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case user_id = "user_id"
		case group_id = "group_id"
		case post_type = "post_type"
		case description = "description"
		case total_like = "total_like"
		case total_comment = "total_comment"
		case created_at = "created_at"
		case is_like = "is_like"
		case user_profile_image = "user_profile_image"
		case user_full_name = "user_full_name"
		case post_images = "post_images"
		case post_comments = "post_comments"
        case is_saved  = "is_saved"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
		group_id = try values.decodeIfPresent(String.self, forKey: .group_id)
		post_type = try values.decodeIfPresent(String.self, forKey: .post_type)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		total_like = try values.decodeIfPresent(String.self, forKey: .total_like)
		total_comment = try values.decodeIfPresent(String.self, forKey: .total_comment)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
		is_like = try values.decodeIfPresent(String.self, forKey: .is_like)
		user_profile_image = try values.decodeIfPresent(String.self, forKey: .user_profile_image)
		user_full_name = try values.decodeIfPresent(String.self, forKey: .user_full_name)
		post_images = try values.decodeIfPresent([Post_images].self, forKey: .post_images)
		post_comments = try values.decodeIfPresent([Post_comments].self, forKey: .post_comments)
        is_saved = try values.decodeIfPresent(String.self, forKey: .is_saved)
	}

}
