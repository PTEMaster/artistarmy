//
//  LoginVC.swift
//  IRatePro
//
//  Created by mac on 24/12/20.
// SignupVC



import UIKit

class SignupVC: KBaseViewController ,MoveToForgotPassDelegate {
    func onButtonClick() {
        let vc = ForgotPasswordVC.instance(storyBoard: .Signup) as! ForgotPasswordVC
        vc.cCode  = (btnCountryCode.titleLabel?.text)!
        vc.MobileNumbwer = tfMobile.text!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBOutlet weak var btnCountryCode: UIButton!
    @IBOutlet weak var tfMobile: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
        let cCode =     getCountryCallingCode(countryRegionCode: countryCode)
            btnCountryCode.setTitle("+" + cCode, for: .normal)
        }
      //  let custombar = self.tabBarController as? CustomTabBarController
     //hp   custombar?.isHiddenTabBar(hidden: true)
    }
    
    @IBAction func forgotPassword(_ sender: Any) {
       let vc = LoginVC.instance(storyBoard: .Signup) as! LoginVC
       // vc.ISComeFromForgot = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    

    
    
    
    @IBAction func actionLogin(_ sender: Any) {
        let valid = validation()
        if valid.status {
            call_SigninAPI()
        }else{
            presentAlert("", msgStr: valid.message, controller: self)
        }
       
        

    }
    
    
    
    @IBAction func actionCountryPicker(_ sender: Any) {
        let countryPicker = TDCountryPicker(style: .grouped)
        countryPicker.delegate = self
        
        // Display calling codes
        countryPicker.showCallingCodes = false
    
         let pickerNavigationController = UINavigationController(rootViewController: countryPicker)
        self.present(pickerNavigationController, animated: true, completion: nil)
    }
    
    
    
    
    
    func validation() -> (status:Bool, message:String) {
        var msg:String = ""
        var status:Bool = true
        
        if let fullname = tfMobile.text , fullname.isEmpty {
            status = false
            msg = Validation.kEnterMobileNumber.rawValue
        }
       
        return (status:status, message:msg)
        
        }
        
        
    
    func call_SigninAPI(){
        var param = [String : Any]()
        param[params.kmobile_number] = tfMobile.text
        param[params.kcountry_code] = btnCountryCode.titleLabel?.text
        param[params.ksocial] = ""
        
        ServerManager.shared.POST(url: ApiAction.signup_mobile , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            
           /* if data.count > 0 {
                          do {
                              let jsonDecoder = JSONDecoder()
                              _ = try jsonDecoder.decode(SignupModel.self, from: data)
                              
                          } catch DecodingError.typeMismatch( _, let context) {
                              print("DecodingError.typeMismatch: \(context.debugDescription)")
                              for i in 0..<context.codingPath.count { print("  [\(i)] = \(context.codingPath[i])") }
                              
                          } catch DecodingError.valueNotFound( _, let context) {
                              print("DecodingError.valueNotFound: \(context.debugDescription)")
                              for i in 0..<context.codingPath.count { print("  [\(i)] = \(context.codingPath[i])") }
                              
                          } catch DecodingError.keyNotFound( _, let context) {
                              print("DecodingError.keyNotFound: \(context.debugDescription)")
                              for i in 0..<context.codingPath.count { print("  [\(i)] = \(context.codingPath[i])") }
                          } catch DecodingError.dataCorrupted(let context) {
                              print("DecodingError.dataCorrupted: \(context.debugDescription)")
                              for i in 0..<context.codingPath.count { print("  [\(i)] = \(context.codingPath[i])") }
                              
                          } catch let err {
                              print(err.localizedDescription)
                          }
                      }*/
           guard  let obj = try? JSONDecoder().decode(SignupModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                        // Mark:- set user detail in local and auto login
              //  UserDefaults.standard.setLoggedIn(value: true)
             //   AppDataHelper.shard.logins = obj
             //   UserDefaults.standard.setUserData(value: data)
                
              
                let vc = EnterOTPVC.instance(storyBoard: .Signup) as! EnterOTPVC
                vc.delegate = self
                vc.cCode = self.btnCountryCode.titleLabel?.text ?? ""
                vc.MobileNumber = self.tfMobile.text!
                vc.modalPresentationStyle = .overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                self.present(vc, animated: true, completion: nil)
               // presentAlert("Your OTP", msgStr: obj.userinfo?.otp, controller: self)
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
        
    }
    
}
    
    
    
    




extension SignupVC: TDCountryPickerDelegate {
    
    func countryPicker(_ picker: TDCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        _ = picker.navigationController?.popToRootViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
       // let flag =  picker.getFlag(countryCode: code)
       btnCountryCode.setTitle(dialCode, for: .normal)
     //   btnRegister.setImage(flag, for: .normal)
    }
    
    func getCountryCallingCode(countryRegionCode:String)->String{
        let prefixCodes = ["AF": "93", "AE": "971", "AL": "355", "AN": "599", "AS":"1", "AD": "376", "AO": "244", "AI": "1", "AG":"1", "AR": "54","AM": "374", "AW": "297", "AU":"61", "AT": "43","AZ": "994", "BS": "1", "BH":"973", "BF": "226","BI": "257", "BD": "880", "BB": "1", "BY": "375", "BE":"32","BZ": "501", "BJ": "229", "BM": "1", "BT":"975", "BA": "387", "BW": "267", "BR": "55", "BG": "359", "BO": "591", "BL": "590", "BN": "673", "CC": "61", "CD":"243","CI": "225", "KH":"855", "CM": "237", "CA": "1", "CV": "238", "KY":"345", "CF":"236", "CH": "41", "CL": "56", "CN":"86","CX": "61", "CO": "57", "KM": "269", "CG":"242", "CK": "682", "CR": "506", "CU":"53", "CY":"537","CZ": "420", "DE": "49", "DK": "45", "DJ":"253", "DM": "1", "DO": "1", "DZ": "213", "EC": "593", "EG":"20", "ER": "291", "EE":"372","ES": "34", "ET": "251", "FM": "691", "FK": "500", "FO": "298", "FJ": "679", "FI":"358", "FR": "33", "GB":"44", "GF": "594", "GA":"241", "GS": "500", "GM":"220", "GE":"995","GH":"233", "GI": "350", "GQ": "240", "GR": "30", "GG": "44", "GL": "299", "GD":"1", "GP": "590", "GU": "1", "GT": "502", "GN":"224","GW": "245", "GY": "595", "HT": "509", "HR": "385", "HN":"504", "HU": "36", "HK": "852", "IR": "98", "IM": "44", "IL": "972", "IO":"246", "IS": "354", "IN": "91", "ID":"62", "IQ":"964", "IE": "353","IT":"39", "JM":"1", "JP": "81", "JO": "962", "JE":"44", "KP": "850", "KR": "82","KZ":"77", "KE": "254", "KI": "686", "KW": "965", "KG":"996","KN":"1", "LC": "1", "LV": "371", "LB": "961", "LK":"94", "LS": "266", "LR":"231", "LI": "423", "LT": "370", "LU": "352", "LA": "856", "LY":"218", "MO": "853", "MK": "389", "MG":"261", "MW": "265", "MY": "60","MV": "960", "ML":"223", "MT": "356", "MH": "692", "MQ": "596", "MR":"222", "MU": "230", "MX": "52","MC": "377", "MN": "976", "ME": "382", "MP": "1", "MS": "1", "MA":"212", "MM": "95", "MF": "590", "MD":"373", "MZ": "258", "NA":"264", "NR":"674", "NP":"977", "NL": "31","NC": "687", "NZ":"64", "NI": "505", "NE": "227", "NG": "234", "NU":"683", "NF": "672", "NO": "47","OM": "968", "PK": "92", "PM": "508", "PW": "680", "PF": "689", "PA": "507", "PG":"675", "PY": "595", "PE": "51", "PH": "63", "PL":"48", "PN": "872","PT": "351", "PR": "1","PS": "970", "QA": "974", "RO":"40", "RE":"262", "RS": "381", "RU": "7", "RW": "250", "SM": "378", "SA":"966", "SN": "221", "SC": "248", "SL":"232","SG": "65", "SK": "421", "SI": "386", "SB":"677", "SH": "290", "SD": "249", "SR": "597","SZ": "268", "SE":"46", "SV": "503", "ST": "239","SO": "252", "SJ": "47", "SY":"963", "TW": "886", "TZ": "255", "TL": "670", "TD": "235", "TJ": "992", "TH": "66", "TG":"228", "TK": "690", "TO": "676", "TT": "1", "TN":"216","TR": "90", "TM": "993", "TC": "1", "TV":"688", "UG": "256", "UA": "380", "US": "1", "UY": "598","UZ": "998", "VA":"379", "VE":"58", "VN": "84", "VG": "1", "VI": "1","VC":"1", "VU":"678", "WS": "685", "WF": "681", "YE": "967", "YT": "262","ZA": "27" , "ZM": "260", "ZW":"263" , "":""]
        let countryDialingCode = prefixCodes[countryRegionCode]
        return countryDialingCode!
    }
}

   




struct SuccessModel : Codable {
    let code : String?
    let message : String?

    enum CodingKeys: String, CodingKey {

        case code = "code"
        case message = "message"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(String.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }

}





struct SignupModel : Codable {
    let code : String?
    let message : String?
    let userinfo : Userinfo?

    enum CodingKeys: String, CodingKey {

        case code = "code"
        case message = "message"
        case userinfo = "userinfo"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(String.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        userinfo = try values.decodeIfPresent(Userinfo.self, forKey: .userinfo)
    }

}
struct Userinfo : Codable {
    let id : String?
    let country_code : String?
    let mobile_number : String?
    let password : String?
    let email : String?
    let full_name : String?
    let artist_name : String?
    let date_of_birth : String?
    let genre_cat : String?
    let biography : String?
    let former_bands : String?
    let profile_image : String?
    let cover_image : String?
    let address : String?
    let state : String?
    let city : String?
    let pincode : String?
    let user_longitude : String?
    let user_latitude : String?
    let user_type : String?
    let device_type : String?
    let ios_token : String?
    let android_token : String?
    let reset_code : String?
    let social : String?
    let spotify : String?
    let youtube : String?
    let instagram : String?
    let facebook : String?
    let otp : String?
    let token : String?
    let status : String?
    let notification_status : String?
    let created_at : String?
    let description : String?
    let song : String?
    let genre_name : String?
    let genre_image : String?
    var total_donate_bit : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case country_code = "country_code"
        case mobile_number = "mobile_number"
        case password = "password"
        case email = "email"
        case full_name = "full_name"
        case artist_name = "artist_name"
        case date_of_birth = "date_of_birth"
        case genre_cat = "genre_cat"
        case biography = "biography"
        case former_bands = "former_bands"
        case profile_image = "profile_image"
        case cover_image = "cover_image"
        case address = "address"
        case state = "state"
        case city = "city"
        case pincode = "pincode"
        case user_longitude = "user_longitude"
        case user_latitude = "user_latitude"
        case user_type = "user_type"
        case device_type = "device_type"
        case ios_token = "ios_token"
        case android_token = "android_token"
        case reset_code = "reset_code"
        case social = "social"
        case spotify = "spotify"
        case youtube = "youtube"
        case instagram = "instagram"
        case facebook = "facebook"
        case otp = "otp"
        case token = "token"
        case status = "status"
        case notification_status = "notification_status"
        case created_at = "created_at"
        case description = "description"
        case song = "song"
        case genre_name = "genre_name"
        case genre_image = "genre_image"
         case  total_donate_bit = "total_donate_bit"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        country_code = try values.decodeIfPresent(String.self, forKey: .country_code)
        mobile_number = try values.decodeIfPresent(String.self, forKey: .mobile_number)
        password = try values.decodeIfPresent(String.self, forKey: .password)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        full_name = try values.decodeIfPresent(String.self, forKey: .full_name)
        artist_name = try values.decodeIfPresent(String.self, forKey: .artist_name)
        date_of_birth = try values.decodeIfPresent(String.self, forKey: .date_of_birth)
        genre_cat = try values.decodeIfPresent(String.self, forKey: .genre_cat)
        biography = try values.decodeIfPresent(String.self, forKey: .biography)
        former_bands = try values.decodeIfPresent(String.self, forKey: .former_bands)
        profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
        cover_image = try values.decodeIfPresent(String.self, forKey: .cover_image)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        state = try values.decodeIfPresent(String.self, forKey: .state)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        pincode = try values.decodeIfPresent(String.self, forKey: .pincode)
        user_longitude = try values.decodeIfPresent(String.self, forKey: .user_longitude)
        user_latitude = try values.decodeIfPresent(String.self, forKey: .user_latitude)
        user_type = try values.decodeIfPresent(String.self, forKey: .user_type)
        device_type = try values.decodeIfPresent(String.self, forKey: .device_type)
        ios_token = try values.decodeIfPresent(String.self, forKey: .ios_token)
        android_token = try values.decodeIfPresent(String.self, forKey: .android_token)
        reset_code = try values.decodeIfPresent(String.self, forKey: .reset_code)
        social = try values.decodeIfPresent(String.self, forKey: .social)
        spotify = try values.decodeIfPresent(String.self, forKey: .spotify)
        youtube = try values.decodeIfPresent(String.self, forKey: .youtube)
        instagram = try values.decodeIfPresent(String.self, forKey: .instagram)
        facebook = try values.decodeIfPresent(String.self, forKey: .facebook)
        otp = try values.decodeIfPresent(String.self, forKey: .otp)
        token = try values.decodeIfPresent(String.self, forKey: .token)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        notification_status = try values.decodeIfPresent(String.self, forKey: .notification_status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        song = try values.decodeIfPresent(String.self, forKey: .song)
        genre_name = try values.decodeIfPresent(String.self, forKey: .genre_name)
        genre_image = try values.decodeIfPresent(String.self, forKey: .genre_image)
        total_donate_bit = try values.decodeIfPresent(String.self, forKey: .total_donate_bit)
    }

}

