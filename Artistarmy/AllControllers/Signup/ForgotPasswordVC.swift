//
//  ForgotPasswordVC.swift
//  IRatePro
//
//  Created by mac on 20/01/21.
//

import UIKit

class ForgotPasswordVC: UIViewController {
    
    
    @IBOutlet weak var tfNewPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    var cCode = ""
    var MobileNumbwer = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfConfirmPassword.setLeftPaddingPoints(5)
        tfNewPassword.setLeftPaddingPoints(5)

        // Do any additional setup after loading the view.
    }
    


    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func actionSend(_ sender: Any) {
        
        let valid = validation()
        if valid.status {
            
            call_forgot_password_API()
        }else{
            presentAlert("", msgStr: valid.message, controller: self)
        }
    }
    func isValidEmail(candidate: String) -> Bool {
        
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        var valid = NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
        if valid {
            valid = !candidate.contains("..")
        }
        return valid
    }
    
    
    func validation() -> (status:Bool, message:String) {
        var msg:String = ""
        var status:Bool = true
        
        if let fullname = tfNewPassword.text , fullname.isEmpty {
            status = false
            msg = Validation.kEnterPassword.rawValue
        }
        else if tfNewPassword.text != tfConfirmPassword.text{
            status = false
            msg = Validation.kPassowrdNotMatched.rawValue
        }
        return (status:status, message:msg)
        
        }
    
    func call_forgot_password_API(){
       var param = [String : Any]()
        
        
        param[params.kmobile_number] = MobileNumbwer
        param[params.kpassword] = tfNewPassword.text
        param[params.kcountry_code] = cCode
       // param[params.kfcm_token] = ""
        param[params.kios_token] = ""
       param[params.kdevice_type] = "2"
        param[params.ksocial] = ""
        
        ServerManager.shared.POST(url: ApiAction.signup_pass , param: param, true,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(SignupModel.self, from: data) else {
                return
            }
            
            if obj.code == ResponseApis.KSuccess {
               DispatchQueue.main.async(execute: {
                    let _ = presentAlertWithOptions("", message: obj.message!, controller: self, buttons: ["Ok"]) { (alert, actionTag) in
                        if actionTag == 0 {
                           /* for controller in self.navigationController!.viewControllers as Array {
                                if controller.isKind(of: LoginVC.self) {
                                    self.navigationController!.popToViewController(controller, animated: true)
                                    break
                                }
                            }*/
                            let vc = LoginVC.instance(storyBoard: .Signup) as! LoginVC
                            self.navigationController?.pushViewController(vc, animated: true)
                            
                        }
                    }
                })
                
                
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
      
    }
    

}
