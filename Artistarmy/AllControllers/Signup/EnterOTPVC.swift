//
//  EnterOTPVC.swift
//  IRatePro
//
//  Created by mac on 20/01/21.
//

import UIKit
protocol MoveToForgotPassDelegate {
    func onButtonClick()
}
class EnterOTPVC: UIViewController {

    @IBOutlet weak var txtDPOTPView: HPOTPView!
    var cCode = ""
    var MobileNumber = ""
    var ISComeFromForgot = false
    var txtOTPView: HPOTPView!
    var strOTP = ""
    
 /*   @IBOutlet weak var txtFd1: UITextField!
    @IBOutlet weak var txtFd2: UITextField!
    @IBOutlet weak var txtFd3: UITextField!
    @IBOutlet weak var txtFd4: UITextField!*/
    
    var delegate: MoveToForgotPassDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
       
        txtDPOTPView.hpOTPViewDelegate = self
      /*  txtDPOTPView.fontTextField = UIFont.systemFont(ofSize: 40)
        txtDPOTPView.textEdgeInsets = UIEdgeInsets(top: 0, left: -1, bottom: 0, right: 0)
        txtDPOTPView.editingTextEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)*/
        
    }
    

  
    
    
    @IBAction func actionVerify(_ sender: Any) {
        let valid = validation()
        if valid.status {
            call_otp_verificationAPI()
        }else{
            presentAlert("", msgStr: valid.message, controller: self)
        }
        
    }
    
    @IBAction func actiondismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func validation() -> (status:Bool, message:String) {
        var msg:String = ""
        var status:Bool = true
        print(txtDPOTPView.validate())
        if strOTP == "" || strOTP.count < 4{
            status = false
            msg = Validation.kEnterPassword.rawValue
        }
        return (status:status, message:msg)
        
        }
    
    

    
    
    func call_otp_verificationAPI(){
        var param = [String : Any]()
        param[params.kmobile_number] = MobileNumber
        param[params.kcountry_code] = cCode
        param[params.kmobile_otp] = strOTP
        
        ServerManager.shared.POST(url: ApiAction.otp_verification , param: param, true,header: nil) { (data, error) in
          guard let data = data else {
                print("data not available")
                return
            }
           guard  let obj = try? JSONDecoder().decode(SignupModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
        
                self.dismiss(animated: true) {
                    self.delegate?.onButtonClick()
                    
                }
               
                
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
        
    }
    }



extension EnterOTPVC: HPOTPViewDelegate{

    
    func hpOTPViewAddText(_ text: String, at position: Int) {
      //  print("addText:- " + text + " at:- \(position)" )
        strOTP = text
       // print(strOTP)
       // print(txtDPOTPView.validate())
    }
    
    func hpOTPViewRemoveText(_ text: String, at position: Int) {
      //  print("removeText:- " + text + " at:- \(position)" )
        strOTP = text
        print(strOTP)
    }
    
    func hpOTPViewChangePositionAt(_ position: Int) {
     //   print("at:-\(position)")
    }
    func hpOTPViewBecomeFirstResponder() {
        
    }
    func hpOTPViewResignFirstResponder() {
        
    }
}



struct OtpModel: Codable {
    let success,  message: String
}

struct ModelNotifiCount: Codable {
    let success, count ,  message: String
}




