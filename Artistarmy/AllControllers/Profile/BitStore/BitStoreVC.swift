//
//  HomeVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit
import SDWebImage
class BitStoreVC: KBaseViewController {
    @IBOutlet weak var tblBit: UITableView!
    @IBOutlet weak var tfAmount: UITextField!
    @IBOutlet weak var lblBitToName: UILabel!
    @IBOutlet weak var lblBitCount: UILabel!
    @IBOutlet weak var tvMsg: KMPlaceholderTextView!
    @IBOutlet weak var viewDonate: UIView!
    @IBOutlet weak var constHeightViewDonate: NSLayoutConstraint!
    var arrbitPack  = [BitPackList]()
    var userId = ""
    var bitToName = ""
    var isComeFromBitStore = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        tblBit.reloadData()
        
        if isComeFromBitStore{
            viewDonate.isHidden = true
            constHeightViewDonate.constant = 0
            viewDonate.frame.size.height = 20
        }else{
            lblBitCount.text = "You Got " + AppDataHelper.shard.logins.total_donate_bit!   +  " Bits"
            
            lblBitToName.text = "Bits to " + bitToName
            viewDonate.isHidden = false
            constHeightViewDonate.constant = 510
        }
        get_bitpack_API()
    }
    
    @IBAction func actionBack(_ sender: Any) {
        pop()
    }
    @IBAction func actionDonte(_ sender: Any) {
        let valid = validation()
        if valid.status {
            donate_bits_API()
        }else{
            presentAlert("", msgStr: valid.message, controller: self)
        }
    }
    
    @IBAction func actionPlay(_ sender: Any) {
    }
    
    
    func validation() -> (status:Bool, message:String) {
        var msg:String = ""
        var status:Bool = true
        let amt = Double(tfAmount.text ?? "0.0")
        let totalAmt = Double(AppDataHelper.shard.logins.total_donate_bit ?? "0.0")
        if tfAmount.text == ""   {
            status = false
            msg = Validation.kdonateAmount.rawValue
        }
        
        else  if amt! > totalAmt! {
            status = false
            msg = Validation.kdonateValidAmount.rawValue
        }
        return (status:status, message:msg)
        
    }
    
    
}
extension BitStoreVC: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrbitPack.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BitStoreCell", for: indexPath) as! BitStoreCell
        let obj = arrbitPack[indexPath.row]
        cell.lblBitPack.text = "Bitpack " + "\(indexPath.row + 1)"
        cell.lblBit.text = obj.bit_amount_d
        cell.lblEUR.text = obj.euro_amount_d
        
        cell.bgImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.bgImg.sd_setImage(with: URL(string: goalImagePath +  obj.image! ), placeholderImage: UIImage(named: ""))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = arrbitPack[indexPath.row]
        if AppDataHelper.shard.isFan{
            select_bitpack_API(obj.id!, bitAmount: obj.amount_in_bit!)
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}
extension BitStoreVC {
    func get_bitpack_API(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        ServerManager.shared.POST(url: ApiAction.get_bitpack , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            
            guard  let obj = try? JSONDecoder().decode(BitStoreModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.lblBitCount.text = "You Got " + obj.total_bit!   +  " Bits"
                AppDataHelper.shard.logins.total_donate_bit = obj.total_bit!
                self?.arrbitPack = obj.bitPack ?? []
                self?.tblBit.reloadData()
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    
    func donate_bits_API(){
        var param = [String : Any]()
        param[params.kdonate_bit] = tfAmount.text
        param[params.kdonate_by] = AppDataHelper.shard.logins.id
        param[params.kdonate_to] = userId
        param[params.kmessage] = tvMsg.text
        param[params.kdonate_name] = AppDataHelper.shard.logins.full_name
        
        ServerManager.shared.POST(url: ApiAction.donate_bits , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            if data.count > 0 {
                do {
                    let jsonDecoder = JSONDecoder()
                    _ = try jsonDecoder.decode(SignupModel.self, from: data)
                    
                } catch DecodingError.typeMismatch( _, let context) {
                    print("DecodingError.typeMismatch: \(context.debugDescription)")
                    for i in 0..<context.codingPath.count { print("  [\(i)] = \(context.codingPath[i])") }
                    
                } catch DecodingError.valueNotFound( _, let context) {
                    print("DecodingError.valueNotFound: \(context.debugDescription)")
                    for i in 0..<context.codingPath.count { print("  [\(i)] = \(context.codingPath[i])") }
                    
                } catch DecodingError.keyNotFound( _, let context) {
                    print("DecodingError.keyNotFound: \(context.debugDescription)")
                    for i in 0..<context.codingPath.count { print("  [\(i)] = \(context.codingPath[i])") }
                } catch DecodingError.dataCorrupted(let context) {
                    print("DecodingError.dataCorrupted: \(context.debugDescription)")
                    for i in 0..<context.codingPath.count { print("  [\(i)] = \(context.codingPath[i])") }
                    
                } catch let err {
                    print(err.localizedDescription)
                }
            }
            
            guard  let obj = try? JSONDecoder().decode(donate_bitsModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                AppDataHelper.shard.logins.total_donate_bit = "\(obj.total_donate_bit!)"
                presentAlert("", msgStr: obj.message, controller: self)
                self?.pop()
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    func select_bitpack_API(_ packId : String , bitAmount : String){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kbit_pack_id] = packId
        param[params.kbit_amount] = bitAmount
        ServerManager.shared.POST(url: ApiAction.select_bitpack , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            
            guard  let obj = try? JSONDecoder().decode(donate_bitsModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                let vc = PaymentWebViewController.instance(storyBoard: .Profile) as! PaymentWebViewController
                vc.paymentUrl = obj.url
                vc.successUrl = "success"
                vc.failureUrl = ""
                self?.push(viewController: vc)
                
                
                //   AppDataHelper.shard.logins.total_donate_bit = "\(obj.total_donate_bit!)"
             } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
}



class BitStoreCell: UITableViewCell {
    @IBOutlet weak var lblBitPack: UILabel!
    @IBOutlet weak var lblBit: UILabel!
    @IBOutlet weak var lblEUR: UILabel!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var bgImg: UIImageView!
}

struct donate_bitsModel : Codable {
    let code : String?
    let message : String?
    let total_donate_bit : String?
    let url : String?
    
    enum CodingKeys: String, CodingKey {
        case code = "code"
        case message = "message"
        case total_donate_bit = "total_donate_bit"
        case url = "url"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(String.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        total_donate_bit = try values.decodeIfPresent(String.self, forKey: .total_donate_bit)
        url = try values.decodeIfPresent(String.self, forKey: .url)
    }
}
