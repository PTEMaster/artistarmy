/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct BitPackList : Codable {
	let id : String?
	let user_id : String?
	let bit_name : String?
	let type : String?
	let bit_amount_d : String?
	let amount_in_bit : String?
	let euro_amount_d : String?
	let amount_in_euro : String?
	let image : String?
	let created_at : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case user_id = "user_id"
		case bit_name = "bit_name"
		case type = "type"
		case bit_amount_d = "bit_amount_d"
		case amount_in_bit = "amount_in_bit"
		case euro_amount_d = "euro_amount_d"
		case amount_in_euro = "amount_in_euro"
		case image = "image"
		case created_at = "created_at"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
		bit_name = try values.decodeIfPresent(String.self, forKey: .bit_name)
		type = try values.decodeIfPresent(String.self, forKey: .type)
		bit_amount_d = try values.decodeIfPresent(String.self, forKey: .bit_amount_d)
		amount_in_bit = try values.decodeIfPresent(String.self, forKey: .amount_in_bit)
		euro_amount_d = try values.decodeIfPresent(String.self, forKey: .euro_amount_d)
		amount_in_euro = try values.decodeIfPresent(String.self, forKey: .amount_in_euro)
		image = try values.decodeIfPresent(String.self, forKey: .image)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
	}

}
