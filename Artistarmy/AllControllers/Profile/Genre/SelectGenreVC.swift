//
//  CreatePostVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//  SelectGenreVC

import UIKit
import SDWebImage

class SelectGenreVC: KBaseViewController {

    @IBOutlet weak var cvImg: UICollectionView!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet var viewPopup: UIView!
    @IBOutlet var viewPopupIn: UIView!
    @IBOutlet weak var tfEnterGenre: UITextField!
    
   lazy var arrGenreCategory  =  [Genre_category]()
    var onDataAvailable : ((_ data: String) -> ())?
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfSearch.addTarget(self, action: #selector(SearchDidChange(_:)), for: .editingChanged)
        cvImg.delegate = self
        cvImg.dataSource = self
        self.Genre_category_API("")
        
      let margin: CGFloat = 16
        guard let collectionView = cvImg, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }

            flowLayout.minimumInteritemSpacing = margin
            flowLayout.minimumLineSpacing = 0
            flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
        tfEnterGenre.setLeftPaddingPoints(5.0)
    }
    

   
    @IBAction func actionBack(_ sender: Any) {
        pop()
    }
    
    @IBAction func actionAddGenre(_ sender: Any) {
    viewPopup.frame = (self.view.window?.frame)!
    viewPopup.backgroundColor = UIColor.black.withAlphaComponent(0.5)
  //  viewPopupIn.backgroundColor = UIColor.black.withAlphaComponent(0.7)
    self.view.window?.addSubview(viewPopup)
        self.test(viewTest: viewPopupIn)
    }
    @IBAction func actionSubmitGenre(_ sender: Any) {
        if !tfEnterGenre.text!.trimmingCharacters(in: .whitespaces).isEmpty {
            add_genre_API()
        }
        
    }
    @IBAction func actionClosePopup(_ sender: Any) {
        self.viewHide(viewMain: viewPopup, viewPopUP: viewPopupIn)
    }
    
    
    
     @IBAction func actionDone(_ sender: Any) {
        let even = arrGenreCategory.filter { $0.is_selected == "1" }
        let values = even.map { $0.genre_id! }
        let str = (values.map{String($0)}).joined(separator: ",")
       self.onDataAvailable?(str)
        UpdateProfile_API(str)
     }
    @objc func SearchDidChange(_ textField: UITextField) {
        let textToSearch = textField.text ?? ""
        if textToSearch.count > 2{
            self.Genre_category_API(textToSearch)
        }else if textToSearch.count == 0{
            self.Genre_category_API("")
        }
    }
    
}



extension SelectGenreVC: UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
  
    
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrGenreCategory.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectGenreCell", for: indexPath) as! SelectGenreCell
        let dict = arrGenreCategory[indexPath.row]
        cell.lblType.text = dict.genre_name

        if indexPath.row % 2 == 0 {
           cell.constTop.constant = 0
            cell.viewBottom.constant = 20
        } else {
            cell.constTop.constant = 20
             cell.viewBottom.constant = 0
        }
        if  dict.is_selected == "0"{
            cell.btncheck.isHidden = true
        }else{
            cell.btncheck.isHidden = false
        }
        cell.btncheck.tag = indexPath.row
        cell.btncheck.addTarget(self, action: #selector(userDetail(_:)), for: .touchUpInside)
        return cell
    }
    @objc func userDetail(_ sender: UIButton) {
        
         let dict = self.arrGenreCategory[sender.tag]
        let ind =  IndexPath(row: sender.tag, section: 0)
        let cell = cvImg.cellForItem(at: ind) as! SelectGenreCell
         if  dict.is_selected == "0"{
        arrGenreCategory[sender.tag].is_selected = "1"
            cell.btncheck.isHidden = false
         }else{
         arrGenreCategory[sender.tag].is_selected = "0"
            cell.btncheck.isHidden = true
         }
    
     }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      
        let cell = cvImg.cellForItem(at: indexPath) as! SelectGenreCell
        let dict = arrGenreCategory[indexPath.row]
         if  dict.is_selected == "0"{
        arrGenreCategory[indexPath.row].is_selected = "1"
            cell.btncheck.isHidden = false
         }else{
         arrGenreCategory[indexPath.row].is_selected = "0"
            cell.btncheck.isHidden = true
         }
        
    }
    
   
    
    
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let noOfCellsInRow = 2   //number of column you want
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size, height: size + 30)
    }
    
    func test(viewTest: UIView) {
            let orignalT: CGAffineTransform = viewTest.transform
            viewTest.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
            UIView.animate(withDuration: 0.4, animations: {
                
                viewTest.transform = orignalT
            }, completion:nil)
        }
    
        
        func viewHide(viewMain: UIView, viewPopUP: UIView) {
            let orignalT: CGAffineTransform = viewPopUP.transform
            UIView.animate(withDuration: 0.3, animations: {
                viewPopUP.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
            }, completion: {(sucess) in
                viewMain.removeFromSuperview()
                viewPopUP.transform = orignalT
            })
            navigationController?.navigationBar.isUserInteractionEnabled = true
            navigationController?.navigationBar.tintColor = UIColor.white
            
        }
    
}




class SelectGenreCell: UICollectionViewCell {
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var btncheck: UIButton!
    @IBOutlet weak var constTop: NSLayoutConstraint!
    @IBOutlet weak var viewWidth: NSLayoutConstraint!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewBottom: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

  
}

extension SelectGenreVC {
    func Genre_category_API(_ search : String){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.ksearch] = search
        
        ServerManager.shared.POST(url: ApiAction.genre_category , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(GenreModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrGenreCategory.removeAll()
                self?.arrGenreCategory =  obj.genre_category!
                self?.cvImg.reloadData()
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    func UpdateProfile_API(_ allId : String){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kgenre_cat] = allId
                                                              
        ServerManager.shared.POST(url: ApiAction.update_profile , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.pop()
                presentAlert("", msgStr: obj.message, controller: self)
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    func add_genre_API(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kgenre_name] = tfEnterGenre.text
                                                              
        ServerManager.shared.POST(url: ApiAction.add_genre , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
               
                presentAlert("", msgStr: obj.message, controller: self)
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
   
    
    
    
}
