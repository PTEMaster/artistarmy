//
//  HomeVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
// PinboardVC


import UIKit
import SDWebImage

import AVFoundation
import GSPlayer

class PinboardVC: KBaseViewController {
    @IBOutlet weak var tableViewFeed: UITableView!
    var items: [URL] = []
   var arrPostList = [Post_list]()
    var id = ""
    var fan_id = ""
    
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.tableViewFeed.register(UINib(nibName: "FeedViewMultipleCell", bundle: nil), forCellReuseIdentifier: "FeedViewMultipleCell")
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        items.removeAll()
        self.tabBarController?.tabBar.isHidden = true
        post_list_API()
    }
    
    
    //----video
    

    
    @IBAction func actionBack(_ sender: Any) {
        pop()
    }
    

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //MARK:- its temperory comment by #HIMANSHU PAL      
      //  NotificationCenter.default.removeObserver(self)
    }
   //-----
    
  
    
    

}



extension PinboardVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPostList.count
       // return DemoSource.shared.demoData.count  // video play in cell
    }
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  
  let cell = tableView.dequeueReusableCell(withIdentifier: "FeedViewMultipleCell")  as! FeedViewMultipleCell
     cell.delegate = self
    let dict = arrPostList[indexPath.row]
    cell.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
    cell.imgProfile.sd_setImage(with: URL(string: ProfileImagePath +  dict.user_profile_image! ), placeholderImage: UIImage(named: "user"))
    cell.lblTitle.text = dict.user_full_name ?? "Honey"
    cell.lblFeedText.text = dict.description
    cell.lblTime.text = dict.created_at
   
    cell.myImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
    cell.myImg.sd_setImage(with: URL(string: ProfileImagePath +  AppDataHelper.shard.logins.profile_image! ), placeholderImage: UIImage(named: "user"))
    //like button
    if  dict.is_like  == "1"{
        cell.imgLike.image = UIImage(named: "like")
    }else{
         cell.imgLike.image = UIImage(named: "unlike")
    }
    if dict.total_like == "0"{
        cell.lblLike.text = dict.total_like! + " like"
    }
   else if dict.total_like == "1"{
        cell.lblLike.text = dict.total_like! + " like"
    }else{
        cell.lblLike.text = dict.total_like! + " likes"
    }
    //comment
    if dict.total_comment == "0"{
        cell.lblcomment.text =   "\(dict.total_comment ?? "0") comment"
       }else if dict.total_comment == "1"{
           cell.lblcomment.text = "\(dict.total_comment ?? "1") comment"
       }
    else{
           cell.lblcomment.text = "\(dict.total_comment ?? "") comments"
       }
    
    
    if dict.post_type == "images"{
        if  (dict.post_images != nil) {
             cell.constraintImageHeight.constant = 201
            if dict.post_images?.count == 1 {
                  
                if  dict.post_type == "video"{
                    //video
                }else{
                    cell.imgViewFeed?.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
                    cell.imgViewFeed?.sd_setImage(with: URL(string: PostImagePath + dict.post_images![0].image!), placeholderImage: UIImage(named: "user"))
                    cell.btnPlay.isHidden = true
                }
                cell.imgViewFeed.isHidden = false
                 cell.viewAllImg.isHidden = false
                cell.stackTwoImage.isHidden = true
            }
           else if dict.post_images?.count == 2 {
            cell.imgViewFeed?.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
            cell.imgViewFeed?.sd_setImage(with: URL(string: PostImagePath + dict.post_images![0].image!), placeholderImage: UIImage(named: "user"))
            cell.imgViewFeed1?.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
            cell.imgViewFeed1?.sd_setImage(with: URL(string: PostImagePath + dict.post_images![1].image!), placeholderImage: UIImage(named: "user"))
                cell.imgViewFeed1.isHidden = false
                cell.imgViewFeed.isHidden = false
                 cell.viewAllImg.isHidden = false
                cell.stackTwoImage.isHidden = false
                cell.viewMoreImages.isHidden = true
            }
           
           else if dict.post_images!.count >= 3 {
                 cell.imgViewFeed.isHidden = false
            cell.imgViewFeed?.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
            cell.imgViewFeed?.sd_setImage(with: URL(string: PostImagePath + dict.post_images![0].image!), placeholderImage: UIImage(named: "user"))
            cell.imgViewFeed1?.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
            cell.imgViewFeed1?.sd_setImage(with: URL(string: PostImagePath + dict.post_images![1].image!), placeholderImage: UIImage(named: "user"))
            cell.imgViewFeed2?.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
            cell.imgViewFeed2?.sd_setImage(with: URL(string: PostImagePath + dict.post_images![2].image!), placeholderImage: UIImage(named: "user"))
            
            
                cell.imgViewFeed.isHidden = false
                 cell.viewAllImg.isHidden = false
                cell.imgViewFeed1.isHidden = false
                cell.stackTwoImage.isHidden = false
                cell.viewMoreImages.isHidden = false
            }else{
            cell.imgViewFeed.image  = UIImage(named: "user")
            }
        }
    }
    

    return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = arrPostList[indexPath.row]
        if dict.post_type == "images"{
            let obj = arrPostList[indexPath.row]
            let postId = arrPostList[indexPath.row].id
            let vc = CommentViewController.instance(storyBoard: .Home) as! CommentViewController
            vc.arrPostimages = obj.post_images
            vc.post_id = postId ?? ""
            push(viewController: vc)
            
        }
        
      
     }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let objPost = arrPostList[indexPath.row]
        if objPost.post_type  == "video" {
            
        } else {
            guard let cell = cell as? FeedViewMultipleCell else { return }
            if objPost.total_like == "0"{
                cell.lblLike.text = objPost.total_like! + " like"
            }
           else if objPost.total_like == "1"{
                cell.lblLike.text = objPost.total_like! + " like"
            }else{
                cell.lblLike.text = objPost.total_like! + " likes"
            }
        }
    }
 
}


extension PinboardVC: UIPopoverPresentationControllerDelegate,FeedViewCellDelegate , HomePopupOverDelegate{
    func onButtonClick(button str: HomeList, index: IndexPath) {
        let  postid = arrPostList[index.row].id!
        if str == .deletePost{
            DeletePost_API(postid, index: index)
        }else{
            add_pinboard_API(postid, index: index)
        }
    }
    func feedViewCellUserProfile(cell: UITableViewCell) {
        guard  let indexPath = tableViewFeed.indexPath(for: cell) else {
            return
        }
        let userID = arrPostList[indexPath.row].user_id
        if AppDataHelper.shard.logins.id == userID{
            self.tabBarController?.selectedIndex = 4
        }else{
            let vc = ProfileVC.instance(storyBoard: .Profile) as! ProfileVC
            vc.userID = userID!
            vc.isComeFromFan = true
            push(viewController: vc)
        }
        
        
    }
    
    func AllComments(cell: UITableViewCell) {
        guard  let indexPath = tableViewFeed.indexPath(for: cell) else {
            return
        }
        let postId = arrPostList[indexPath.row].id
        let vc = CommentViewController.instance(storyBoard: .Home) as! CommentViewController
        vc.post_id = postId ?? ""
        vc.arrPostimages = arrPostList[indexPath.row].post_images
        push(viewController: vc)
    }
    
    func feedViewCellLikePost(cell: UITableViewCell) {
        guard  let indexPath = tableViewFeed.indexPath(for: cell) else {
            return
        }
        guard let celli = cell as? FeedViewMultipleCell else {
            return
        }
        let dict = arrPostList[indexPath.row]
        var param = [String:Any]()
        param[params.kpost_id] = dict.id
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        ServerManager.shared.POST(url: ApiAction.post_like , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(LikeModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrPostList[indexPath.row].total_like = "\(obj.count ?? 0)"
                if  dict.is_like  == "1"{
                    //likepost
                    self?.arrPostList[indexPath.row].is_like = "0"
                    celli.imgLike.image = UIImage(named: "like")
                }else{
                    self?.arrPostList[indexPath.row].is_like = "1"
                    celli.imgLike.image = UIImage(named: "unlike")
                }
                self?.tableView(self!.tableViewFeed, willDisplay: cell, forRowAt: indexPath)
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
        
    }
    
    func popoverView(Cell: FeedViewMultipleCell, sender: UIButton) {
        guard let indexPath = tableViewFeed.indexPath(for: Cell) else {
            return
        }
        let alertController = HomePopupOver.instance(storyBoard: .Home) as! HomePopupOver
            let isSaved = arrPostList[indexPath.row].is_saved
            alertController.modalPresentationStyle = .popover
            let popover = alertController.popoverPresentationController
            popover?.delegate = self
            alertController.delegate = self
            alertController.myIndex = indexPath
            alertController.isSaved = isSaved ?? "0"
            popover?.sourceView = sender
            popover?.permittedArrowDirections =  .up
            alertController.preferredContentSize = CGSize(width: 90, height: 60)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}


extension PinboardVC {
    func post_list_API(){
        var param = [String : Any]()
        
        param[params.kuser_id] = id
        param[params.kfan_id] = fan_id
        print(param)
        ServerManager.shared.POST(url: ApiAction.get_pinboard , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(HomeListingModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                print("Success")
                //self?.arrPostList.removeAll()
                self?.arrPostList = obj.post_list!
                self?.tableViewFeed.reloadData()
               // self?.actionPost()
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    func DeletePost_API(_ postid : String , index : IndexPath){
        var param = [String : Any]()
        param[params.kpost_id] = postid
        param[params.kuser_id] = id
        ServerManager.shared.POST(url: ApiAction.delete_post , param: param, false,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(LikeModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrPostList.remove(at:index.row)
                self?.tableViewFeed.deleteRows(at: [index], with: .fade)
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    func add_pinboard_API(_ postid : String , index : IndexPath){
        var param = [String : Any]()
        param[params.kpost_id] = postid
        param[params.kuser_id] = id
        ServerManager.shared.POST(url: ApiAction.add_pinboard , param: param, false,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrPostList.remove(at:index.row)
                self?.tableViewFeed.deleteRows(at: [index], with: .fade)
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    
    func actionPost(){
        var param = [String : Any]()
        param[params.kuser_id] = id
        ServerManager.shared.POST(url: ApiAction.reels_list , param: param, false,header: nil) {   (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(FeedModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                 
                for dict in obj.reels_list!{
                    
                    let videoLink =  dict.reel_video
                    let aa = VideoImagePath  + videoLink!
                    self.items.append(URL(string: aa)!)
                }

                self.checkPreload()
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    func checkPreload() {
        let urls = items
            .suffix(from: min(0, items.count))
            .prefix(2)
        VideoPreloadManager.shared.set(waiting: Array(urls))
    }
}

