//
//  CreatePostVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit
import  SDWebImage
class FavouriteArtistVC: KBaseViewController {
    
    @IBOutlet weak var cvImg: UICollectionView!
    
    var arrfavourite_artist = [Favourite_artist]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        cvImg.delegate = self
        cvImg.dataSource = self
   
        
      let margin: CGFloat = 10
        guard let collectionView = cvImg, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
            flowLayout.minimumInteritemSpacing = margin
            flowLayout.minimumLineSpacing = 1
            flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
    }
    

   
    @IBAction func actionBack(_ sender: Any) {
        pop()
    }
   
    
    
}



extension FavouriteArtistVC: UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
  
    
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrfavourite_artist.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavouriteArtistCell", for: indexPath) as! FavouriteArtistCell
      
        let dict  = arrfavourite_artist[indexPath.row]
        
       cell.lblName.text = dict.full_name
        cell.lblcount.text = dict.total_like
        cell.img.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.img.sd_setImage(with: URL(string: ProfileImagePath +  (dict.profile_image)! ), placeholderImage: UIImage(named: "user"))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict  = arrfavourite_artist[indexPath.row]
        if   AppDataHelper.shard.logins.id != dict.artist_id {
        let vc = ProfileVC.instance(storyBoard: .Profile) as! ProfileVC
        vc.userID = dict.artist_id!
        vc.isComeFromFan = true
        push(viewController: vc)
        }
    }
     
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let noOfCellsInRow = 3   //number of column you want
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size, height: size + 90)
    }
}




class FavouriteArtistCell: UICollectionViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblcount: UILabel!
  
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

  
}

