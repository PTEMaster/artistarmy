//
//  HomeVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit
import SDWebImage

class SearchForArtistVC: KBaseViewController {
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var tblSearch: UITableView!
    
    var arrUserinfo = [Userinfo]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfSearch.addTarget(self, action: #selector(SearchDidChange(_:)), for: .editingChanged)
        // Do any additional setup after loading the view.
        Genre_category_API("")
    }
    
    @IBAction func actionBack(_ sender: Any) {
       pop()
    }
    @objc func SearchDidChange(_ textField: UITextField) {
        let textToSearch = textField.text ?? ""
        if textToSearch.count > 2{
           self.Genre_category_API(textToSearch)
        }else if textToSearch.count == 0{
           self.Genre_category_API("")
        }
    }
}

extension SearchForArtistVC: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrUserinfo.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as! SearchCell
     
            let obj = arrUserinfo[indexPath.row]
        cell.imgCheck.isHidden = false
        if obj.user_type == "0"{
            cell.imgCheck.isHidden = true
        }
        cell.lblname.text = obj.full_name
       // cell.lbltype.text = "" //obj.artist_name
        cell.imgUser.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgUser.sd_setImage(with: URL(string: ProfileImagePath +  obj.profile_image! ), placeholderImage: UIImage(named: "user"))
       
       
        return cell
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       let dict = self.arrUserinfo[indexPath.row]
        if dict.user_type == "0"{
            let vc = ProfileFanVC.instance(storyBoard: .Profile) as! ProfileFanVC
              vc.fanid = dict.id!
              vc.isSeenFanProfile = true
              push(viewController: vc)
        }else{
            let vc = ProfileVC.instance(storyBoard: .Profile) as! ProfileVC
            vc.userID = dict.id!
            vc.isComeFromFan = true
            push(viewController: vc)
        }
        
   }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
   
}






extension SearchForArtistVC {
    func Genre_category_API(_ search : String){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.ksearch] = search
        
        ServerManager.shared.POST(url: ApiAction.home_search , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SearchModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrUserinfo = obj.userinfo ?? []
                self?.tblSearch.reloadData()
            } else {
                print("failure")
               // presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
}


class SearchCell: UITableViewCell {
    @IBOutlet weak var lbltype: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var imgCheck: UIImageView!
 
    
    
}

