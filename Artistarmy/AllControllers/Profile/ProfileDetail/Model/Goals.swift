/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Goals : Codable {
	let id : String?
	let user_id : String?
	let goal_name : String?
	let amount : String?
	let exp_date : String?
	let exp_time : String?
	let description : String?
	let goal_image : String?
	let redeem_members : String?
	let created_at : String?
    let color_code : String?
    let progress_bar : String?
    

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case user_id = "user_id"
		case goal_name = "goal_name"
		case amount = "amount"
		case exp_date = "exp_date"
		case exp_time = "exp_time"
		case description = "description"
		case goal_image = "goal_image"
		case redeem_members = "redeem_members"
		case created_at = "created_at"
        case color_code = "color_code"
        case progress_bar  = "progress_bar"
        
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
		goal_name = try values.decodeIfPresent(String.self, forKey: .goal_name)
		amount = try values.decodeIfPresent(String.self, forKey: .amount)
		exp_date = try values.decodeIfPresent(String.self, forKey: .exp_date)
		exp_time = try values.decodeIfPresent(String.self, forKey: .exp_time)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		goal_image = try values.decodeIfPresent(String.self, forKey: .goal_image)
		redeem_members = try values.decodeIfPresent(String.self, forKey: .redeem_members)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        color_code = try values.decodeIfPresent(String.self, forKey: .color_code)
        progress_bar = try values.decodeIfPresent(String.self, forKey: .progress_bar)
	}

}
