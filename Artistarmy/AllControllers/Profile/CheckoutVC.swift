//
//  HomeVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit

class CheckoutVC: KBaseViewController {
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPhoneNumber: UITextField!
    @IBOutlet weak var tfCountry: UITextField!
    @IBOutlet weak var tfAddress1: UITextField!
    @IBOutlet weak var tfaddress2: UITextField!
    @IBOutlet weak var tfTown: UITextField!
    @IBOutlet weak var tfState: UITextField!
    @IBOutlet weak var btncheck: UIButton!
    @IBOutlet weak var tfPostcode: UITextField!
    var goalId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionBack(_ sender: Any) {
       pop()
    }
    
    @IBAction func actionCheckUncheck(_ sender: Any) {
        btncheck.isSelected = !btncheck.isSelected

    }
    @IBAction func actionRedeem(_ sender: Any) {
        let valid = validation()
        if valid.status {
            
            redeem_check_API()
        }else{
            presentAlert("", msgStr: valid.message, controller: self)
        }
    }
    
    func validation() -> (status:Bool, message:String) {
        var msg:String = ""
        var status:Bool = true
        if tfName.text == "" {
            status = false
            msg = Validation.kEnterName.rawValue
        }
        else if  tfEmail.text == ""{
            status = false
            msg = Validation.kEnterEmail.rawValue
            
        }
        /*else if self.isValidEmail(candidate: tfEmail.text!) {
            status = false
            msg = Validation.kEnterValidEmail.rawValue
        }*/
        else if   tfPhoneNumber.text == ""{
            status = false
            msg = Validation.kEnterMobileNumber.rawValue
        }
        else if  tfCountry.text == ""{
            status = false
            msg = Validation.kCountryName.rawValue
        }
        else if   tfAddress1.text == ""{
            status = false
            msg = Validation.kEnterAddress.rawValue
        }
        
        else if  tfTown.text == ""{
            status = false
            msg = Validation.kCityName.rawValue
        }
        else if  tfState.text == ""{
            status = false
            msg = Validation.kStateName.rawValue
        }
        else if  tfPostcode.text == ""{
            status = false
            msg = Validation.kPostalCode.rawValue
        }else if !btncheck.isSelected{
            status = false
            msg = Validation.kprivacy.rawValue
        }
            return (status:status, message:msg)
        
        }
    
    
}




extension CheckoutVC {
    func redeem_check_API(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kgoal_id] = goalId
        param[params.kfull_name] = tfName.text
        param[params.kemail] = tfEmail.text
        param[params.kphone_number] = tfPhoneNumber.text
        param[params.kcountry] = tfCountry.text
        param[params.kaddress1] = tfAddress1.text
        param[params.kaddress2] = tfaddress2.text
        param[params.kcity] = tfTown.text
        param[params.kstate] = tfState.text
        param[params.kpostcode] = tfPostcode.text
        
        ServerManager.shared.POST(url: ApiAction.redeem_check , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.pop()
                Artistarmy.show(message: obj.message!)
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
}
