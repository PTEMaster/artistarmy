//
//  HomeVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit

class AboutVC: KBaseViewController {

    @IBOutlet weak var lblArtistName: UILabel!
    @IBOutlet weak var lblRealEmail: UILabel!
    @IBOutlet weak var lblDOB: UILabel!
    @IBOutlet weak var lblGenre: UILabel!
    @IBOutlet weak var lblFormerBands: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblSpotify: UILabel!
    @IBOutlet weak var lblYoutube: UILabel!
    @IBOutlet weak var lblInstagram: UILabel!
    @IBOutlet weak var lblFacebook: UILabel!
    @IBOutlet weak var lblBiography: UILabel!
    var artistID = ""
    //Variable
    var dictUserinfo: Userinfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getProfile_API()
        // Do any additional setup after loading the view.
        self.tabBarController?.tabBar.isHidden = true
    }
    
    
    @IBAction func actionBack(_ sender: Any) {
       pop()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
      
    }
     
    
 
    func SetProfileData(){
      //  self?.dictUserinfo
        self.lblArtistName.text = dictUserinfo?.artist_name
        self.lblRealEmail.text = dictUserinfo?.email
        self.lblDOB.text = dictUserinfo?.date_of_birth
        self.lblGenre.text = dictUserinfo?.genre_name
        self.lblFormerBands.text = dictUserinfo?.former_bands
        self.lblLocation.text = dictUserinfo?.address
        self.lblSpotify.text = dictUserinfo?.spotify
        self.lblYoutube.text = dictUserinfo?.youtube
        self.lblInstagram.text = dictUserinfo?.instagram
        self.lblFacebook.text = dictUserinfo?.facebook
        self.lblBiography.text = dictUserinfo?.biography
    }
  
    
    
}

extension AboutVC {
    func getProfile_API(){
        var param = [String : Any]()
        param[params.kuser_id] = artistID
        ServerManager.shared.POST(url: ApiAction.getProfile , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(AboutModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.dictUserinfo = obj.userinfo
                self?.SetProfileData()
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
}

