//
//  HomeVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit

class UpdateProfileVC: KBaseViewController {
    @IBOutlet weak var tfArtistName: UITextField!
    @IBOutlet weak var tfRealEmail: UITextField!
    @IBOutlet weak var tfDOB: UITextField!
    @IBOutlet weak var lblGenre: UILabel!
    @IBOutlet weak var tfFormerBands: UITextField!
    @IBOutlet weak var tfLocation: UITextField!
    @IBOutlet weak var tfSpotify: UITextField!
    @IBOutlet weak var tfYoutube: UITextField!
    @IBOutlet weak var tfInstagram: UITextField!
    @IBOutlet weak var tfFacebook: UITextField!
    @IBOutlet weak var tvBiography: UITextView!
    var picker = UIDatePicker()
    //Variable
    var dictUserinfo: Userinfo?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker = UIDatePicker()
        tfDOB.inputView = picker
        picker.addTarget(self, action: #selector(self.handleDatePicker), for: UIControl.Event.valueChanged)
        picker.datePickerMode = .date
       
        // Do any additional setup after loading the view.
    }
    @objc func handleDatePicker() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yy-MM-dd"
        tfDOB.text = dateFormatter.string(from: picker.date)
        tfDOB.resignFirstResponder()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getProfile_API()
    }
    
    func doSomethingWithData(data: String) {
    
        }
    
    @IBAction func actionBack(_ sender: Any) {
       pop()
    }
    @IBAction func actionUpdate(_ sender: Any) {
        UpdateProfile_API()
    }
    @IBAction func actionGenre(_ sender: Any) {
        let vc = SelectGenreVC.instance(storyBoard: .Profile) as! SelectGenreVC
        vc.onDataAvailable = {[weak self]
                        (data) in
                        if let weakSelf = self {
                            weakSelf.doSomethingWithData(data: data)
                        }
                    }

        push(viewController: vc)
      }
    
       func SetProfileData(){
         //  self?.dictUserinfo
        tfArtistName.text = dictUserinfo?.artist_name
        tfRealEmail.text = dictUserinfo?.full_name
        tfDOB.text = dictUserinfo?.date_of_birth
        lblGenre.text = dictUserinfo?.genre_name
        tfFormerBands.text = dictUserinfo?.former_bands
        tfLocation.text = dictUserinfo?.address
        tfSpotify.text = dictUserinfo?.spotify
        tfYoutube.text = dictUserinfo?.youtube
        tfInstagram.text = dictUserinfo?.instagram
        tfFacebook.text = dictUserinfo?.facebook
        tvBiography.text = dictUserinfo?.biography
       }
}

extension UpdateProfileVC {
    func getProfile_API(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        ServerManager.shared.POST(url: ApiAction.getProfile , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(AboutModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.dictUserinfo = obj.userinfo
                self?.SetProfileData()
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    func UpdateProfile_API(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kartist_name] = tfArtistName.text
        param[params.kfull_name] = tfRealEmail.text
        param[params.kdate_of_birth] = tfDOB.text
        param[params.kbiography] = tvBiography.text
        param[params.kformer_bands] = tfFormerBands.text
        param[params.kaddress] = tfLocation.text
        param[params.kspotify] = tfSpotify.text
        param[params.kyoutube] = tfYoutube.text
        param[params.kinstagram] = tfInstagram.text
        param[params.kfacebook] = tfFacebook.text
                                                              
        ServerManager.shared.POST(url: ApiAction.update_profile , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(AboutModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.pop()
                presentAlert("", msgStr: obj.message, controller: self)
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
}

