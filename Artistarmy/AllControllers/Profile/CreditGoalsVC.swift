//
//  CreatePostVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit
import SDWebImage
import SDWebImage

class CreditGoalsVC: KBaseViewController {

    @IBOutlet weak var cvImg: UICollectionView!
    @IBOutlet var viewPopup: UIView!
    @IBOutlet var viewPopupIn: UIView!
    @IBOutlet weak var lblGoalName: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var autoGraph: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    
    var goalId = ""
    
    var arrgoals = [Goals]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
       
        cvImg.delegate = self
        cvImg.dataSource = self
   
        
      let margin: CGFloat = 10
        guard let collectionView = cvImg, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }

            flowLayout.minimumInteritemSpacing = margin
            flowLayout.minimumLineSpacing = 10
            flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
    }
    

   
    @IBAction func actionBack(_ sender: Any) {
        pop()
    }
    
    
    @IBAction func actionClosePopup(_ sender: Any) {
        self.viewHide(viewMain: viewPopup, viewPopUP: viewPopupIn)
    }
    
    @IBAction func actionRedeem(_ sender: Any) {
        self.viewHide(viewMain: viewPopup, viewPopUP: viewPopupIn)
            let seconds = 1.0
            DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
                let vc = CheckoutVC.instance(storyBoard: .Profile) as! CheckoutVC
                vc.goalId = self.goalId
                self.push(viewController: vc)
            }
      }
}



extension CreditGoalsVC: UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
  
    
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrgoals.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CreditGoalsCell", for: indexPath) as! CreditGoalsCell
        let dict = arrgoals[indexPath.row]
        cell.lblGoalName.text = dict.goal_name
        cell.lblcount.text = dict.amount
        cell.lblprogressPercentage.text = dict.progress_bar! + "%"
        
        let per = Float(dict.progress_bar!)!/100
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            UIView.animate(withDuration: 0.5, delay: 0, options: [.beginFromCurrentState, .allowUserInteraction], animations: { [unowned self] in
            cell.progress.setProgress( per, animated: true)
          })
        }
        //progress_bar
        if dict.goal_image != ""{
            cell.img.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.img.sd_setImage(with: URL(string: goalImagePath +  dict.goal_image! ), placeholderImage: UIImage(named: "user"))
            cell.lblcount.textColor = UIColor(red: 241/255, green: 67/255, blue: 54/255, alpha: 1.0)
            cell.viewbg.layer.borderColor = UIColor.gray.cgColor
            cell.viewbg.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        }else{
            cell.img.image = UIImage(named: "")
            if dict.color_code == "red"{
                let redColor = UIColor(red: 241/255, green: 67/255, blue: 54/255, alpha: 1.0)
                cell.lblcount.textColor = redColor
                cell.viewbg.layer.borderColor = redColor.cgColor
                cell.viewbg.backgroundColor = redColor.withAlphaComponent(0.5)
                cell.progress.trackTintColor = redColor
            }else if dict.color_code == "green"{
                let greenColor = UIColor(red: 52/255, green: 190/255, blue: 135/255, alpha: 1.0)
                cell.lblcount.textColor = greenColor
                cell.viewbg.layer.borderColor = greenColor.cgColor
                cell.viewbg.backgroundColor = greenColor.withAlphaComponent(0.5)
                cell.progress.trackTintColor = greenColor
            }else{
                let yellowColor = UIColor(red: 224/255, green: 155/255, blue: 60/255, alpha: 1.0)
                cell.lblcount.textColor = yellowColor
                cell.viewbg.layer.borderColor = yellowColor.cgColor
                cell.viewbg.backgroundColor = yellowColor.withAlphaComponent(0.5)
                cell.progress.trackTintColor = yellowColor
            }
           
          
        }
        
        
      
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = arrgoals[indexPath.row]
        lblGoalName.text = dict.goal_name
        lblAmount.text = dict.amount
        lblDate.text = dict.exp_date
        lblTime.text = dict.exp_time
        goalId = dict.id!
        viewPopup.frame = (self.view.window?.frame)!
        viewPopup.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        viewPopupIn.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.view.window?.addSubview(viewPopup)
        
        self.test(viewTest: viewPopupIn)
    }
     
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let noOfCellsInRow = 3   //number of column you want
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size, height: size + 30)
    }
    
    
   
    //------------
    func test(viewTest: UIView) {
            let orignalT: CGAffineTransform = viewTest.transform
            viewTest.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
            UIView.animate(withDuration: 0.4, animations: {
                
                viewTest.transform = orignalT
            }, completion:nil)
        }
    
        
        func viewHide(viewMain: UIView, viewPopUP: UIView) {
            let orignalT: CGAffineTransform = viewPopUP.transform
            UIView.animate(withDuration: 0.3, animations: {
                viewPopUP.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
            }, completion: {(sucess) in
                viewMain.removeFromSuperview()
                viewPopUP.transform = orignalT
            })
            navigationController?.navigationBar.isUserInteractionEnabled = true
            navigationController?.navigationBar.tintColor = UIColor.white
            
        }
    
 
    
    
    

}




class CreditGoalsCell: UICollectionViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblGoalName: UILabel!
    @IBOutlet weak var progress: UIProgressView!
    @IBOutlet weak var lblprogressPercentage: UILabel!
    @IBOutlet weak var lblcount: UILabel!
    @IBOutlet weak var viewbg: UIView!
  
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

  
}

extension UIColor {
    convenience init(hexFromString:String, alpha:CGFloat = 1.0) {
        var cString:String = hexFromString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        var rgbValue:UInt64 = 10066329 //color #999999 if string has wrong format

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) == 6) {
            Scanner(string: cString).scanHexInt64(&rgbValue)
        }

        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
}
