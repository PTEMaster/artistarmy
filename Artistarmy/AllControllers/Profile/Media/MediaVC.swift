//
//  CreatePostVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit
import SDWebImage
class MediaVC: KBaseViewController {
    
    @IBOutlet weak var cvImg: UICollectionView!
    var artistID = ""
    var arrMedia = [Media]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        cvImg.delegate = self
        cvImg.dataSource = self
   
        
      let margin: CGFloat = 10
        guard let collectionView = cvImg, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }

            flowLayout.minimumInteritemSpacing = margin
            flowLayout.minimumLineSpacing = 10
            flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
        self.tabBarController?.tabBar.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        get_mediat_API()
    }
    

   
    @IBAction func actionBack(_ sender: Any) {
        pop()
    }
   
    
    
}



extension MediaVC: UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
  
    
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMedia.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MediaVCCell", for: indexPath) as! MediaVCCell
        let obj = arrMedia[indexPath.row]
        cell.img.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.img.sd_setImage(with: URL(string:  obj.image! ), placeholderImage: UIImage(named: "user"))
      
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
      
    }
     
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let noOfCellsInRow = 3   //number of column you want
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size, height: size)
    }
}




class MediaVCCell: UICollectionViewCell {
    @IBOutlet weak var img: UIImageView!
  
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

  
}

extension MediaVC {
    func get_mediat_API(){
        var param = [String : Any]()
        param[params.kuser_id] = artistID
        ServerManager.shared.POST(url: ApiAction.get_media , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(MediaModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrMedia = obj.media!
                self?.cvImg.reloadData()
                
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
}
