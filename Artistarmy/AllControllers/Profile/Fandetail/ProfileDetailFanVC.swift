//
//  HomeVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit
import SDWebImage

class ProfileDetailFanVC: KBaseViewController {

    @IBOutlet weak var cvTopFans: UICollectionView!
    @IBOutlet weak var cvConcert: UICollectionView!
    @IBOutlet weak var tableViewFeed: UITableView!
    @IBOutlet weak var tableSong: UITableView!
    
    @IBOutlet var viewPopup: UIView!
    @IBOutlet var viewPopupIn: UIView!
    var id = ""
    
    var arrfavourite_artist = [Favourite_artist]()
    var arrbookinginfo = [Bookinginfo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
        favourite_artistlist_API()
        concertbooked_byusert_API()
        }
    
    @IBAction func actionBack(_ sender: Any) {
       pop()
    }
    
    //---
    @IBAction func actionFavArtist(_ sender: Any) {
        let vc = FavouriteArtistVC.instance(storyBoard: .Profile) as! FavouriteArtistVC
        vc.arrfavourite_artist = arrfavourite_artist
        push(viewController: vc)
    }
    
    @IBAction func actionConcerts(_ sender: Any) {
        let vc = ConcertBookNowVC.instance(storyBoard: .Profile) as! ConcertBookNowVC
        vc.isComeFromFan = true
        vc.fanID = id
        push(viewController: vc)
    }
    
    //----
    
    
    @IBAction func actionClosePopup(_ sender: Any) {
       
    }
    

    
    
}

extension ProfileDetailFanVC: UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
  
    
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         if cvTopFans == collectionView{
            return arrfavourite_artist.count
        }else{
            return arrbookinginfo.count
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if cvTopFans == collectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileDetailCell", for: indexPath) as! ProfileDetailCell
            let dict  = arrfavourite_artist[indexPath.row]
            
           cell.lblFanName.text = dict.full_name
            cell.lblcount.text = dict.total_like
            cell.img.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.img.sd_setImage(with: URL(string: ProfileImagePath +  (dict.profile_image)! ), placeholderImage: UIImage(named: "user"))
           
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ConcertDetailCell", for: indexPath) as! ConcertDetailCell
            let dict  = arrbookinginfo[indexPath.row]
            cell.lblName.text = dict.concert_title
            cell.img.sd_imageIndicator = SDWebImageActivityIndicator.gray
          
            cell.img.sd_setImage(with: URL(string: ProfileImagePath +  dict.concert_image! ), placeholderImage: UIImage(named: "user"))
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
       
    }
}





extension ProfileDetailFanVC {
    func favourite_artistlist_API(){
        var param = [String : Any]()
        param[params.kuser_id] = id
        ServerManager.shared.POST(url: ApiAction.favourite_artistlist , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(FanFavouriteArtistModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrfavourite_artist = obj.favourite_artist  ?? []
                self?.cvTopFans.reloadData()
            } else {
                print("failure")
                Artistarmy.show(message: obj.message ?? "")
            }
        }
    }
    func concertbooked_byusert_API(){
        var param = [String : Any]()
        param[params.kuser_id] = id
        ServerManager.shared.POST(url: ApiAction.concertbooked_byuser , param: param, false,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(FanConcertModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrbookinginfo = obj.bookinginfo ?? []
                self?.cvConcert.reloadData()
            } else {
                print("failure")
               // Artistarmy.show(message: obj.message ?? "")
            }
        }
    }
}
