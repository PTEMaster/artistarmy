//
//  HomeVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit

class FollowersVC : KBaseViewController {
    @IBOutlet weak var tblNotification: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    var comefromFriend = false
    var artistID = ""
    var fanID = ""
    var arrFollower =  [Followers]()
    var arrfriends = [Friends]()
    var page:Int = 0
    var isDownloading = false
    var pointContentOffset = CGPoint.zero
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        self.tabBarController?.tabBar.isHidden = true

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        if  comefromFriend{
            lblTitle.text = "Friends"
            get_allfriendAPI()
        }else{
            get_allfollowers_API()
        }
    }
    
}
extension FollowersVC: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  comefromFriend{
           return arrfriends.count
        }else{
            return  arrFollower.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FollowersCell", for: indexPath) as! FollowersCell
        if  comefromFriend{
            let obj = arrfriends[indexPath.row]
            cell.lblname.text = obj.full_name
            cell.lbltype.text = obj.biography
            cell.btnFollow.tag = indexPath.row
            cell.btnFollow.addTarget(self, action: #selector(userDetail(_:)), for: .touchUpInside)
            cell.imgUser.sd_setImage(with: URL(string: ProfileImagePath +  obj.profile_image! ), placeholderImage: UIImage(named: "user"))
            if   obj.is_friend == "0"{
                cell.btnFollow.setTitle("Add Friend", for: .normal)
            }else{
                cell.btnFollow.setTitle("Unfriend", for: .normal)
            }
            
            if indexPath.row == (arrfriends.count - 1) && (tblNotification.contentOffset.y > pointContentOffset.y) {
                if !isDownloading {
                    isDownloading = true
                    if arrfriends.count % appDelegate.pageCount == 0 {
                        page += appDelegate.pageCount
                        self.get_allfriendAPI()
                    }
                }
            }
        }else{
            let obj = arrFollower[indexPath.row]
            cell.lblname.text = obj.full_name
            cell.lbltype.text = obj.biography
            cell.btnFollow.tag = indexPath.row
            cell.btnFollow.addTarget(self, action: #selector(userDetail(_:)), for: .touchUpInside)
            cell.imgUser.sd_setImage(with: URL(string: ProfileImagePath +  obj.profile_image! ), placeholderImage: UIImage(named: "user"))
            if   obj.is_friend == "0"{
                cell.btnFollow.setTitle("Follow", for: .normal)
            }else{
                cell.btnFollow.setTitle("Following", for: .normal)
            }
            
            
            if indexPath.row == (arrFollower.count - 1) && (tblNotification.contentOffset.y > pointContentOffset.y) {
                if !isDownloading {
                    isDownloading = true
                    if arrFollower.count % appDelegate.pageCount == 0 {
                        page += appDelegate.pageCount
                        self.get_allfollowers_API()
                    }
                }
            }
            
        }
        return cell
    }
    
    
   @objc func userDetail(_ sender: UIButton) {
    if comefromFriend{
        let dict = self.arrfriends[sender.tag]
        add_friend_API(friend_id: dict.id!, index: sender.tag)
    }else{
        let dict = self.arrFollower[sender.tag]
    add_followers_API(friend_id: dict.id!, index: sender.tag)
    }
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if comefromFriend{
            let dict = self.arrfriends[indexPath.row]
            let vc = ProfileFanVC.instance(storyBoard: .Profile) as! ProfileFanVC
            vc.fanid = dict.friends_id!
            vc.isSeenFanProfile = true
            push(viewController: vc)
        }else{
            let dict = self.arrFollower[indexPath.row]
            let vc = ProfileFanVC.instance(storyBoard: .Profile) as! ProfileFanVC
            vc.fanid = dict.fans_id!
            vc.isSeenFanProfile = true
            push(viewController: vc)
        }
        
   }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @IBAction func actionBack(_ sender: Any) {
        pop()
    }
}



class FollowersCell: UITableViewCell {
    @IBOutlet weak var lbltype: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblname: UILabel!
    
    @IBOutlet weak var btnFollow: UIButton!
    @IBAction func actionFollowing(_ sender: Any) {
        
    }
    @IBAction func actionOption(_ sender: Any) {
       
    }
    
    
}
extension FollowersVC {
    func get_allfollowers_API(){
        var param = [String : Any]()
        param[params.kfans_id] = fanID
       param[params.kartist_id] = artistID
        param[params.kstart] =  page
        param[params.kpageCount] =   appDelegate.pageCount
        ServerManager.shared.POST(url: ApiAction.get_allfollowers , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(FollowerModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrFollower.append(contentsOf: obj.followers!)
                self?.tblNotification.reloadData()
                if obj.followers!.count == 0{
                    self?.isDownloading = true
                }else{
                    self?.isDownloading = false
                }
            } else {
                self?.isDownloading = true
            }
        }
    }
    
    func add_followers_API(friend_id : String , index : Int){
        var param = [String : Any]()
        param[params.kartist_id] = AppDataHelper.shard.logins.id
       param[params.kfans_id] = friend_id
        
        ServerManager.shared.POST(url: ApiAction.add_followers , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                let ind = IndexPath(row: index, section: 0)
                let cell = self!.tblNotification.cellForRow(at: ind) as!  FollowersCell
                
                if  self?.arrFollower[index].is_friend == "0" {
                    cell.btnFollow.setTitle("Following", for: .normal)
                    self?.arrFollower[index].is_friend = "1"
                }else{
                    cell.btnFollow.setTitle("Follow", for: .normal)
                    self?.arrFollower[index].is_friend = "0"
                }
                
                
              //  self?.tblNotification.reloadData()
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    //---------
    func add_friend_API(friend_id : String , index : Int){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
       param[params.kfriends_id] = friend_id
        ServerManager.shared.POST(url: ApiAction.add_friends , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                let ind = IndexPath(row: index, section: 0)
                let cell = self!.tblNotification.cellForRow(at: ind) as!  FollowersCell
                
                if  self?.arrfriends[index].is_friend == "0" {
                    cell.btnFollow.setTitle("Friends", for: .normal)
                    self?.arrfriends[index].is_friend = "1"
                }else{
                    cell.btnFollow.setTitle("Add Friend", for: .normal)
                    self?.arrfriends[index].is_friend = "0"
                }
              //  self?.tblNotification.reloadData()
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
//---------
    
    func get_allfriendAPI(){
        var param = [String : Any]()
        param[params.kfans_id] = fanID
       param[params.kuser_id] = artistID
        param[params.kstart] =  page
        param[params.kpageCount] =   appDelegate.pageCount
        ServerManager.shared.POST(url: ApiAction.get_allfriends , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(FriendModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrfriends.append(contentsOf: obj.friends!)
                self?.tblNotification.reloadData()
                if obj.friends!.count == 0{
                    self?.isDownloading = true
                }else{
                    self?.isDownloading = false
                }
            } else {
                print("failure")
                self?.isDownloading = true
            }
        }
    }
    
    
}


