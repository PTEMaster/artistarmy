/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Followers : Codable {
    let id : String?
    let artist_id : String?
    let fans_id : String?
    let created_at : String?
    let country_code : String?
    let mobile_number : String?
    let password : String?
    let email : String?
    let full_name : String?
    let artist_name : String?
    let date_of_birth : String?
    let genre_cat : String?
    let biography : String?
    let former_bands : String?
    let profile_image : String?
    let cover_image : String?
    let address : String?
    let state : String?
    let city : String?
    let pincode : String?
    let user_longitude : String?
    let user_latitude : String?
    let user_type : String?
    let device_type : String?
    let ios_token : String?
    let android_token : String?
    let reset_code : String?
    let social : String?
    let spotify : String?
    let youtube : String?
    let instagram : String?
    let facebook : String?
    let otp : String?
    let token : String?
    let status : String?
    let notification_status : String?
    let description : String?
    let song : String?
    var is_friend : String?
    var isSelected = false
    
    
    
    enum CodingKeys: String, CodingKey {

        case id = "id"
        case artist_id = "artist_id"
        case fans_id = "fans_id"
        case created_at = "created_at"
        case country_code = "country_code"
        case mobile_number = "mobile_number"
        case password = "password"
        case email = "email"
        case full_name = "full_name"
        case artist_name = "artist_name"
        case date_of_birth = "date_of_birth"
        case genre_cat = "genre_cat"
        case biography = "biography"
        case former_bands = "former_bands"
        case profile_image = "profile_image"
        case cover_image = "cover_image"
        case address = "address"
        case state = "state"
        case city = "city"
        case pincode = "pincode"
        case user_longitude = "user_longitude"
        case user_latitude = "user_latitude"
        case user_type = "user_type"
        case device_type = "device_type"
        case ios_token = "ios_token"
        case android_token = "android_token"
        case reset_code = "reset_code"
        case social = "social"
        case spotify = "spotify"
        case youtube = "youtube"
        case instagram = "instagram"
        case facebook = "facebook"
        case otp = "otp"
        case token = "token"
        case status = "status"
        case notification_status = "notification_status"
        case description = "description"
        case song = "song"
        case is_friend = "is_friend"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        artist_id = try values.decodeIfPresent(String.self, forKey: .artist_id)
        fans_id = try values.decodeIfPresent(String.self, forKey: .fans_id)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        country_code = try values.decodeIfPresent(String.self, forKey: .country_code)
        mobile_number = try values.decodeIfPresent(String.self, forKey: .mobile_number)
        password = try values.decodeIfPresent(String.self, forKey: .password)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        full_name = try values.decodeIfPresent(String.self, forKey: .full_name)
        artist_name = try values.decodeIfPresent(String.self, forKey: .artist_name)
        date_of_birth = try values.decodeIfPresent(String.self, forKey: .date_of_birth)
        genre_cat = try values.decodeIfPresent(String.self, forKey: .genre_cat)
        biography = try values.decodeIfPresent(String.self, forKey: .biography)
        former_bands = try values.decodeIfPresent(String.self, forKey: .former_bands)
        profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
        cover_image = try values.decodeIfPresent(String.self, forKey: .cover_image)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        state = try values.decodeIfPresent(String.self, forKey: .state)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        pincode = try values.decodeIfPresent(String.self, forKey: .pincode)
        user_longitude = try values.decodeIfPresent(String.self, forKey: .user_longitude)
        user_latitude = try values.decodeIfPresent(String.self, forKey: .user_latitude)
        user_type = try values.decodeIfPresent(String.self, forKey: .user_type)
        device_type = try values.decodeIfPresent(String.self, forKey: .device_type)
        ios_token = try values.decodeIfPresent(String.self, forKey: .ios_token)
        android_token = try values.decodeIfPresent(String.self, forKey: .android_token)
        reset_code = try values.decodeIfPresent(String.self, forKey: .reset_code)
        social = try values.decodeIfPresent(String.self, forKey: .social)
        spotify = try values.decodeIfPresent(String.self, forKey: .spotify)
        youtube = try values.decodeIfPresent(String.self, forKey: .youtube)
        instagram = try values.decodeIfPresent(String.self, forKey: .instagram)
        facebook = try values.decodeIfPresent(String.self, forKey: .facebook)
        otp = try values.decodeIfPresent(String.self, forKey: .otp)
        token = try values.decodeIfPresent(String.self, forKey: .token)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        notification_status = try values.decodeIfPresent(String.self, forKey: .notification_status)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        song = try values.decodeIfPresent(String.self, forKey: .song)
        is_friend = try values.decodeIfPresent(String.self, forKey: .is_friend)
    }

}
