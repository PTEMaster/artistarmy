/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Concerts : Codable {
	let concert_id : String?
	let user_id : String?
	let concert_title : String?
	let concert_time : String?
	let concert_date : String?
	let concert_venue : String?
	let description : String?
	let created_at : String?
    let concert_image : String?
    let is_booked  : String?

	enum CodingKeys: String, CodingKey {

		case concert_id = "concert_id"
		case user_id = "user_id"
		case concert_title = "concert_title"
		case concert_time = "concert_time"
		case concert_date = "concert_date"
		case concert_venue = "concert_venue"
		case description = "description"
		case created_at = "created_at"
        case  concert_image = "concert_image"
        case is_booked = "is_booked"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		concert_id = try values.decodeIfPresent(String.self, forKey: .concert_id)
		user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
		concert_title = try values.decodeIfPresent(String.self, forKey: .concert_title)
		concert_time = try values.decodeIfPresent(String.self, forKey: .concert_time)
		concert_date = try values.decodeIfPresent(String.self, forKey: .concert_date)
		concert_venue = try values.decodeIfPresent(String.self, forKey: .concert_venue)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        concert_image = try values.decodeIfPresent(String.self, forKey: .concert_image)
        is_booked = try values.decodeIfPresent(String.self, forKey: .is_booked)
	}

}
