//
//  HomeVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit
import SDWebImage

class ConcertBookNowVC: KBaseViewController {
    @IBOutlet weak var tblConcertList: UITableView!
    var artistID = ""
    var fanID = ""
    var isComeFromFan = false
    var arrbookinginfo = [Bookinginfo]()
    
   lazy var arrConcerts =  [Concerts]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        if  isComeFromFan{
           concertbooked_byusert_API()
            //arrbookinginfo
        }else{
            all_concert_API()
            // arrConcerts
        }
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
      
    }
    @IBAction func actionBack(_ sender: Any) {
       pop()
    }
    
}
extension ConcertBookNowVC: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  isComeFromFan{
           return arrbookinginfo.count
        }else{
            return arrConcerts.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConcertBookCell", for: indexPath) as! ConcertBookCell
        if  isComeFromFan{
            let obj  = arrbookinginfo[indexPath.row]
            cell.lblName.text = obj.concert_title
            cell.lblDescription.text = obj.description
            cell.lblDate.text = obj.concert_date
            cell.lblTime.text = obj.concert_time
            cell.lblAddress.text = obj.concert_venue
            cell.btnDelete.layer.cornerRadius = 10
            cell.btnDelete.layer.maskedCorners = [ .layerMinXMaxYCorner , .layerMaxXMaxYCorner]
            cell.btnDelete.tag = indexPath.row
            
            cell.imgUser.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imgUser.sd_setImage(with: URL(string: ProfileImagePath +  (obj.concert_image)! ), placeholderImage: UIImage(named: "user"))
        
            if obj.is_booked == "1"{
                cell.btnDelete.backgroundColor = .red
                cell.btnDelete.setTitle("Cancel Booking", for: .normal)
                cell.btnDelete.addTarget(self, action: #selector(cancelBooking(_:)), for: .touchUpInside)
            }else  if obj.is_booked == "0"{
                cell.btnDelete.backgroundColor = green
                cell.btnDelete.setTitle("Book Now", for: .normal)
                cell.btnDelete.addTarget(self, action: #selector(bookNow(_:)), for: .touchUpInside)
            }else{
                cell.btnDelete.backgroundColor = .red
                cell.btnDelete.setTitle("Cancel Booking", for: .normal)
                cell.btnDelete.addTarget(self, action: #selector(cancelBooking(_:)), for: .touchUpInside)
            }
        }else{
        let obj  = arrConcerts[indexPath.row]
        cell.lblName.text = obj.concert_title
        cell.lblDescription.text = obj.description
        cell.lblDate.text = obj.concert_date
        cell.lblTime.text = obj.concert_time
        cell.lblAddress.text = obj.concert_venue
        cell.btnDelete.layer.cornerRadius = 10
        cell.btnDelete.layer.maskedCorners = [ .layerMinXMaxYCorner , .layerMaxXMaxYCorner]
        cell.btnDelete.tag = indexPath.row
        cell.imgUser.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgUser.sd_setImage(with: URL(string: ConcertImagePath +  obj.concert_image! ), placeholderImage: UIImage(named: "user"))
            if obj.is_booked == "1"{
                cell.btnDelete.backgroundColor = .red
                cell.btnDelete.setTitle("Cancel Booking", for: .normal)
                cell.btnDelete.addTarget(self, action: #selector(cancelBooking(_:)), for: .touchUpInside)
            }else if obj.is_booked == "0"{
                cell.btnDelete.backgroundColor = green
                cell.btnDelete.setTitle("Book Now", for: .normal)
                cell.btnDelete.addTarget(self, action: #selector(bookNow(_:)), for: .touchUpInside)
                
            }else{
                cell.btnDelete.backgroundColor = .red
                cell.btnDelete.setTitle("Cancel Concert", for: .normal)
                cell.btnDelete.addTarget(self, action: #selector(cancelBooking(_:)), for: .touchUpInside)
            }
    }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   
   }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func cancelBooking(_ sender: UIButton) {
        if  isComeFromFan{
            let dict = self.arrbookinginfo[sender.tag]
           cancel_concert_API(dict.concert_id ?? "")
            
        }else{
            let dict = self.arrConcerts[sender.tag]
            delete_concert_API(dict.concert_id ?? "")
        }
     }
    
    @objc func bookNow(_ sender: UIButton) {
        
        if  isComeFromFan{
            let dict = self.arrbookinginfo[sender.tag]
            concert_booking_API(dict.concert_id ?? "")
            
        }else{
            let dict = self.arrConcerts[sender.tag]
            concert_booking_API(dict.concert_id ?? "")
        }
    
     }
    
}


extension ConcertBookNowVC {
    func all_concert_API(){
        var param = [String : Any]()
        param[params.kartist_id] = artistID
        param[params.kfans_id] = fanID
        
        ServerManager.shared.POST(url: ApiAction.all_concert , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(AllConcertModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrConcerts = obj.concerts!
                self?.tblConcertList.reloadData()
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    func concertbooked_byusert_API(){
        var param = [String : Any]()
        param[params.kuser_id] = fanID
        ServerManager.shared.POST(url: ApiAction.concertbooked_byuser , param: param, false,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(FanConcertModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrbookinginfo = obj.bookinginfo ?? []
                self?.tblConcertList.reloadData()
            } else {
                print("failure")
                Artistarmy.show(message: obj.message ?? "")
            }
        }
    }
    func cancel_concert_API(_ concertId : String){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kconcert_id] = concertId
        ServerManager.shared.POST(url: ApiAction.cancel_concert_booking , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                Artistarmy.show(message: obj.message ?? "")
                if  self!.isComeFromFan{
                    self?.concertbooked_byusert_API()
                }else{
                    self?.all_concert_API()
                }
            } else {
                print("failure")
                Artistarmy.show(message: obj.message ?? "")
            }
        }
    }
    func concert_booking_API(_ concertId : String){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kconcert_id] = concertId
        ServerManager.shared.POST(url: ApiAction.concert_booking , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                Artistarmy.show(message: obj.message ?? "")
                if  self!.isComeFromFan{
                    self?.concertbooked_byusert_API()
                }else{
                    self?.all_concert_API()
                }
            } else {
                print("failure")
                Artistarmy.show(message: obj.message ?? "")
            }
        }
    }
    
    func delete_concert_API(_ concertId : String){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kconcert_id] = concertId
        ServerManager.shared.POST(url: ApiAction.delete_concerts , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.all_concert_API()
            } else {
                print("failure")
                Artistarmy.show(message: obj.message ?? "")
            }
        }
    }
   
    
}












class ConcertBookCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    
    
   
    @IBAction func actionDelete(_ sender: Any) {
    }
    
}
