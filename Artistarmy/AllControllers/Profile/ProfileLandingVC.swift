//
//  HomeVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit

class ProfileLandingVC: KBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
      
       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        if AppDataHelper.shard.isFan{
            let vc = ProfileFanVC.instance(storyBoard: .Profile) as! ProfileFanVC
            push(viewController: vc)
        }else{
            let vc = ProfileVC.instance(storyBoard: .Profile) as! ProfileVC
            push(viewController: vc)
        }
    }
    
    
    
    
}
