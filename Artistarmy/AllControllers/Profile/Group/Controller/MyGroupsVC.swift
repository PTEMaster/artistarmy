//
//  CreatePostVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit
import SDWebImage


class MyGroupsVC: KBaseViewController {

    @IBOutlet weak var cvJoinedGroup: UICollectionView!
    @IBOutlet weak var cvSuggestGroup: UICollectionView!
    var id = ""
    var arrGroup_list = [Group_list]()
    var arrGroup_suggestion = [Group_suggestion]()
    
    var groupId = ""
    override func viewDidLoad() {
        super.viewDidLoad()
      let margin: CGFloat = 10
        guard let collectionView = cvJoinedGroup, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
            flowLayout.minimumInteritemSpacing = margin
            flowLayout.minimumLineSpacing = 10
            flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
        self.tabBarController?.tabBar.isHidden = true
       
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        grouplist_byid_API()
    }
    

   
    @IBAction func actionBack(_ sender: Any) {
        pop()
    }
  
    
}



extension MyGroupsVC: UICollectionViewDataSource , UICollectionViewDelegateFlowLayout, joinedGroupDelegate, GroupDelegate{
    func onButtonClick(button str: GroupList) {
        if str == .groupLeave{
            leave_group_API()
        }else{
            
        }
        
    }
    
    func popoverView(Cell: cvJoinedGroupCell, sender: UIButton) {
        guard let indexPath = cvJoinedGroup.indexPath(for: Cell) else {
            return
        }
        self.groupId = arrGroup_list[indexPath.row].id!
        if AppDataHelper.shard.logins.id == arrGroup_list[indexPath.row].user_id{
        let alertController = storyboard?.instantiateViewController(withIdentifier: "GroupPopupVC") as! GroupPopupVC
            alertController.modalPresentationStyle = .popover
            let popover = alertController.popoverPresentationController
            popover?.delegate = self
            alertController.delegate = self
            popover?.sourceView = sender
            popover?.permittedArrowDirections =  .up
            alertController.preferredContentSize = CGSize(width: 90, height: 60)
            self.present(alertController, animated: true, completion: nil)
    }
    }
    
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == cvJoinedGroup{
           return arrGroup_list.count
        }else{
            return arrGroup_suggestion.count
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if cvJoinedGroup == collectionView{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cvJoinedGroupCell", for: indexPath) as! cvJoinedGroupCell
            cell.delegate = self
            let obj = arrGroup_list[indexPath.row]
            cell.lblGroupName.text = obj.tittle
            cell.lblcount.text = obj.members! + "People"
        return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cvSuggestGroupCell", for: indexPath) as! cvSuggestGroupCell
            let obj = arrGroup_suggestion[indexPath.row]
            cell.lblGroupName.text = obj.tittle
            cell.lblcount.text = obj.members! + "People"
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
       
    }
     
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if cvJoinedGroup == collectionView{
            let noOfCellsInRow = 2   //number of column you want
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

            let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
            return CGSize(width: size, height: size + 30)
        }else{
            return CGSize(width: 150, height: 200)
        }
       
    }
    
    
   
    
    

}

extension MyGroupsVC {
    func grouplist_byid_API(){
        var param = [String : Any]()
        param[params.kuser_id] = id
        ServerManager.shared.POST(url: ApiAction.kgrouplist_byid , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(GroupModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrGroup_list = obj.group_list!
                self?.arrGroup_suggestion  = obj.group_suggestion!
                
                self?.cvJoinedGroup.reloadData()
                self?.cvSuggestGroup.reloadData()
                
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    func leave_group_API(){
        var param = [String : Any]()
        param[params.kuser_id] = id
        param[params.kgroup_id] = groupId
        
        ServerManager.shared.POST(url: ApiAction.leave_group , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(GroupModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.grouplist_byid_API()
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }

}


extension MyGroupsVC: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}






protocol joinedGroupDelegate:NSObject {
    func popoverView(Cell: cvJoinedGroupCell , sender:UIButton)
}
class cvJoinedGroupCell: UICollectionViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblGroupName: UILabel!
    @IBOutlet weak var lblcount: UILabel!
    weak var delegate:joinedGroupDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func actionPopoverview(_ sender: UIButton) {
        print("MoreOptionBtnAction")
        delegate?.popoverView(Cell: self, sender: sender)
    }
}

class cvSuggestGroupCell: UICollectionViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblGroupName: UILabel!
    @IBOutlet weak var lblcount: UILabel!
    @IBOutlet weak var btnJoinNow: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
   
  
}











