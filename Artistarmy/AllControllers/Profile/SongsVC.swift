//
//  HomeVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit

class SongsVC: KBaseViewController {

    
    @IBOutlet weak var btnSong1: UIButton!
    @IBOutlet weak var btnSong2: UIButton!
    @IBOutlet weak var btnSong3: UIButton!
    @IBOutlet weak var stk1: NSLayoutConstraint!
    @IBOutlet weak var stk2: NSLayoutConstraint!
    @IBOutlet weak var stk3: NSLayoutConstraint!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSong1.isSelected =  false
        btnSong2.isSelected =  true
        btnSong3.isSelected =  true
        stk1.constant = 400
        stk2.constant = 0
        stk3.constant = 0
        
        view1.isHidden = false
        view2.isHidden = true
        view3.isHidden = true
        
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionBack(_ sender: Any) {
       pop()
    }
    @IBAction func actionSong1(_ sender: Any) {
        if btnSong1.isSelected {
            btnSong1.isSelected = false
            stk1.constant = 400
            view1.isHidden = false
        }else{
            btnSong1.isSelected = true
            stk1.constant = 0
            view1.isHidden = true
            }
    }
    
    @IBAction func actionSong2(_ sender: Any) {
        if btnSong2.isSelected {
            btnSong2.isSelected = false
            stk2.constant = 400
            view2.isHidden = false
        }else{
            btnSong2.isSelected = true
            stk2.constant = 0
            view2.isHidden = true
            }
    }
    
    @IBAction func actionSong3(_ sender: Any) {
        if btnSong3.isSelected {
            btnSong3.isSelected = false
            stk3.constant = 400
            view3.isHidden = false
        }else{
            btnSong3.isSelected = true
            stk3.constant = 0
            view3.isHidden = true
            }
    }
    
}
