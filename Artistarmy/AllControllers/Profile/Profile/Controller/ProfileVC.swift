//
//  HomeVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit
import SDWebImage
import AVFoundation
import Firebase
import FirebaseDynamicLinks

class ProfileVC: KBaseViewController {
    enum ImgType: String {
        case profileImg
        case BgImg
    }
    
    @IBOutlet weak var cvImg: UICollectionView!
    var userID = ""
    var isComeFromFan = false
    var dictUserinfo: UserinfoReels?
    @IBOutlet weak var viewShare: UIView!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnBgCamera: UIButton!
    @IBOutlet weak var btnFollow: UIButton!
    
    @IBOutlet weak var viewFollow: UIView!
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var viewDonateBits: UIView!
    @IBOutlet weak var viewBitStore: UIView!
    @IBOutlet weak var viewEditProfile: UIView!
    
    @IBOutlet weak var lblReedem: UILabel!
    @IBOutlet weak var lblFollowers: UILabel!
    @IBOutlet weak var lblCredit: UILabel!
    @IBOutlet weak var lblPost: UILabel!
    
    @IBOutlet weak var viewSong1: UIView!
    @IBOutlet weak var viewSong2: UIView!
    @IBOutlet weak var viewSong3: UIView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMusicType: UILabel!
    @IBOutlet weak var lmgProfile: UIImageView!
    @IBOutlet weak var lmgBgProfile: UIImageView!
    @IBOutlet weak var lblTitleName: UILabel!
    @IBOutlet weak var lmgCheck: UIImageView!
    var imagepickerDocument:TDImagePicker!
    var imgType : ImgType?
    //
    @IBOutlet weak var cvSong: UICollectionView!
    @IBOutlet weak var cvTopFans: UICollectionView!
    @IBOutlet weak var cvCreditGoal: UICollectionView!
    @IBOutlet weak var cvConcert: UICollectionView!
    @IBOutlet weak var tableViewFeed: UITableView!
    @IBOutlet weak var tableSong: UITableView!
    @IBOutlet weak var constriantTblHeight: NSLayoutConstraint!
    
    
    
    @IBOutlet var viewPopup: UIView!
    @IBOutlet var viewPopupIn: UIView!
    
    var arrArtist_songs = [Artist_songs]()
    var arrfans = [Fans]()
    var arrgoals = [Goals]()
    var arrconcerts = [Concerts]()
    var  arrPostList =  [Post_list]()
    var artistID = ""
    
    //
    let dispatchGroup = DispatchGroup()
    let audioPlayerManager = AudioPlayerManager()
    
        
    override func viewDidLoad() {
        super.viewDidLoad()
       
        imagepickerDocument = TDImagePicker(presentationController: self, delegate: self)
        self.tableViewFeed.register(UINib(nibName: "FeedViewMultipleCell", bundle: nil), forCellReuseIdentifier: "FeedViewMultipleCell")
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("isComeFromFan=-=-=-=-=-=-=-==-=-=\(isComeFromFan)")
     self.tabBarController?.tabBar.isHidden = false
       
        if isComeFromFan{
            btnCamera.isHidden = true
            btnBgCamera.isHidden = true
            viewBitStore.isHidden = true
            viewEditProfile.isHidden = true
            if AppDataHelper.shard.logins.id == userID{
                viewFollow.isHidden = true
            }
        }else{
            viewShare.isHidden = true
            viewFollow.isHidden = true
            viewMessage.isHidden = true
            viewDonateBits.isHidden = true
        }
        
        if  isComeFromFan{
            artistID = userID //dictUserinfo?.id ?? ""
        }else{
            artistID = AppDataHelper.shard.logins.id!
        }
        Loader.showLoader()
        getProfile_API()
        self.SongList_API()
        self.Get_allfansList_API()
        self.Artistgoal_list_API()
        self.All_concert_API()
        self.Newsfeed_list_API()
        dispatchGroup.notify(queue: .main) {
            
            Loader.hideLoader()
           }
        }
    
   
    
    @IBAction func actionPost(_ sender: Any) {
        if isComeFromFan{
            pop()
        }else{
            self.tabBarController?.selectedIndex = 0
        }
       
      //
       
    }
    
    
    
    @IBAction func actionUpdateProfile(_ sender: UIButton) {
    let alertController = storyboard?.instantiateViewController(withIdentifier: "MenuPopupVC") as! MenuPopupVC
        alertController.modalPresentationStyle = .popover
        let popover = alertController.popoverPresentationController
        popover?.delegate = self
        alertController.delegate = self
        popover?.sourceView = sender
        popover?.permittedArrowDirections =  .up
        alertController.preferredContentSize = CGSize(width: 130, height: 120)
        self.present(alertController, animated: true, completion: nil)
        
    }
    //---=-=-=-=-=-===-=-=-==-
    
    @IBAction func actionFollow(_ sender: Any) {

        AddFollow_API()
      }

    
    @IBAction func actionMessage(_ sender: Any) {
//          let vc = SettingVC.instance(storyBoard: .Profile) as! SettingVC
//          push(viewController: vc)
        Artistarmy.show(message: "Work in progress")
      }

    @IBAction func actionDonateBit(_ sender: Any) {
        let vc = BitStoreVC.instance(storyBoard: .Profile) as! BitStoreVC
        vc.userId = userID
        vc.bitToName  = self.dictUserinfo!.full_name!
        push(viewController: vc)
      }
    
    @IBAction func actionBitStore(_ sender: Any) {
          let vc = BitStoreVC.instance(storyBoard: .Profile) as! BitStoreVC
        vc.isComeFromBitStore = true
          push(viewController: vc)
      }

    @IBAction func actionEditProfile(_ sender: Any) {
          let vc = UpdateProfileVC.instance(storyBoard: .Profile) as! UpdateProfileVC
          push(viewController: vc)
      }

    @IBAction func actionUploadProfileImg(_ sender: Any) {
        imgType  = ImgType.profileImg
     imagepickerDocument.present(from: sender as! UIView)
        
    }
    @IBAction func actionUploadProfileBgImg(_ sender: Any) {
        imgType  = ImgType.BgImg
     imagepickerDocument.present(from: sender as! UIView)
        
    }

}

extension ProfileVC: MenuDelegate , TDImagePickerDelegate {
    func didSelect(image: UIImage?) {
        var param = [String : Any]()
        var img  = UIImageView()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        if  imgType == .profileImg{
            param[params.ktype] = "1"
            lmgProfile.image = image
            img = lmgProfile
        }else{
            lmgBgProfile.image = image
            param[params.ktype] = "2"
            img = lmgBgProfile
        }
        ServerManager.shared.POSTWithImage(url: ApiAction.update_image, param: param, imgParam: "image", imageView: img) { (data, error) in
            guard let data = data else {
                  print("data not available")
                  return
              }
             guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                  return
              }
              if obj.code == ResponseApis.KSuccess {
                presentAlert("", msgStr: obj.message, controller: self)
              } else {
                  print("failure")
                  presentAlert("", msgStr: obj.message, controller: self)
              }
          }
    }
    
    func onButtonClick(button str: MenuList) {
        if str == .share {
            showSheet()
          
        } else if str == .report {
            report_artist_API()
        } else if str == .block {
            block_usert_API()
        }
    }
    
    
    func showSheet(){
        let alert = UIAlertController(title: "Share", message: "Share your post", preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "In app share", style: .default , handler:{ (UIAlertAction)in
                let vc = FriendShareListVC.instance(storyBoard: .Video) as! FriendShareListVC
                vc.artistId = self.artistID
                vc.enumShareThrough = .profile
                vc.modalPresentationStyle = .overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                self.present(vc, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Outside app share", style: .default , handler:{ (UIAlertAction)in
                self.CreateDynamicLink()
            
            }))

            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            }))
            //uncomment for iPad Support
            //alert.popoverPresentationController?.sourceView = self.view

            self.present(alert, animated: true, completion: {
                print("completion block")
            })
    }
    
    
    func CreateDynamicLink(){
      //  print(userID)
      //  print(AppDataHelper.shard.logins.id)
        guard let link = URL(string: "http://artistarmy.de/deeplinking/artist_army.php?artist_id="+"\(userID)"+"&user_id="+"\(AppDataHelper.shard.logins.id ?? "")"+"&type=3") else { return }
        
            let dynamicLinksDomainURIPrefix = "https://artistarmy.page.link" //domain-link
            let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix )

            linkBuilder!.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.artistarmy.info")
            linkBuilder!.iOSParameters?.appStoreID = "1605546265"
        linkBuilder!.androidParameters = DynamicLinkAndroidParameters(packageName: "com.cti.artistarmy")
        
           guard let longDynamicLink = linkBuilder?.url else { return }
           print("The long URL is: \(longDynamicLink)")
        
//        longDynamicLink.shorten() { url, warnings, error in
//          guard let url = url, error != nil else { return }
//          print("The short URL is: \(url)")
//        }
        
     /*
       linkBuilder!.options = DynamicLinkComponentsOptions()
        linkBuilder!.options?.pathLength = .short
        linkBuilder!.shorten() { url, warnings, error in
            
          guard let url = url, error != nil else {
              print(error)
              print(warnings)
              print(error?.localizedDescription)
              return
              
          }
          print("The short URL is: \(url)")
        }*/
        
        
        
        
        let someText:String = "Share"
            let objectsToShare:URL = link
            let sharedObjects:[AnyObject] = [objectsToShare as AnyObject,someText as AnyObject]
            let activityViewController = UIActivityViewController(activityItems : sharedObjects, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view

        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook,UIActivity.ActivityType.postToTwitter,UIActivity.ActivityType.mail]

            self.present(activityViewController, animated: true, completion: nil)
    }
}

/*      Uri baseUrl = Uri.parse("http://artistarmy.de/deeplinking/artist_army.php?artist_id="+art_id+"&user_id="+sd.getKEY_ID()+"&type=3");
 String domain ="https://artistarmy.page.link";

 DynamicLink link = FirebaseDynamicLinks.getInstance()
         .createDynamicLink()
         .setLink(baseUrl)
         .setDomainUriPrefix(domain)
         .setIosParameters(new DynamicLink.IosParameters.Builder("com.artistarmy.info").build())
         .setAndroidParameters(new DynamicLink.AndroidParameters.Builder("com.cti.artistarmy").build())
         .buildDynamicLink();
 Log.e("DYNAMIC LINK","===123===="+link);
 Uri dynamicLinkUri = link.getUri();
*/


//-*-----

extension ProfileVC: UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
  
    
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == cvImg{
            return 6
        }
        else if cvSong == collectionView{
            if arrArtist_songs.count > 3{
                return 3
            }else{
                return arrArtist_songs.count
            }
            
            
        }else if cvTopFans == collectionView{
            if arrfans.count > 3{
                return 3
            }else{
                return arrfans.count
            }
            
        }else if cvCreditGoal == collectionView{
            if arrgoals.count > 3{
                return 3
            }else{
                return arrgoals.count
            }
        }else{
            if arrconcerts.count > 3{
                return 3
            }else{
                return arrconcerts.count
            }
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if cvImg == collectionView{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "profileCell", for: indexPath) as! profileCell
       /* if indexPath.row == 0{
            cell.img.image = UIImage(named: "profile")
            cell.lblNamw.text = "Profile"
            }
            else*/ if indexPath.row == 0{
                cell.img.image = UIImage(named: "media")
                cell.lblNamw.text = "Media"
            }
            else if indexPath.row == 1{
                cell.img.image = UIImage(named: "followers")
                cell.lblNamw.text = "Followers"
            }
            else if indexPath.row == 2{
                cell.img.image = UIImage(named: "concerts")
                cell.lblNamw.text = "Concerts"
            }
            else if indexPath.row == 3{
                cell.img.image = UIImage(named: "about")
                cell.lblNamw.text = "About"
            }/*
            else if indexPath.row == 4{
                cell.img.image = UIImage(named: "Groups")
                cell.lblNamw.text = "Groups"
            }*/
            else if indexPath.row == 4{
                cell.img.image = UIImage(named: "HallofFrame")
                cell.lblNamw.text = "Hall of fame"
            }
            else if indexPath.row == 5{
                cell.img.image = UIImage(named: "PinBoard")
                cell.lblNamw.text = "Pinboard"
            }
            
      
        return cell
        }else  if cvSong == collectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SongCVCell", for: indexPath) as! SongCVCell

            let dict = arrArtist_songs[indexPath.row]
            cell.lblSongName.text = dict.songs_title
            cell.btnPlay.tag = indexPath.row
            cell.btnPlay.addTarget(self, action: #selector(userDetail(_:)), for: .touchUpInside)
       if  dict.isplay{
        cell.btnPlay.isSelected = true
       }else{
        cell.btnPlay.isSelected = false
       }
            
            return cell
        }
      else  if cvTopFans == collectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileDetailCell", for: indexPath) as! ProfileDetailCell
        let dict = arrfans[indexPath.row]
        cell.lblFanName.text = dict.full_name
        cell.lblcount.text = dict.total_like
        cell.img.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.img.sd_setImage(with: URL(string: ProfileImagePath +  (dict.profile_image)! ), placeholderImage: UIImage(named: "user"))
       

      //  sd_setImageWithURL
        return cell
        }else if cvCreditGoal == collectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CreditGoalsDetailCell", for: indexPath) as! CreditGoalsDetailCell
            let dict = arrgoals[indexPath.row]
            cell.lblFanName.text = dict.goal_name
            cell.lblcount.text = dict.amount
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ConcertDetailCell", for: indexPath) as! ConcertDetailCell
            let dict = arrconcerts[indexPath.row]
            cell.lblName.text = dict.concert_title
            cell.img.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.img.sd_setImage(with: URL(string: ConcertImagePath +  (dict.concert_image)! ), placeholderImage: UIImage(named: "user"))
            return cell
        }
    }
    
    @objc func userDetail(_ sender: UIButton) {
      print("sender.tag\(sender.tag)")
       let dict = self.arrArtist_songs[sender.tag]
        
        add_credits_API(dict.user_id!, dict.id!, "\(sender.tag + 1)" )
        let ind =  IndexPath(row: sender.tag, section: 0)
        let cell = cvSong.cellForItem(at: ind) as! SongCVCell
      //  arrArtist_songs.map.($0.dict)
        for (index, element) in arrArtist_songs.enumerated() {
            print(element)
            arrArtist_songs[index].isplay = false
        }
        arrArtist_songs[sender.tag].isplay = true
       // getAudioData(audioUrl: songsPath + dict.songs_name!, cell: cell)
        cvSong.reloadData()
        getAudioData(audioUrl: songsPath + dict.songs_name!, cell: cell, arr: arrArtist_songs, ind: sender.tag)
        }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if cvImg == collectionView{
        var id = ""
        print("isComeFromFan=-=-=-=-=-=-=-==-=-=\(isComeFromFan)")
        
        if  isComeFromFan{
            id = userID //dictUserinfo?.id ?? ""
        }else{
            id = AppDataHelper.shard.logins.id!
        }
     /*   if indexPath.row == 0{
            let vc = ProfileDetailVC.instance(storyBoard: .Profile) as! ProfileDetailVC
          //   vc.artistID = id
          //  vc.isComeFromFan = isComeFromFan
            push(viewController: vc)
            }
            else*/
        if indexPath.row == 0{
                let vc = MediaVC.instance(storyBoard: .Profile) as! MediaVC
                vc.artistID = id
                push(viewController: vc)
            }
            else if indexPath.row == 1{
                let vc = FollowersVC.instance(storyBoard: .Profile) as! FollowersVC
                vc.comefromFriend = false
                if  isComeFromFan{
                    vc.artistID = id
                    vc.fanID = AppDataHelper.shard.logins.id!
                }else{
                    vc.artistID = AppDataHelper.shard.logins.id!
                    vc.fanID = ""
                }
               
                   push(viewController: vc)
            }
            else if indexPath.row == 2{
                let vc = ConcertBookNowVC.instance(storyBoard: .Profile) as! ConcertBookNowVC
                vc.artistID = id
                if  isComeFromFan{
                    vc.fanID = AppDataHelper.shard.logins.id!
                }else{
                    vc.fanID = ""
                }
                   push(viewController: vc)
            }
            else if indexPath.row == 3{
                let vc = AboutVC.instance(storyBoard: .Profile) as! AboutVC
                vc.artistID = id
                   push(viewController: vc)
            }
          /*  else if indexPath.row == 4{
               
                let vc = MyGroupsVC.instance(storyBoard: .Profile) as! MyGroupsVC
                vc.id = id
                   push(viewController: vc)
            }*/
            else if indexPath.row == 4{
                let vc = HalloffameStaticVC.instance(storyBoard: .Favourite) as! HalloffameStaticVC
                vc.isComeFromProfile = true
                vc.usedID = id
                vc.userName = dictUserinfo?.full_name ?? ""
                push(viewController: vc)
            }
            else if indexPath.row == 5{
                let vc = PinboardVC.instance(storyBoard: .Profile) as! PinboardVC
                if  isComeFromFan{
                    vc.id = id
                    vc.fan_id = AppDataHelper.shard.logins.id!
                }else{
                    vc.id = AppDataHelper.shard.logins.id!
                    vc.fan_id = ""
                }
                   push(viewController: vc)
            }
        }else if cvTopFans == collectionView{
         /*   let dict = arrArtist_songs[indexPath.row]
            print(dict.songs_title)
            
            getAudioData(audioUrl: songsPath + dict.songs_name!)*/
            
            
            let dict = arrfans[indexPath.row]
            if AppDataHelper.shard.logins.id != dict.fans_id{
            let vc = ProfileFanVC.instance(storyBoard: .Profile) as! ProfileFanVC
            vc.fanid = dict.fans_id!
            vc.isSeenFanProfile = true
            push(viewController: vc)
            }
            
            
        }else if cvCreditGoal == collectionView{
            
        }else{
            
        }
    }
 

}

extension ProfileVC {
    func getProfile_API(){
        dispatchGroup.enter()
        var param = [String : Any]()
        param[params.kref_user_id] =   appDelegate.ref_user_id
        param[params.kref_artist_id] = appDelegate.ref_artist_id
        
        if isComeFromFan{
            param[params.kfans_id] = AppDataHelper.shard.logins.id
            param[params.kartist_id] =  userID
        }else{
            param[params.kartist_id] = AppDataHelper.shard.logins.id
        }
        ServerManager.shared.POST(url: ApiAction.artistProfile , param: param, false,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(GetUserDetailModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
               
                self?.dispatchGroup.leave()
                self?.dictUserinfo = obj.userinfo
                self?.SetProfileData()
               
               
            } else {
                print("failure")
                
                Loader.hideLoader()
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    func SetProfileData(){
        lblTitleName.text = self.dictUserinfo?.full_name
        lblMusicType.text = self.dictUserinfo?.genre_name
        lblName.text = self.dictUserinfo?.address
        lblReedem.text = self.dictUserinfo?.total_redeem
        lblFollowers.text = self.dictUserinfo?.total_follower
        lblCredit.text = self.dictUserinfo?.total_credits
        lblPost.text = self.dictUserinfo?.total_post
        lmgProfile.sd_setImage(with: URL(string: ProfileImagePath +  (dictUserinfo?.profile_image)! ), placeholderImage: UIImage(named: "user"))
        lmgBgProfile.sd_setImage(with: URL(string: ProfileImagePath +  (dictUserinfo?.cover_image)! ), placeholderImage: UIImage(named: "user"))
        if self.dictUserinfo?.is_follow == "0"{
           //
            btnFollow.isSelected  = false
        }else{
          //  self.dictUserinfo?.is_follow = "0"
            btnFollow.isSelected  = true
        }
    }
    
    func AddFollow_API(){
         var param = [String : Any]()
         param[params.kfans_id] = AppDataHelper.shard.logins.id
        param[params.kartist_id] =  userID//dictUserinfo?.id
        
        ServerManager.shared.POST(url: ApiAction.add_followers , param: param, true,header: nil) { [weak self]  (data, error) in
             guard let data = data else {
                 print("data not available")
                 return
             }
             guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                 return
             }
             if obj.code == ResponseApis.KSuccess {
                if self?.dictUserinfo?.is_follow == "0"{
                    self?.btnFollow.isSelected  = true
                    self?.dictUserinfo?.is_follow = "1"
                }else{
                    self?.btnFollow.isSelected  = false
                    self?.dictUserinfo?.is_follow = "0"
                }
                
             } else {
                 print("failure")
                
                 presentAlert("", msgStr: obj.message, controller: self)
             }
         }
     }
}

class profileCell: UICollectionViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblNamw: UILabel!
  
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

  
}


extension ProfileVC{
    

    
  
    @IBAction func actionSong1(_ sender: Any) {
        viewPopup.frame = (self.view.window?.frame)!
        viewPopup.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        //viewPopupIn.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.view.window?.addSubview(viewPopup)
        
        //self.test(viewTest: viewPopupIn)
      
    }
    @IBAction func actionClosePopup(_ sender: Any) {
        self.viewHide(viewMain: viewPopup, viewPopUP: viewPopupIn)
    }
    
    
    //----
    
    @IBAction func actionTopFans(_ sender: Any) {
        let vc = FavouriteFanVC.instance(storyBoard: .Profile) as! FavouriteFanVC
        vc.arrfans = arrfans
        push(viewController: vc)
    }
    
    
    @IBAction func actionCreditGoals(_ sender: Any) {
        
        let vc = CreditGoalsVC.instance(storyBoard: .Profile) as! CreditGoalsVC
        vc.arrgoals = arrgoals
        push(viewController: vc)
    }
    
    
    
    @IBAction func actionConcerts(_ sender: Any) {
        let vc = ConcertBookNowVC.instance(storyBoard: .Profile) as! ConcertBookNowVC
        if isComeFromFan{
            vc.fanID = AppDataHelper.shard.logins.id!
        }
        vc.artistID = artistID
        push(viewController: vc)
    }
    
    
    
    
    
    
    
    func test(viewTest: UIView) {
            let orignalT: CGAffineTransform = viewTest.transform
            viewTest.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
            UIView.animate(withDuration: 0.4, animations: {
                
                viewTest.transform = orignalT
            }, completion:nil)
        }
    
        
        func viewHide(viewMain: UIView, viewPopUP: UIView) {
            let orignalT: CGAffineTransform = viewPopUP.transform
            UIView.animate(withDuration: 0.3, animations: {
                viewPopUP.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
            }, completion: {(sucess) in
                viewMain.removeFromSuperview()
                viewPopUP.transform = orignalT
            })
            navigationController?.navigationBar.isUserInteractionEnabled = true
            navigationController?.navigationBar.tintColor = UIColor.white
            
        }
    
    
}



extension ProfileVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(arrPostList.count)
        return  arrPostList.count
    }
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "FeedViewMultipleCell")  as! FeedViewMultipleCell
       cell.delegate = self
    let dict = arrPostList[indexPath.row]
      cell.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
      cell.imgProfile.sd_setImage(with: URL(string: ProfileImagePath +  dict.user_profile_image! ), placeholderImage: UIImage(named: "user"))
      cell.lblTitle.text = dict.user_full_name ?? "Honey"
      cell.lblFeedText.text = dict.description
      cell.lblTime.text = dict.created_at
     
      
      //like button
      if  dict.is_like  == "1"{
          cell.imgLike.image = UIImage(named: "like")
      }else{
           cell.imgLike.image = UIImage(named: "unlike")
      }
    
      if dict.total_like == "0"{
          cell.lblLike.text = dict.total_like! + " like"
      }
     else if dict.total_like == "1"{
          cell.lblLike.text = dict.total_like! + " like"
      }else{
          cell.lblLike.text = dict.total_like! + " likes"
      }
      //comment
      if dict.total_comment == "0"{
          cell.lblcomment.text =   "\(dict.total_comment ?? "0") comment"
         }else if dict.total_comment == "1"{
             cell.lblcomment.text = "\(dict.total_comment ?? "1") comment"
         }
      else{
             cell.lblcomment.text = "\(dict.total_comment ?? "") comments"
         }
      
      
      if dict.post_type == "images"{
          if  (dict.post_images != nil) {
               cell.constraintImageHeight.constant = 201
              if dict.post_images?.count == 1 {
                    
                  if  dict.post_type == "video"{
                    
                  }else{
                      cell.imgViewFeed?.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
                      cell.imgViewFeed?.sd_setImage(with: URL(string: PostImagePath + dict.post_images![0].image!), placeholderImage: UIImage(named: "user"))
                      cell.btnPlay.isHidden = true
                  }
                  cell.imgViewFeed.isHidden = false
                   cell.viewAllImg.isHidden = false
                  cell.stackTwoImage.isHidden = true
              }
             else if dict.post_images?.count == 2 {
              cell.imgViewFeed?.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
              cell.imgViewFeed?.sd_setImage(with: URL(string: PostImagePath + dict.post_images![0].image!), placeholderImage: UIImage(named: "user"))
              cell.imgViewFeed1?.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
              cell.imgViewFeed1?.sd_setImage(with: URL(string: PostImagePath + dict.post_images![1].image!), placeholderImage: UIImage(named: "user"))
                  cell.imgViewFeed1.isHidden = false
                  cell.imgViewFeed.isHidden = false
                   cell.viewAllImg.isHidden = false
                  cell.stackTwoImage.isHidden = false
                  cell.viewMoreImages.isHidden = true
              }
             
             else if dict.post_images!.count >= 3 {
                   cell.imgViewFeed.isHidden = false
              cell.imgViewFeed?.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
              cell.imgViewFeed?.sd_setImage(with: URL(string: PostImagePath + dict.post_images![0].image!), placeholderImage: UIImage(named: "user"))
              cell.imgViewFeed1?.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
              cell.imgViewFeed1?.sd_setImage(with: URL(string: PostImagePath + dict.post_images![1].image!), placeholderImage: UIImage(named: "user"))
              cell.imgViewFeed2?.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
              cell.imgViewFeed2?.sd_setImage(with: URL(string: PostImagePath + dict.post_images![2].image!), placeholderImage: UIImage(named: "user"))
              
              
                  cell.imgViewFeed.isHidden = false
                   cell.viewAllImg.isHidden = false
                  cell.imgViewFeed1.isHidden = false
                  cell.stackTwoImage.isHidden = false
                  cell.viewMoreImages.isHidden = false
              }else{
              cell.imgViewFeed.image  = UIImage(named: "user")
              }
          }
      }
      

      return cell
      }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let objPost = arrPostList[indexPath.row]
        if objPost.post_type  == "video" {
            
        } else {
            guard let cell = cell as? FeedViewMultipleCell else { return }
            if objPost.total_like == "0"{
                cell.lblLike.text = objPost.total_like! + " like"
            }
           else if objPost.total_like == "1"{
                cell.lblLike.text = objPost.total_like! + " like"
            }else{
                cell.lblLike.text = objPost.total_like! + " likes"
            }
        }
    }
      
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = arrPostList[indexPath.row]
        if dict.post_type == "images"{
            let obj = arrPostList[indexPath.row]
            let postId = arrPostList[indexPath.row].id
            let vc = CommentViewController.instance(storyBoard: .Home) as! CommentViewController
            vc.arrPostimages = obj.post_images
            vc.post_id = postId ?? ""
            push(viewController: vc)
            
        }
        
      
     }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        footerView.backgroundColor = .clear
        return footerView
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }

    
 
}




extension ProfileVC {
    func SongList_API(){
        dispatchGroup.enter()
        var param = [String : Any]()
        param[params.kuser_id] = artistID
       
        ServerManager.shared.POST(url: ApiAction.artistsongs_list , param: param, false,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(AllSongListModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.dispatchGroup.leave()
                self?.arrArtist_songs = obj.artist_songs!
                self?.cvSong.reloadData()
                if  self?.arrArtist_songs.count == 0{
                    self?.cvSong.displayBackgroundText()
                }else{
                    self?.cvSong.removeBackgroundText()
                }
            } else {
                print("failure")
                Loader.hideLoader()
                Artistarmy.show(message: obj.message ?? "")
            }
        }
    }
    
    func Get_allfansList_API(){
        dispatchGroup.enter()
        var param = [String : Any]()
        param[params.kuser_id] = artistID
        ServerManager.shared.POST(url: ApiAction.get_allfansList , param: param, false,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(FanModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.dispatchGroup.leave()
                self?.arrfans  = obj.fans!
                self?.cvTopFans.reloadData()
                if  self?.arrfans.count == 0{
                    self?.cvSong.displayBackgroundText()
                }else{
                    self?.cvSong.removeBackgroundText()
                }
            } else {
                Loader.hideLoader()
                Artistarmy.show(message: obj.message ?? "")
            }
        }
    }
    
    func Artistgoal_list_API(){
        dispatchGroup.enter()
        var param = [String : Any]()
        param[params.kuser_id] = artistID
        ServerManager.shared.POST(url: ApiAction.artistgoal_list , param: param, false,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(GoalModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.dispatchGroup.leave()
                self?.arrgoals  = obj.goals!
                self?.cvCreditGoal.reloadData()
                if  self?.arrgoals.count == 0{
                                  self?.cvSong.displayBackgroundText()
                              }else{
                                  self?.cvSong.removeBackgroundText()
                              }
            } else {
                print("failure")
                Loader.hideLoader()
                Artistarmy.show(message: obj.message ?? "")
            }
        }
    }
    func All_concert_API(){
        dispatchGroup.enter()
        var param = [String : Any]()
        param[params.kartist_id] = artistID
        ServerManager.shared.POST(url: ApiAction.all_concert , param: param, false,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(ConcertModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.dispatchGroup.leave()
                self?.arrconcerts  = obj.concerts!
                self?.cvConcert.reloadData()
                if  self?.arrconcerts.count == 0{
                                  self?.cvSong.displayBackgroundText()
                              }else{
                                  self?.cvSong.removeBackgroundText()
                              }
            } else {
                Loader.hideLoader()
                Artistarmy.show(message: obj.message ?? "")
            }
        }
    }
    func Newsfeed_list_API(){
        dispatchGroup.enter()
        var param = [String : Any]()
        param[params.kartist_id] = artistID
        if isComeFromFan{
            param[params.kfans_id] = AppDataHelper.shard.logins.id
        }
       
        
        ServerManager.shared.POST(url: ApiAction.newsfeed_list , param: param, false,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(HomeListingModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.dispatchGroup.leave()
                self?.arrPostList = obj.post_list!
                self?.constriantTblHeight.constant  = 0
                self?.constriantTblHeight.constant = CGFloat(408 * (self?.arrPostList.count)!)
                self?.constriantTblHeight.constant  += 826
                self?.tableViewFeed.reloadData()
                
            } else {
                Loader.hideLoader()
                print("failure")
                Artistarmy.show(message: obj.message ?? "")
            }
        }
    }
    
    
    func block_usert_API(){
        var param = [String : Any]()
        param[params.block_id] = artistID
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        ServerManager.shared.POST(url: ApiAction.block_user , param: param, true,header: nil) {  (data, error) in
            
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                Artistarmy.show(message: obj.message!)
            } else {
                print("failure")
               
                Artistarmy.show(message: obj.message ?? "")
            }
        }
    }
    
    func report_artist_API(){
        var param = [String : Any]()
        param[params.block_id] = artistID
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        ServerManager.shared.POST(url: ApiAction.report_artist , param: param, true,header: nil) {   (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                Artistarmy.show(message: obj.message!)
            } else {
                Artistarmy.show(message: obj.message ?? "")
            }
        }
    }
   
    
   

}






extension ProfileVC: UIPopoverPresentationControllerDelegate,FeedViewCellDelegate , HomePopupOverDelegate{
    
    
    func onButtonClick(button str: HomeList, index: IndexPath) {
        let  postid = arrPostList[index.row].id!
        if str == .deletePost{
            DeletePost_API(postid, index: index)
        }else if  str == .savePost{
            add_pinboard_API(postid, index: index)
        }else{
            //report
            
            report_user_API(postid, index: index)
            
            }
    }
    
    func AllComments(cell: UITableViewCell) {
        guard  let indexPath = tableViewFeed.indexPath(for: cell) else {
            return
        }
        let postId = arrPostList[indexPath.row].id
        let vc = CommentViewController.instance(storyBoard: .Home) as! CommentViewController
        vc.post_id = postId ?? ""
        vc.arrPostimages = arrPostList[indexPath.row].post_images
        push(viewController: vc)
    }
    
    func feedViewCellUserProfile(cell: UITableViewCell) {
        guard  let indexPath = tableViewFeed.indexPath(for: cell) else {
            return
        }
        let userID = arrPostList[indexPath.row].user_id
        
        
        if AppDataHelper.shard.logins.id == userID{
            self.tabBarController?.selectedIndex = 4
        }else{
            let vc = ProfileVC.instance(storyBoard: .Profile) as! ProfileVC
            vc.userID = userID!
            vc.isComeFromFan = true
            push(viewController: vc)
        }
       //
       
        
    }
    
    
    func feedViewCellLikePost(cell: UITableViewCell) {
        guard  let indexPath = tableViewFeed.indexPath(for: cell) else {
            return
        }
        guard let celli = cell as? FeedViewMultipleCell else {
            return
        }
        let dict = arrPostList[indexPath.row]
        var param = [String:Any]()
        param[params.kpost_id] = dict.id
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        ServerManager.shared.POST(url: ApiAction.post_like , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(LikeModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrPostList[indexPath.row].total_like = "\(obj.count ?? 0)"
                if  dict.is_like  == "1"{
                    //likepost
                    self?.arrPostList[indexPath.row].is_like = "0"
                    celli.imgLike.image = UIImage(named: "unlike")
                }else{
                    self?.arrPostList[indexPath.row].is_like = "1"
                    celli.imgLike.image = UIImage(named: "like")
                }
                self?.tableView(self!.tableViewFeed, willDisplay: cell, forRowAt: indexPath)
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    func DeletePost_API(_ postid : String , index : IndexPath){
        var param = [String : Any]()
        param[params.kpost_id] = postid
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        ServerManager.shared.POST(url: ApiAction.delete_post , param: param, false,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(LikeModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrPostList.remove(at:index.row)
                self?.tableViewFeed.deleteRows(at: [index], with: .fade)
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    
    func report_user_API(_ postid : String , index : IndexPath){
        let  user_id = self.arrPostList[index.row].user_id
        var param = [String : Any]()
        param[params.kpost_id] = postid
        param[params.block_id] = user_id
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        
        ServerManager.shared.POST(url: ApiAction.report_user , param: param, false,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(LikeModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                Artistarmy.show(message: obj.message!)
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    
    func add_pinboard_API(_ postid : String , index : IndexPath){
        var param = [String : Any]()
        param[params.kpost_id] = postid
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        ServerManager.shared.POST(url: ApiAction.add_pinboard , param: param, false,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
          let  is_saved = self?.arrPostList[index.row].is_saved
                if is_saved == "0"{
                    self?.arrPostList[index.row].is_saved = "1"
                }else{
                    self?.arrPostList[index.row].is_saved = "0"
                }
                Artistarmy.show(message: obj.message ?? "")
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    func popoverView(Cell: FeedViewMultipleCell, sender: UIButton) {
        guard let indexPath = tableViewFeed.indexPath(for: Cell) else {
            return
        }
        let alertController = HomePopupOver.instance(storyBoard: .Home) as! HomePopupOver
            let isSaved = arrPostList[indexPath.row].is_saved
            alertController.modalPresentationStyle = .popover
            let popover = alertController.popoverPresentationController
            popover?.delegate = self
            alertController.delegate = self
            alertController.myid = arrPostList[indexPath.row].user_id!
            alertController.isSaved = isSaved ?? "0"
            alertController.myIndex = indexPath
            popover?.sourceView = sender
            popover?.permittedArrowDirections =  .up
        if arrPostList[indexPath.row].user_id! == AppDataHelper.shard.logins.id{
              alertController.preferredContentSize = CGSize(width: 90, height: 80)
        }else{
            alertController.preferredContentSize = CGSize(width: 90, height: 80)
        }
          
            self.present(alertController, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}

extension ProfileVC {
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        appDelegate.ref_user_id = ""
       appDelegate.ref_artist_id = ""
      
        stopPlaying(true)
    //    self.cameraDelegate?.swiftyCamSessionDidStopRunning(self)
    }
    override func viewDidDisappear(_ animated: Bool) {
       stopPlaying(true)
        
    }
    
    func getAudioData(audioUrl: String , cell : SongCVCell ,arr : [Artist_songs] , ind : Int) {
        loderView.shared.showLoder()
        let urlString = audioUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
       
        if let url = URL(string: urlString!) {
         
        audioPlayerManager.prepareAudio(from: url) {
           if let totalDuration = self.audioPlayerManager.audioPlayer?.currentItem?.duration{
                let durationSeconds = CMTimeGetSeconds(totalDuration)
                let x = Double(durationSeconds).rounded(toPlaces: 2)
                print("Total songss duration--\(x)")
            loderView.shared.hideLoder()
            self.audioPlayerManager.playAudio { (progress) in
                DispatchQueue.main.async {
                    let y = Double(progress).rounded(toPlaces: 2)
              print("progress.........................1......................\(y)")
                    if y == 1{
                        self.arrArtist_songs[ind].isplay = false
                        cell.btnPlay.isSelected = false
                        self.cvSong.reloadData()
                    }
                    
           }
            }
            
            }
        }
        }
        
        }
    
    
    // MARK:- Pre-audio
    func prepareAudio(from urlString: String) {
        if audioPlayerManager.isPlaying{
            stopPlaying()
        }
        let urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        if let url = URL(string: urlString!) {
            loderView.shared.showLoder()
            
            audioPlayerManager.prepareAudio(from: url) {
                loderView.shared.hideLoder()

                self.audioPlayerManager.playAudio { (progress) in
                    DispatchQueue.main.async {
                  print("progress...................2............................\(progress)")
               }
                }
                isTabed = true
                
              
            }
        }
    }
    // MARK:- Pre-audio
    func stopPlaying(_ destroyPlayer: Bool = false) {
        audioPlayerManager.stopPlaying(destroyPlayer)
    }
    
    func add_credits_API( _ artist_id: String ,_ song_id : String , _ song_no :String ){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kartist_id] = artist_id
        param[params.ksong_id] = song_id
        param[params.ksong_no] = song_no
        param[params.krefer_id] = appDelegate.ref_user_id
        
        ServerManager.shared.POST(url: ApiAction.add_credits , param: param, true,header: nil) {   (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                
            } else {
                print("failure")
              //  presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }

}


class SongCVCell: UICollectionViewCell {
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var lblSongName: UILabel!
}

class ProfileDetailCell: UICollectionViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblFanName: UILabel!
    @IBOutlet weak var lblcount: UILabel!
}

class CreditGoalsDetailCell: UICollectionViewCell {
    @IBOutlet weak var img: UIView!
    @IBOutlet weak var lblFanName: UILabel!
    @IBOutlet weak var lblcount: UILabel!
}

class ConcertDetailCell: UICollectionViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblName: UILabel!
}




class songCell: UITableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}



