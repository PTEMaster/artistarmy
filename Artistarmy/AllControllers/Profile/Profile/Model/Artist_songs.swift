/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Artist_songs : Codable {
	let id : String?
	let user_id : String?
	let songs_title : String?
	let songs_name : String?
	let singer_name : String?
	let songs_image : String?
	let created_at : String?
    var isplay: Bool = false

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case user_id = "user_id"
		case songs_title = "songs_title"
		case songs_name = "songs_name"
		case singer_name = "singer_name"
		case songs_image = "songs_image"
		case created_at = "created_at"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
		songs_title = try values.decodeIfPresent(String.self, forKey: .songs_title)
		songs_name = try values.decodeIfPresent(String.self, forKey: .songs_name)
		singer_name = try values.decodeIfPresent(String.self, forKey: .singer_name)
		songs_image = try values.decodeIfPresent(String.self, forKey: .songs_image)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
	}

}
