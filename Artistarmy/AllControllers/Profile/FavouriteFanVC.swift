//
//  CreatePostVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit
import SDWebImage
class FavouriteFanVC: KBaseViewController {
    
    @IBOutlet weak var cvImg: UICollectionView!
    var arrfans = [Fans]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cvImg.delegate = self
        cvImg.dataSource = self
   
        
      let margin: CGFloat = 10
        guard let collectionView = cvImg, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }

            flowLayout.minimumInteritemSpacing = margin
            flowLayout.minimumLineSpacing = 1
            flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
    }
    

   
    @IBAction func actionBack(_ sender: Any) {
        pop()
    }
}



extension FavouriteFanVC: UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
  
    
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrfans.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavouriteArtistCell", for: indexPath) as! FavouriteArtistCell
        let dict = arrfans[indexPath.row]
        cell.lblName.text = dict.full_name
        cell.lblcount.text = dict.total_like
        cell.img.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.img.sd_setImage(with: URL(string: ProfileImagePath +  (dict.profile_image)! ), placeholderImage: UIImage(named: "user"))
      
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = arrfans[indexPath.row]
        if AppDataHelper.shard.logins.id != dict.fans_id{
        let vc = ProfileFanVC.instance(storyBoard: .Profile) as! ProfileFanVC
        vc.fanid = dict.fans_id!
        vc.isSeenFanProfile = true
        push(viewController: vc)
        }
    }
     
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let noOfCellsInRow = 3   //number of column you want
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size, height: size + 70)
    }
    
    
   
    
 
    
    
    

}






