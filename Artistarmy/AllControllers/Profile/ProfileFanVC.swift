//
//  HomeVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit
import SDWebImage

class ProfileFanVC: KBaseViewController , MenuDelegate{
    
    @IBOutlet weak var cvImg: UICollectionView!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnAddFriend: UIButton!
    @IBOutlet weak var viewAddFriend: UIView!
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var viewGiftCredits: UIView!
    @IBOutlet weak var viewEditProfile: UIView!
    @IBOutlet weak var viewBitStore: UIView!
    @IBOutlet weak var lblReedem: UILabel!
    @IBOutlet weak var lblFollowers: UILabel!
    @IBOutlet weak var lblCredit: UILabel!
    @IBOutlet weak var lblPost: UILabel!
    @IBOutlet weak var viewShare: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMusicType: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblTitleName: UILabel!
    @IBOutlet weak var lmgCheck: UIImageView!
    var imagepickerDocument:TDImagePicker!
    var isSeenFanProfile = false
    var fanid  = ""
    var dictUserinfo: FanUserinfo?
    //
    var arrfavourite_artist = [Favourite_artist]()
    var arrbookinginfo = [Bookinginfo]()
    @IBOutlet weak var cvTopFans: UICollectionView!
    @IBOutlet weak var cvConcert: UICollectionView!
    
    let dispatchGroup = DispatchGroup()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagepickerDocument = TDImagePicker(presentationController: self, delegate: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("isSeenFanProfile=-=-=-=-=-=-=-==-=-=\(isSeenFanProfile), \(fanid)")
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        btnCamera.isHidden = true
        if  isSeenFanProfile{
            //seen fan profile
            if AppDataHelper.shard.isFan{
                viewGiftCredits.isHidden = true
                viewEditProfile.isHidden = true
                viewBitStore.isHidden = true
            }else{
                viewAddFriend.isHidden = true
                viewEditProfile.isHidden = true
                viewBitStore.isHidden = true
                viewShare.isHidden = true
            }
        }else{
            //ownProfile
            btnCamera.isHidden = false
            viewMessage.isHidden = true
            viewGiftCredits.isHidden = true
            viewAddFriend.isHidden = true
        }
        viewShare.isHidden = true
        Loader.showLoader()
        getProfile_API()
        favourite_artistlist_API()
        concertbooked_byusert_API()
        dispatchGroup.notify(queue: .main) {
            
            Loader.hideLoader()
           }
        
        
        }
    override func viewWillDisappear(_ animated: Bool) {
    //    self.tabBarController?.tabBar.isHidden = true
    }
    
    
    @IBAction func actionUpdateProfile(_ sender: UIButton) {
 /*   let alertController = storyboard?.instantiateViewController(withIdentifier: "MenuPopupVC") as! MenuPopupVC
        alertController.modalPresentationStyle = .popover
        let popover = alertController.popoverPresentationController
        popover?.delegate = self
        alertController.delegate = self
        popover?.sourceView = sender
        popover?.permittedArrowDirections =  .up
        alertController.preferredContentSize = CGSize(width: 130, height: 120)
        self.present(alertController, animated: true, completion: nil)
        */
    }
    
    @IBAction func actionBack(_ sender: Any) {
      
      //  self.tabBarController?.selectedIndex = 0
      //  pop()
       if isSeenFanProfile{
            pop()
        }else{
            self.tabBarController?.selectedIndex = 0
        }
    }
    
    @IBAction func actionEditProfile(_ sender: Any) {
          let vc = UpdateFanProfileVC.instance(storyBoard: .Profile) as! UpdateFanProfileVC
          push(viewController: vc)
      }
    @IBAction func actionUploadProfileImg(_ sender: Any) {
     imagepickerDocument.present(from: sender as! UIView)
        
    }
    @IBAction func actionAddfriend(_ sender: Any) {
        add_friend_API()
    }
    
    @IBAction func actionDonateBit(_ sender: Any) {
        let vc = BitStoreVC.instance(storyBoard: .Profile) as! BitStoreVC
        push(viewController: vc)
      }

 

    @IBAction func actionBitStore(_ sender: Any) {
          let vc = BitStoreVC.instance(storyBoard: .Profile) as! BitStoreVC
        vc.isComeFromBitStore = true
          push(viewController: vc)
      }
    
    
    @IBAction func actionFavArtist(_ sender: Any) {
        let vc = FavouriteArtistVC.instance(storyBoard: .Profile) as! FavouriteArtistVC
        vc.arrfavourite_artist = arrfavourite_artist
        push(viewController: vc)
    }
    
    @IBAction func actionConcerts(_ sender: Any) {
        let vc = ConcertBookNowVC.instance(storyBoard: .Profile) as! ConcertBookNowVC
        vc.isComeFromFan = true
        var id = ""
        if  isSeenFanProfile{
            id =  fanid//dictUserinfo?.id ?? ""
        }else{
            id = AppDataHelper.shard.logins.id!
        }
        vc.fanID = id
        push(viewController: vc)
    }
    
   
}


extension ProfileFanVC: UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
  
    
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if cvTopFans == collectionView{
            if arrfavourite_artist.count > 3{
                return 3
            }else{
                return arrfavourite_artist.count
            }
        }else if cvConcert == collectionView{
            if arrbookinginfo.count > 3{
                return 3
            }else{
                return arrbookinginfo.count
            }
        }else{
            return 5
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if cvTopFans == collectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileDetailCell", for: indexPath) as! ProfileDetailCell
            let dict  = arrfavourite_artist[indexPath.row]
            
           cell.lblFanName.text = dict.full_name
            cell.lblcount.text = dict.total_like
            cell.img.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.img.sd_setImage(with: URL(string: ProfileImagePath +  (dict.profile_image)! ), placeholderImage: UIImage(named: "user"))
           
            return cell
        }else if cvConcert == collectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ConcertDetailCell", for: indexPath) as! ConcertDetailCell
            let dict  = arrbookinginfo[indexPath.row]
            cell.lblName.text = dict.concert_title
            cell.img.sd_imageIndicator = SDWebImageActivityIndicator.gray
          
            cell.img.sd_setImage(with: URL(string: ProfileImagePath +  dict.concert_image! ), placeholderImage: UIImage(named: "user"))
            
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "profileCell", for: indexPath) as! profileCell
            if indexPath.row == 0{
                    cell.img.image = UIImage(named: "media")
                    cell.lblNamw.text = "Media"
                }
                else if indexPath.row == 1{
                    cell.img.image = UIImage(named: "followers")
                    cell.lblNamw.text = "Friends"
                }
                else if indexPath.row == 2{
                    cell.img.image = UIImage(named: "about")
                    cell.lblNamw.text = "About"
                }
                else if indexPath.row == 3{
                    cell.img.image = UIImage(named: "HallofFrame")
                    cell.lblNamw.text = "Following"
                }
                else if indexPath.row == 4{
                    cell.img.image = UIImage(named: "PinBoard")
                    cell.lblNamw.text = "Pinboard"
                }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if cvTopFans == collectionView{
            let dict  = arrfavourite_artist[indexPath.row]
            if   AppDataHelper.shard.logins.id != dict.artist_id {
            let vc = ProfileVC.instance(storyBoard: .Profile) as! ProfileVC
            vc.userID = dict.artist_id!
            vc.isComeFromFan = true
            push(viewController: vc)
            }
            
        }else if cvConcert == collectionView{
            
        }else{
            var id = ""
            if  isSeenFanProfile{
                id =  fanid//dictUserinfo?.id ?? ""
            }else{
                id = AppDataHelper.shard.logins.id!
            }
            
            if indexPath.row == 0{
                let vc = MediaVC.instance(storyBoard: .Profile) as! MediaVC
                vc.artistID = id
                push(viewController: vc)
            }
            else if indexPath.row == 1{// friends
                let vc = FollowersVC.instance(storyBoard: .Profile) as! FollowersVC
                vc.comefromFriend = true
                if  isSeenFanProfile{
                    vc.artistID =  AppDataHelper.shard.logins.id!
                    vc.fanID = id
                }else{
                    vc.artistID = AppDataHelper.shard.logins.id!
                    vc.fanID = ""
                }
                push(viewController: vc)
            }
            else if indexPath.row == 2{
                let vc = AboutFanVC.instance(storyBoard: .Profile) as! AboutFanVC
                vc.artistID = id
                push(viewController: vc)
            }
            else if indexPath.row == 3{ // following
                let vc = FollowersVC.instance(storyBoard: .Profile) as! FollowersVC
                if  isSeenFanProfile{
                    vc.artistID = AppDataHelper.shard.logins.id!
                    vc.fanID = id
                }else{
                    vc.artistID = ""
                    vc.fanID = AppDataHelper.shard.logins.id!
                }
                push(viewController: vc)
            }
            else if indexPath.row == 4{
                let vc = PinboardVC.instance(storyBoard: .Profile) as! PinboardVC
                if  isSeenFanProfile{
                    vc.id = id
                    vc.fan_id = AppDataHelper.shard.logins.id!
                }else{
                    vc.id = AppDataHelper.shard.logins.id!
                    vc.fan_id = ""
                }
                push(viewController: vc)
            }
        }
        }
}




extension ProfileFanVC {
    func getProfile_API(){
        var param = [String : Any]()
        if isSeenFanProfile{
            param[params.kfans_id] =  AppDataHelper.shard.logins.id
            param[params.kuser_id] =  fanid
        }else{
            param[params.kuser_id] = AppDataHelper.shard.logins.id
        }
        ServerManager.shared.POST(url: ApiAction.fanProfile , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(FanDetailModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.dictUserinfo = obj.userinfo
                self?.SetProfileData()
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    func SetProfileData(){
        if !isSeenFanProfile{
            AppDataHelper.shard.logins.total_donate_bit = self.dictUserinfo?.total_donate_bit
        }
        lblTitleName.text = self.dictUserinfo?.full_name
        lblMusicType.text = self.dictUserinfo?.genre_name
        lblName.text = self.dictUserinfo?.address
        lblReedem.text = self.dictUserinfo?.total_redeem
        lblFollowers.text = self.dictUserinfo?.total_friend
        lblCredit.text = self.dictUserinfo?.total_credits
        lblPost.text = self.dictUserinfo?.total_shared
        imgProfile.sd_setImage(with: URL(string: ProfileImagePath +  (dictUserinfo?.profile_image)! ), placeholderImage: UIImage(named: "user"))
        if self.dictUserinfo?.is_friend == "0"{
            self.btnAddFriend.isSelected  = false
        }else{
            self.btnAddFriend.isSelected  = true
        }
  
       
    }
    
    
    func add_friend_API(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
       param[params.kfriends_id] = fanid
        ServerManager.shared.POST(url: ApiAction.add_friends , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                if self?.dictUserinfo?.is_friend == "0"{
                    self?.btnAddFriend.isSelected  = true
                    self?.dictUserinfo?.is_friend = "1"
                }else{
                    self?.btnAddFriend.isSelected  = false
                    self?.dictUserinfo?.is_friend = "0"
                }
             } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
    
    
   
}



extension ProfileFanVC:  TDImagePickerDelegate {
    func didSelect(image: UIImage?) {
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.ktype] = "1"
        imgProfile.image = image
        ServerManager.shared.POSTWithImage(url: ApiAction.update_image, param: param, imgParam: "image", imageView: imgProfile) { (data, error) in
            guard let data = data else {
                  print("data not available")
                  return
              }
             guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                  return
              }
              if obj.code == ResponseApis.KSuccess {
                //Artistarmy.show(message: obj.message ?? "")
              } else {
                  print("failure")
               // Artistarmy.show(message: obj.message ?? "")
              }
          }
    }
    
   
}

extension ProfileFanVC: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    func onButtonClick(button str: MenuList) {
        if str == .share {
            
        } else if str == .report {
            
        } else if str == .block {
            
        }
    }
}




extension ProfileFanVC {
    func favourite_artistlist_API(){
        var param = [String : Any]()
        var id = ""
        if  isSeenFanProfile{
            id =  fanid//dictUserinfo?.id ?? ""
        }else{
            id = AppDataHelper.shard.logins.id!
        }
        param[params.kuser_id] = id
        ServerManager.shared.POST(url: ApiAction.favourite_artistlist , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(FanFavouriteArtistModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrfavourite_artist = obj.favourite_artist  ?? []
                self?.cvTopFans.reloadData()
            } else {
                print("failure")
                Artistarmy.show(message: obj.message ?? "")
            }
        }
    }
    func concertbooked_byusert_API(){
        var param = [String : Any]()
        var id = ""
        if  isSeenFanProfile{
            id =  fanid//dictUserinfo?.id ?? ""
        }else{
            id = AppDataHelper.shard.logins.id!
        }
        param[params.kuser_id] = id
        ServerManager.shared.POST(url: ApiAction.concertbooked_byuser , param: param, false,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(FanConcertModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrbookinginfo = obj.bookinginfo ?? []
                self?.cvConcert.reloadData()
            } else {
                print("failure")
               // Artistarmy.show(message: obj.message ?? "")
            }
        }
    }
}
