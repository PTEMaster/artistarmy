//
//  CreatePostVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit
import SDWebImage
import  SafariServices


class SettingVC: KBaseViewController {

    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if  let dict = AppDataHelper.shard.logins{
            lblName.text = dict.full_name
            imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
           imgProfile.sd_setImage(with: URL(string: ProfileImagePath +  dict.profile_image! ), placeholderImage: UIImage(named: "user"))
        let cc =   dict.country_code
            lblNumber.text = cc! + " " + dict.mobile_number! 
            
        }
        
    }
    

    
    @IBAction func actionProfile(_ sender: Any) {
        return
        let vc = UpdateProfileVC.instance(storyBoard: .Profile) as! UpdateProfileVC
                push(viewController: vc)
    }
    
    
    
    @IBAction func actionLogout(_ sender: Any) {
     //   UserDefaults.standard.setLoggedIn(value: false)
       // UserDefaults.standard.set("false", forKey: UserDefaultKey.isUserLogin)
       // UserDefaults.standard.removeObject(forKey: UserDefaultKey.userDict)
      //  UserDefaults.standard.removeObject(forKey: UserDefaultKey.loginDate)
        UserDefaults.standard.removeAllUserdefault()
        SwitchNav.loginRootNavigation()
    }
    
    @IBAction func actionPreview(_ sender: Any) {
        return
        let vc = PreviewIntroductionVC.instance(storyBoard: .Profile) as! PreviewIntroductionVC
                push(viewController: vc)

     }
    
    @IBAction func actionSong(_ sender: Any) {
        return
        let vc = SongsVC.instance(storyBoard: .Profile) as! SongsVC
                push(viewController: vc)

     }
    
    @IBAction func actionConcert(_ sender: Any) {
        let vc = ConcertVC.instance(storyBoard: .Profile) as! ConcertVC
                push(viewController: vc)

     }
    
    @IBAction func actionCreditGoal(_ sender: Any) {
     
        let vc = CreateCreditGoalsVC.instance(storyBoard: .Profile) as! CreateCreditGoalsVC
                push(viewController: vc)

     }
    @IBAction func actionTermsCondition(_ sender: Any) {
       
        let vc = SFSafariViewController(url: URL(string: termsCondition)!)
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)

     }
    @IBAction func actionPrivacyPolicy(_ sender: Any) {
        let vc = SFSafariViewController(url: URL(string: privacypolicy)!)
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)

     }
    
    
    
    
    
    
    
  
    @IBAction func actionBack(_ sender: Any) {
        pop()
    }
    
    
}





extension SettingVC: SFSafariViewControllerDelegate {
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        print("safari did finish")
    }
    
    func safariViewController(_ controller: SFSafariViewController, initialLoadDidRedirectTo URL: URL) {
        print("url\(URL.absoluteString)")
        let urlString = "https://www.google.co.in/"
        
        if URL.absoluteString == urlString {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                // do stuff 42 seconds later
                self.dismiss(animated: true) {
                    NotificationCenter.default.post(name: Notification.Name("popAndPush"), object: nil)
                    self.navigationController?.popViewController(animated: true)
                    
                }
            }
        }
    }
}
