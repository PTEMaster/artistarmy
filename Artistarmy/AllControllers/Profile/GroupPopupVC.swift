//
//  MenuPopupVC.swift
//  IRatePro
//
//  Created by mac on 17/02/21.
//

import UIKit
protocol GroupDelegate {
    func onButtonClick(button str:GroupList)
}
class GroupPopupVC: UIViewController {

    @IBOutlet weak var btnLeave: UIButton!
    @IBOutlet weak var btnMute: UIButton!
    var delegate: GroupDelegate?
  
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    // MARK: - Button Action

    @IBAction func actionLeave(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.onButtonClick(button: .groupLeave)
        }
    }
    
    @IBAction func actionMute(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.onButtonClick(button: .groupMute)
        }
    }
    
   
}


enum GroupList {
    case groupLeave
    case groupMute
}
