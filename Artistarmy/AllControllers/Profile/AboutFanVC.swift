//
//  HomeVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit

class AboutFanVC: KBaseViewController {

    @IBOutlet weak var lblRealEmail: UILabel!
    @IBOutlet weak var lblDOB: UILabel!
    @IBOutlet weak var lblBiography: UILabel!
    var artistID = ""
    //Variable
    var dictUserinfo: Userinfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        getProfile_API()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @IBAction func actionBack(_ sender: Any) {
       pop()
    }
     
    
 
    func SetProfileData(){
      //  self?.dictUserinfo
        self.lblRealEmail.text = dictUserinfo?.full_name
        self.lblDOB.text = dictUserinfo?.date_of_birth
        self.lblBiography.text = dictUserinfo?.biography
    }
  
    
    
}

extension AboutFanVC {
    func getProfile_API(){
        var param = [String : Any]()
        param[params.kuser_id] = artistID
        ServerManager.shared.POST(url: ApiAction.getProfile , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(AboutModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.dictUserinfo = obj.userinfo
                self?.SetProfileData()
            } else {
                print("failure")
                presentAlert("", msgStr: obj.message, controller: self)
            }
        }
    }
}

