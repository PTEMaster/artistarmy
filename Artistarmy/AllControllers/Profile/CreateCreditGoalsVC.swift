//
//  CreatePostVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit
import SDWebImage


class CreateCreditGoalsVC: KBaseViewController , UITextFieldDelegate{

    @IBOutlet weak var cvImg: UICollectionView!
    @IBOutlet weak var TfGoalName: UITextField!
    @IBOutlet weak var tfAmountCreditNeeded: UITextField!
    @IBOutlet weak var tfExpireDate: UITextField!
    @IBOutlet weak var tfExpTime: UITextField!
    @IBOutlet weak var tfDiscription: UITextField!
    @IBOutlet weak var imgDoc: UIImageView!
    var picker = UIDatePicker()
    var  isDate = true
    var imagepickerDocument:TDImagePicker!
    var arrgoals = [Goals]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagepickerDocument = TDImagePicker(presentationController: self, delegate: self)
        cvImg.delegate = self
        cvImg.dataSource = self
   
        
      let margin: CGFloat = 10
        guard let collectionView = cvImg, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }

            flowLayout.minimumInteritemSpacing = margin
            flowLayout.minimumLineSpacing = 10
            flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
        
        
        picker = UIDatePicker()
        tfExpireDate.inputView = picker
        tfExpTime.inputView = picker
        
        
        picker.addTarget(self, action: #selector(self.handleDatePicker), for: UIControl.Event.valueChanged)
        
        picker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            picker.preferredDatePickerStyle = .wheels
        } else {
            picker.datePickerMode = UIDatePicker.Mode.date
          
        }
        Artistgoal_list_API()
        
    }
    @objc func handleDatePicker() {
        if isDate{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            tfExpireDate.text = dateFormatter.string(from: picker.date)
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm:ss"
            tfExpTime.text = dateFormatter.string(from: picker.date)
        }
        
        
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tfExpireDate{
            isDate = true
            picker.datePickerMode = .date
        }else{
            isDate = false
            picker.datePickerMode = .time
            }
    }

   
    @IBAction func actionBack(_ sender: Any) {
        pop()
    }
    
    @IBAction func actionCreate(_ sender: Any) {
        
        
        let valid = validation()
        if valid.status {
            
            Add_artistgoals_API()
        }else{
            presentAlert("", msgStr: valid.message, controller: self)
        }
        
       
    }
    
    @IBAction func actionUploadPic(_ sender: Any) {
        
        imagepickerDocument.present(from: sender as! UIView)
        
       
    }
    
    
    func validation() -> (status:Bool, message:String) {
        var msg:String = ""
        var status:Bool = true
        if TfGoalName.text == "" {
            status = false
            msg = Validation.kGoalName.rawValue
        }
        else if  tfAmountCreditNeeded.text == ""{
            status = false
            msg = Validation.kCerditsNedded.rawValue
        }
        else if   tfExpireDate.text == ""{
            status = false
            msg = Validation.kdate.rawValue
        }
        else if   tfExpTime.text == ""{
            status = false
            msg = Validation.ktime.rawValue
        }
        else if tfDiscription.text == ""{
            status = false
            msg = Validation.kdescription.rawValue
        }
        else if imgDoc.image == nil{
            status = false
            msg = Validation.kImageUpload.rawValue
        }
            return (status:status, message:msg)
        
        }
    
}



extension CreateCreditGoalsVC: UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
  
    
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrgoals.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CreditGoalsCell", for: indexPath) as! CreditGoalsCell
      
/*let dict = arrgoals[indexPath.row]
        cell.lblGoalName.text = dict.goal_name
        cell.lblcount.text = dict.amount
        cell.img.sd_setImage(with: URL(string: goalImagePath +  (dict.goal_image)! ), placeholderImage: UIImage(named: "user"))
        */
        
        
        
        let dict = arrgoals[indexPath.row]
        cell.lblGoalName.text = dict.goal_name
        cell.lblcount.text = dict.amount
        cell.lblprogressPercentage.text = dict.progress_bar! + "%"
        
        let per = Float(dict.progress_bar!)!/100
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            UIView.animate(withDuration: 0.5, delay: 0, options: [.beginFromCurrentState, .allowUserInteraction], animations: { [unowned self] in
            cell.progress.setProgress( per, animated: true)
          })
        }
        //progress_bar
        if dict.goal_image != ""{
            cell.img.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.img.sd_setImage(with: URL(string: goalImagePath +  dict.goal_image! ), placeholderImage: UIImage(named: "user"))
            cell.lblcount.textColor = UIColor(red: 241/255, green: 67/255, blue: 54/255, alpha: 1.0)
            cell.viewbg.layer.borderColor = UIColor.gray.cgColor
            cell.viewbg.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        }else{
            cell.img.image = UIImage(named: "")
            if dict.color_code == "red"{
                let redColor = UIColor(red: 241/255, green: 67/255, blue: 54/255, alpha: 1.0)
                cell.lblcount.textColor = redColor
                cell.viewbg.layer.borderColor = redColor.cgColor
                cell.viewbg.backgroundColor = redColor.withAlphaComponent(0.5)
                cell.progress.trackTintColor = redColor
            }else if dict.color_code == "green"{
                let greenColor = UIColor(red: 52/255, green: 190/255, blue: 135/255, alpha: 1.0)
                cell.lblcount.textColor = greenColor
                cell.viewbg.layer.borderColor = greenColor.cgColor
                cell.viewbg.backgroundColor = greenColor.withAlphaComponent(0.5)
                cell.progress.trackTintColor = greenColor
            }else{
                let yellowColor = UIColor(red: 224/255, green: 155/255, blue: 60/255, alpha: 1.0)
                cell.lblcount.textColor = yellowColor
                cell.viewbg.layer.borderColor = yellowColor.cgColor
                cell.viewbg.backgroundColor = yellowColor.withAlphaComponent(0.5)
                cell.progress.trackTintColor = yellowColor
            }
           
          
        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
       
    }
     
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let noOfCellsInRow = 3   //number of column you want
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size, height: size + 30)
    }
    
    
   
    func Add_artistgoals_API(){
        
       /* user_id:8
        goal_name:Hip Hop Artist
        amount:100
        exp_date:2021-10-15
        exp_time:18:20:20
        description:fitness/dance
        goal_image:
        */
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.goal_name] = TfGoalName.text
        param[params.amount] = tfAmountCreditNeeded.text
        param[params.exp_date] = tfExpireDate.text
        param[params.exp_time] = tfExpTime.text
        param[params.description] = tfDiscription.text
            param[params.goal_image] = ""
           // ServerManager.shared.POST(url: ApiAction.add_artistgoals , param: param, true,header: nil) { [weak self]  (data, error) in
                
                
                ServerManager.shared.POSTWithImage(url: ApiAction.add_artistgoals, param: param, imgParam: params.goal_image, imageView: imgDoc) { (data, error) in
                guard let data = data else {
                    print("data not available")
                    return
                }
                guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                    return
                }
                if obj.code == ResponseApis.KSuccess {
                    self.pop()
                    presentAlert("", msgStr: obj.message, controller: self)
                } else {
                    print("failure")
                    
                    presentAlert("", msgStr: obj.message, controller: self)
                }
            }
    }
    
    
    func Artistgoal_list_API(){
   
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        ServerManager.shared.POST(url: ApiAction.artistgoal_list , param: param, false,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(GoalModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                
                self?.arrgoals  = obj.goals!
                self?.cvImg.reloadData()
                if  self?.arrgoals.count == 0{
                    self?.cvImg.displayBackgroundText()
                }else{
                    self?.cvImg.removeBackgroundText()
                }
            } else {
                print("failure")
                Loader.hideLoader()
                Artistarmy.show(message: obj.message ?? "")
            }
        }
    }
}


extension CreateCreditGoalsVC : TDImagePickerDelegate {
    func didSelect(image: UIImage?) {
        imgDoc.image = image
    }
}















