//
//  HomeVC.swift
//  Artistarmy
//
//  Created by mac on 07/06/21.
//

import UIKit
import SDWebImage

class ConcertVC: KBaseViewController , UITextFieldDelegate{
    
    @IBOutlet weak var tblConcert: UITableView!
    @IBOutlet weak var tfEnterTitle: UITextField!
    @IBOutlet weak var tfEnterDate: UITextField!
    @IBOutlet weak var tfTime: UITextField!
    @IBOutlet weak var tfVenue: UITextField!
    @IBOutlet weak var tfDescription: UITextField!
    
    var arrconcerts = [Concerts]()
    var picker = UIDatePicker()
    var  isDate = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker = UIDatePicker()
        tfEnterDate.inputView = picker
        tfTime.inputView = picker
        
        
        picker.addTarget(self, action: #selector(self.handleDatePicker), for: UIControl.Event.valueChanged)
        
        picker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            picker.preferredDatePickerStyle = .wheels
        } else {
            picker.datePickerMode = UIDatePicker.Mode.date
          
        }
      
        All_concert_API()
    }
    @objc func handleDatePicker() {
        if isDate{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yy-MM-dd"
            tfEnterDate.text = dateFormatter.string(from: picker.date)
            
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            tfTime.text = dateFormatter.string(from: picker.date)
        }
        
        
    }
    @objc func handleDatePickerTime() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        tfTime.text = dateFormatter.string(from: picker.date)
        tfTime.resignFirstResponder()
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tfEnterDate{
            isDate = true
            picker.datePickerMode = .date
        }else{
            isDate = false
            picker.datePickerMode = .time
            }
    }
    
    @IBAction func actionBack(_ sender: Any) {
       pop()
    }
    @IBAction func acriobCreate(_ sender: Any) {
        
        let valid = validation()
        if valid.status {
            
            add_concert_API()
        }else{
            presentAlert("", msgStr: valid.message, controller: self)
        }
       
    }
    
    func validation() -> (status:Bool, message:String) {
        var msg:String = ""
        var status:Bool = true
        if tfEnterTitle.text == "" {
            status = false
            msg = Validation.kEnterTitle.rawValue
        }
        else if  tfEnterDate.text == ""{
            status = false
            msg = Validation.kdate.rawValue
        }
        else if   tfTime.text == ""{
            status = false
            msg = Validation.ktime.rawValue
            
        }
        else if  tfVenue.text == ""{
            status = false
            msg = Validation.kvenue.rawValue
        }
        else if   tfDescription.text == ""{
            status = false
            msg = Validation.kdescription.rawValue
        }
            return (status:status, message:msg)
        
        }
}
extension ConcertVC: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return arrconcerts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConcertCell", for: indexPath) as! ConcertCell
        cell.btnDelete.layer.cornerRadius = 25
        cell.btnDelete.layer.maskedCorners = [ .layerMinXMaxYCorner , .layerMaxXMaxYCorner]
        let obj = arrconcerts[indexPath.row]
        cell.lblName.text = obj.concert_title
        cell.lblDescription.text = obj.description
        cell.lblDate.text = obj.concert_date
        cell.lblTime.text = obj.concert_time
        cell.lblAddress.text = obj.concert_venue
        cell.btnDelete.tag = indexPath.row
        cell.imgUser.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgUser.sd_setImage(with: URL(string: ConcertImagePath +  obj.concert_image! ), placeholderImage: UIImage(named: "user"))
        cell.btnDelete.backgroundColor = .red
        cell.btnDelete.setTitle("Cancel Booking", for: .normal)
       cell.btnDelete.addTarget(self, action: #selector(cancelBooking(_:)), for: .touchUpInside)
       
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   
   }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func cancelBooking(_ sender: UIButton) {
        let dict = self.arrconcerts[sender.tag]
       cancel_concert_API(dict.concert_id ?? "")
     }
    
    func cancel_concert_API(_ concertId : String){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kconcert_id] = concertId
        ServerManager.shared.POST(url: ApiAction.delete_concerts , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.All_concert_API()
            } else {
                print("failure")
                Artistarmy.show(message: obj.message ?? "")
            }
        }
    }
    
    func All_concert_API(){
        var param = [String : Any]()
        param[params.kartist_id] = AppDataHelper.shard.logins.id
        ServerManager.shared.POST(url: ApiAction.all_concert , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(ConcertModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.arrconcerts  = obj.concerts!
                self?.tblConcert.reloadData()
                
            } else {
               
                Artistarmy.show(message: obj.message ?? "")
            }
        }
    }
    
    func add_concert_API(){
        var param = [String : Any]()
        param[params.kuser_id] = AppDataHelper.shard.logins.id
        param[params.kconcert_title ]   = tfEnterTitle.text
        param[params.kconcert_date ] = tfEnterDate.text
        param[params.kconcert_time ] = tfTime.text
        param[params.kconcert_venue ] = tfVenue.text
        param[params.kdescription ] = tfDescription.text
        param[params.kconcert_image ] = ""
        
        ServerManager.shared.POST(url: ApiAction.add_concerts , param: param, true,header: nil) { [weak self]  (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(SuccessModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self?.All_concert_API()
            } else {
                print("failure")
                Artistarmy.show(message: obj.message ?? "")
            }
        }
    }
    
  
}



class ConcertCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    
    
    @IBAction func actionDelete(_ sender: Any) {
        
    }
    
}
