//
//  MenuPopupVC.swift
//  IRatePro
//
//  Created by mac on 17/02/21.
//

import UIKit
protocol MenuDelegate {
    func onButtonClick(button str:MenuList)
}
class MenuPopupVC: UIViewController {

    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnReport: UIButton!
    @IBOutlet weak var btnBlock: UIButton!
    var delegate: MenuDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    // MARK: - Button Action

    @IBAction func actionShare(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.onButtonClick(button: .share)
        }
    }
    
    @IBAction func actionReport(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.onButtonClick(button: .report)
        }
    }
    
    @IBAction func actionBlock(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.onButtonClick(button: .block)
        }
    }
}


enum MenuList {
    case share
    case report
    case block
}
