//
//  AppDelegate.swift
//  Artistarmy
//
//  Created by mac on 03/06/21.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import FirebaseMessaging


@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var strFcmToken:String = ""
    //---
    var ref_user_id = ""
    var ref_artist_id = ""
    
    var post_id = ""
    var reel_id = ""
    //----
    let pageCount = 15
        
    let customURLScheme = "https://artistarmy.page.link/share"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
              IQKeyboardManager.shared.enable = true
              IQKeyboardManager.shared.enableAutoToolbar = true
              if #available(iOS 13.0, *) {
                          // Always adopt a light interface style.
                window?.overrideUserInterfaceStyle = .light
                application.statusBarStyle = .default
            }
        
        FirebaseOptions.defaultOptions()?.deepLinkURLScheme = customURLScheme
        FirebaseApp.configure()
        setupNotification(application, launchOptions: launchOptions)
        
              /*  for family: String in UIFont.familyNames
                   {
                       print("\(family)\n")
                       for names: String in UIFont.fontNames(forFamilyName: family)
                       {
                           print("== \(names)")
                       }
                   }*/
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
  
    }
    //-------------------------------------------------
    //MARK:- Setup Notification
    //-------------------------------------------------
    func setupNotification(_ application: UIApplication, launchOptions:[UIApplication.LaunchOptionsKey: Any]? ) {
        Messaging.messaging().delegate = self
        application.registerForRemoteNotifications()
        application.applicationIconBadgeNumber = 0
        requestNotificationAuthorization(application: application)
        if let userInfo = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] {
            NSLog("[RemoteNotification] applicationState: didFinishLaunchingWithOptions for iOS9: \(userInfo)")
            //TODO: Handle background notification
        }

        if (launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [String: AnyObject]) != nil {
            let when = DispatchTime.now() + 2.0
            DispatchQueue.main.asyncAfter(deadline: when) {
               // self.handleNotification(userInfo: remoteNotificationData)
            }
        }
    }
    func requestNotificationAuthorization(application: UIApplication) {
        UNUserNotificationCenter.current().delegate = self
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {_ , _ in })
        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
    }
    /*@available(iOS 9.0, *)
   

      func application(_ application: UIApplication, open url: URL, sourceApplication: String?,
                       annotation: Any) -> Bool {
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
          // Handle the deep link. For example, show the deep-linked content or
          // apply a promotional offer to the user's account.
          // [START_EXCLUDE]
          // In this sample, we just open an alert.
          handleDynamicLink(dynamicLink)
          // [END_EXCLUDE]
          return true
        }
        // [START_EXCLUDE silent]
        // Show the deep link that the app was called with.
        showDeepLinkAlertView(withMessage: "openURL:\n\(url)")
        // [END_EXCLUDE]
        return false
      }

      // [END openurl]
      // [START continueuseractivity]
      func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                       restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        let handled = DynamicLinks.dynamicLinks()
          .handleUniversalLink(userActivity.webpageURL!) { dynamiclink, error in
            // [START_EXCLUDE]
            if let dynamiclink = dynamiclink {
              self.handleDynamicLink(dynamiclink)
            }
            // [END_EXCLUDE]
          }

        // [START_EXCLUDE silent]
        if !handled {
          // Show the deep link URL from userActivity.
          let message =
            "continueUserActivity webPageURL:\n\(userActivity.webpageURL?.absoluteString ?? "")"
          showDeepLinkAlertView(withMessage: message)
        }
        // [END_EXCLUDE]
        return handled
      }

      // [END continueuseractivity]
      func handleDynamicLink(_ dynamicLink: DynamicLink) {
        let matchConfidence: String
        if dynamicLink.matchType == .weak {
          matchConfidence = "Weak"
        } else {
          matchConfidence = "Strong"
        }
        let message = "App URL: \(dynamicLink.url?.absoluteString ?? "")\n" +
          "Match Confidence: \(matchConfidence)\nMinimum App Version: \(dynamicLink.minimumAppVersion ?? "")"
        showDeepLinkAlertView(withMessage: message)
      }

      func showDeepLinkAlertView(withMessage message: String) {
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        let alertController = UIAlertController(
          title: "Deep-link Data",
          message: message,
          preferredStyle: .alert
        )
        alertController.addAction(okAction)
        window?.rootViewController?.present(alertController, animated: true, completion: nil)
      }*/


}



//-------------------------------------------------
//MARK:- Firebase messaging
//-------------------------------------------------
extension AppDelegate:MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("fcm token :- \(String(describing: fcmToken))")
        self.strFcmToken = fcmToken!
        UserDefaults.standard.set(fcmToken, forKey: "FCMToken")
    }

    // iOS9, called when presenting notification in foreground
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {

        print("[RemoteNotification] applicationState:  didReceiveRemoteNotification for iOS9: \(userInfo)")
       // self.handleNotification(userInfo: userInfo)
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {

        Messaging.messaging().apnsToken = deviceToken
        self.strFcmToken = Messaging.messaging().fcmToken ?? ""
    }
    
}
//-------------------------------------------------
//MARK:- Notification delegate
//-------------------------------------------------
extension AppDelegate: UNUserNotificationCenterDelegate {
    private func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompleteionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
          
        }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {

        let userInfo = notification.request.content.userInfo
     //   print(" UserNotificationCenter applicationState: willPresentNotification: \(userInfo)")

        // send push notifcation steps - [click on ment then choose fanbase -> select mutiple name and click on send button]
        // open ContestJustMeVC view controller  when we get push notifcation from fanbase
        // on ContestJustMeVC call api " -> https://ctinfotech.com/CT06/wooo/api/getcontest" with parameter "contest_id:1"
        completionHandler([.alert, .sound, .badge])
    }

     func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {

        let userInfo = response.notification.request.content.userInfo
      // print(" didReceiveResponse: \(userInfo)")
        //TODO: Handle background notification
        /*let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let enqVC = storyboard.instantiateViewController(withIdentifier: "HomeSliderVC") as! HomeSliderVC
        let navigationController = UINavigationController.init(rootViewController: enqVC)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()*/
        NotificationCenter.default.post(name: .notificationData, object: userInfo, userInfo: userInfo)

        completionHandler()
    }
}


extension AppDelegate{

    /*func application(_ app: UIApplication, open url: URL,
                     options: [UIApplication.OpenURLOptionsKey: Any]) -> Bool {
      return application(app, open: url,
                         sourceApplication: options[UIApplication.OpenURLOptionsKey
                           .sourceApplication] as? String,
                         annotation: "")
    }*/

func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
    let str: String? = url.debugDescription
    print("Test deeplink\(str!)")

    if (url.scheme == "artistarmy") {

        let str: String? = url.debugDescription
        let strValue = str?.replacingOccurrences(of: "artistarmy://com.ctinfotech.hamro.nepali.music.activity", with: "")
        print(strValue)
        if (strValue?.contains("paymentSuccess"))! {

        }else {

            let fullEventArr = strValue?.components(separatedBy: "/")

            let  strEventId = fullEventArr![1]
            let  strUserId = fullEventArr![2]

            print("1111:- \(strEventId)")
            print("2222:- \(strUserId)")

        /*    if let loginDict = UserDefaults.standard.dictionary(forKey: loginDetail)  {

                AppDataManager.shared.loginData = LoginModel(loginDetail: loginDict)

               /* let eventDetailVC = ContestJustMeVC.instance() as! ContestJustMeVC

                eventDetailVC = strEventId
                eventDetailVC.userId = strUserId

                (window?.rootViewController as? UINavigationController)?.pushViewController(eventDetailVC, animated: true)*/

            }*/
        }
        return true
    }else{
        return true
    }
    
}
}


extension Notification.Name {
      static let notificationData = Notification.Name("notificationData")
    static let Deeplinking = Notification.Name("Deeplinking")
    
    
    
}
