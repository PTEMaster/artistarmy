//
//  SceneDelegate.swift
//  Artistarmy
//
//  Created by mac on 03/06/21.
//

import UIKit
import Firebase
import FirebaseMessaging
import FirebaseDynamicLinks

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
      //
        if let userActivity = connectionOptions.userActivities.first {
            if let incomingURL = userActivity.webpageURL {
                _ = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURL) { (dynamicLink, error) in
                    guard error == nil else { return }
                    if let dynamicLink = dynamicLink {
                        if let urlString = dynamicLink.url?.absoluteString {
                            // Handle deep links
                            print("url string:-  ", urlString)
                           // self.handleDeepLink(urlString: urlString)
                        }
                    }
                }
            }else {
                print("web url not found :-- ", userActivity)
            }
        }
        guard let _ = (scene as? UIWindowScene) else { return }
    }
    
    


    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    
    
  
func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        guard let url = URLContexts.first?.url else {
            return
        }
        if (url.scheme == "artistarmy") {
            let str: String? = url.debugDescription
            print("deeplink------=\(str!)")
            let strValue = str?.replacingOccurrences(of: "artistarmy://?", with: "")
          //  print("strValue\(strValue!)")
            if (strValue?.contains("type=3"))! {
                if UserDefaults.standard.isLoggedIn() {
                    let fullEventArr = strValue?.components(separatedBy: "&")
                    NotificationCenter.default.post(Notification(name: .Deeplinking, object: fullEventArr, userInfo: nil))
                    
                }else{
                    
                    let fullEventArr = strValue?.components(separatedBy: "&")
                   
                        let  artist_id = fullEventArr![0]
                        let  user_id = fullEventArr![1]
                        
                        let listItems = artist_id.components(separatedBy: "=")
                        appDelegate.ref_artist_id = listItems[1]
                        let listItems2 = user_id.components(separatedBy: "=")
                        appDelegate.ref_user_id = listItems2[1]
                    
                    
                }
                
                
             
            }else{
               
            }
        }
    }
    
    
    func scene(_ scene: UIScene, continue userActivity: NSUserActivity) {
        
        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
              let url = userActivity.webpageURL,
              let host = url.host else {
                  return
              }
        
        DynamicLinks.dynamicLinks().handleUniversalLink(url) { dynamicLink, error in
            guard error == nil,
                  let dynamicLink = dynamicLink,
                  let urlString = dynamicLink.url?.absoluteString else {
                      return
                  }
          //  print("Dynamic link host: \(host)")
          //  print("Dyanmic link url: \(urlString)")
          //  print("Dynamic link match type: \(dynamicLink.matchType.rawValue)")
          //  let newURL = URL(string: urlString)!
            let artist_id =      dynamicLink.url!.valueOf("artist_id")
            let user_id =     dynamicLink.url!.valueOf("user_id")
            appDelegate.ref_artist_id = artist_id!
            appDelegate.ref_user_id = user_id!
        }
    }
    
    
  
    
    
  /* func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let str: String? = url.debugDescription
        print("Test deeplink\(str!)")

        if (url.scheme == "artistarmy") {

            let str: String? = url.debugDescription
            let strValue = str?.replacingOccurrences(of: "artistarmy://com.ctinfotech.hamro.nepali.music.activity", with: "")
            print(strValue)
            if (strValue?.contains("paymentSuccess"))! {

            }else {

                let fullEventArr = strValue?.components(separatedBy: "/")

                let  strEventId = fullEventArr![1]
                let  strUserId = fullEventArr![2]

                print("1111:- \(strEventId)")
                print("2222:- \(strUserId)")

            /*    if let loginDict = UserDefaults.standard.dictionary(forKey: loginDetail)  {

                    AppDataManager.shared.loginData = LoginModel(loginDetail: loginDict)

                   /* let eventDetailVC = ContestJustMeVC.instance() as! ContestJustMeVC

                    eventDetailVC = strEventId
                    eventDetailVC.userId = strUserId

                    (window?.rootViewController as? UINavigationController)?.pushViewController(eventDetailVC, animated: true)*/

                }*/
            }
        }
        return true
    }*/

}

extension URL {
    func valueOf(_ queryParamaterName: String) -> String? {
        guard let url = URLComponents(string: self.absoluteString) else { return nil }
        return url.queryItems?.first(where: { $0.name == queryParamaterName })?.value
    }
}

