//
//  PaymentWebViewController.swift
//  HorseCo
//
//  Created by Chaitrali Mhatre on 11/14/18.
//  Copyright © 2018 Leza Solutions. All rights reserved.
//

import UIKit
import Alamofire
import WebKit

class PaymentWebViewController: UIViewController,WKUIDelegate,WKNavigationDelegate {
    
    @IBOutlet weak var webView: WKWebView!
    
  //  var webView: WKWebView!
    var isfromWallet = false
    var isfromClass = false
    var isfromGiftVoucher = false
    var paymentUrl: String!
    var successUrl: String!
    var failureUrl: String!
    
    var DeviceWidth:CGFloat = UIScreen.main.bounds.width;
    var ScreenHeight = UIScreen.main.bounds.height;
    var ScreenWidth = UIScreen.main.bounds.width;
    var PrimaryTextColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = ""
        let backBtn = UIButton()
        self.view.backgroundColor =  .clear
        backBtn.setImage(UIImage(named: "back") , for: .normal)
        backBtn.addTarget(self, action:#selector(goback) , for: .touchUpInside)
        let leftMenu = UIBarButtonItem();
        leftMenu.customView=backBtn;
        self.navigationItem.leftBarButtonItem=leftMenu;
        
        self.navigationItem.hidesBackButton=true;
        
        let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
        let userScript = WKUserScript(source: jscript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        let wkUController = WKUserContentController()
        wkUController.addUserScript(userScript)
      //  let wkWebConfig = WKWebViewConfiguration()
       // wkWebConfig.userContentController = wkUController
        
        var finalHeight:CGFloat = (self.navigationController?.navigationBar.frame.size.height)!
        var bottomPadding:CGFloat = 0
        
        if #available(iOS 11.0, *) {
            let window: UIWindow? = UIApplication.shared.keyWindow
            bottomPadding = window?.safeAreaInsets.bottom ?? 0
        }
        
        finalHeight = finalHeight+UIApplication.shared.statusBarFrame.height+bottomPadding
        
        self.view.backgroundColor = .white
        
       // self.webView = WKWebView(frame: CGRect.init(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight-finalHeight), configuration: wkWebConfig)
        self.webView.isOpaque = false
        self.webView.uiDelegate = self
        self.webView.navigationDelegate = self
        self.webView.translatesAutoresizingMaskIntoConstraints = false
      //  view.addSubview(self.webView)
        
        if(self.paymentUrl.count > 0)
        {
            self.webView.load(NSURLRequest(url: NSURL(string: self.paymentUrl)! as URL) as URLRequest)
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionBack(_ sender: Any) {
        pop()
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        if (self.navigationController?.navigationBar.isHidden)!
        {
           // self.activityLoader.showFullScreenActivityIndicator(uiView: (self.navigationController?.view)!)
        }
        else
        {
          //  self.activityLoader.showActivityIndicator(uiView:(self.navigationController?.view)!)
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
      //  self.activityLoader.hideActivityIndicator()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
       // self.activityLoader.hideActivityIndicator()
    }
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
       // self.activityLoader.hideActivityIndicator()
    }
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping ((WKNavigationActionPolicy) -> Void)) {
        let urlString = navigationAction.request.url
        let url = urlString!.absoluteString
        print("PaymentUrl"+urlString!.absoluteString)
       
        
   
        
    if url.range(of:self.failureUrl) != nil{
            print("exists")
            decisionHandler(.cancel)
        }else{
            
            decisionHandler(.allow)
            
//            if url.range(of:self.successUrl) != nil{
//                let seconds = 4.0
//                DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
//                    self.parseAndSend(url: url)
//                    decisionHandler(.cancel)
//                }
//             }
        }
    }
    
    // MARK:- Changes done by Creative thought 
    func parseAndSend(url: String) {
        
        self.webView.stopLoading()
        pop()
    }
    @objc private func goback() {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
  
    
}

